#ifndef HEADER_lw_detect_OS_hpp_ALREADY_INCLUDED
#define HEADER_lw_detect_OS_hpp_ALREADY_INCLUDED

#if defined(_WIN32) || defined(_WIN64) || defined(__WIN32__) || defined(__WINDOWS__)
#define LW_OS_WINDOWS 1
#endif

#if defined(linux) || defined(__linux)
#define LW_OS_LINUX 1
#endif

#if defined(macintosh) || defined(Macintosh) || defined(__APPLE__) || defined(__MACH__)
#define LW_OS_MAC 1
#endif

#endif
