#ifndef HEADER_lw_build_info_h_ALREADY_INCLUDED
#define HEADER_lw_build_info_h_ALREADY_INCLUDED

#include <lw/toolset/config.h>

#define LW_BUILD_TIME __TIME__
#define LW_BUILD_DATE __DATE__
#if LW_TOOLSET_DEBUG
#define LW_BUILD_MODE "debug"
#else
#define LW_BUILD_MODE "release"
#endif

#endif

