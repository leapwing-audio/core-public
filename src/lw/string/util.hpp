#ifndef HEADER_lw_string_util_hpp_ALREADY_INCLUDED
#define HEADER_lw_string_util_hpp_ALREADY_INCLUDED

#include <lw/Range.hpp>
#include <string>
#include <iostream>

namespace lw { namespace string {

    std::string join(const lw::ConstRange<std::string> &strings, const std::string &separator = std::string());
    std::string gsub(const std::string &src, const std::string &pattern, const std::string &replace);
    std::string trim(const std::string &src);

    template<typename It>
    It split(const std::string &string, const std::string &separator, It out)
    {
        std::size_t pos = 0;
        bool empty = separator.empty();

        auto find = [&](std::size_t pos){
            return empty? pos + 1 : string.find(separator, pos);
        };

        std::size_t increase = std::max<std::size_t>(string.size(), 1);

        while (pos < string.size())
        {
            std::size_t cur_pos = find(pos);
            *out++ = string.substr(pos, cur_pos - pos);

            if (cur_pos == std::string::npos)
                pos = std::string::npos;
            else
                pos = cur_pos + separator.size();
        }

        return out;
    }

}}

#endif
