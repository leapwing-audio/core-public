#include <lw/string/util.hpp>

namespace lw { namespace string {

    std::string join(const lw::ConstRange<std::string> & strings, const std::string & separator)
    {
        std::string res;

        for(unsigned int i = 0; i < strings.size(); ++i)
        {
            if (i !=  0)
                res.append(separator);
            res.append(strings[i]);
        }

        return res;
    }

    std::string gsub(const std::string & src, const std::string & pattern, const std::string & replace)
    {
        std::string res = src;
        
        std::size_t index = 0;
        while (index < res.size())
        {
            // find the string
            index = res.find(pattern, index);
            if (index != std::string::npos)
            {
                res.replace(index, pattern.size(), replace);
                index += replace.size();
            }
        }

        return res;
    }
    
    std::string trim(const std::string & src)
    {
        std::size_t s = src.find_first_not_of(' ');
        if (s >= src.size())
            return std::string();

        std::size_t e = src.find_last_not_of(' ');

        return src.substr(s, e - s + 1);
    }

}}
