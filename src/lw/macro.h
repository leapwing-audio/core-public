/*!
 * Paqart: Plugin Audio Quality Assurance and Regression Testing tool
 * Copyright (C) 2018  Leapwing Audio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef HEADER_lw_macro_h_ALREADY_INCLUDED
#define HEADER_lw_macro_h_ALREADY_INCLUDED

#define LW_GET_ARG_1_(V, ...)                   V
#define LW_GET_ARG_2_(_1, V, ...)               V
#define LW_GET_ARG_3_(_1, _2, V, ...)           V
#define LW_GET_ARG_1(all)                       LW_GET_ARG_1_ all
#define LW_GET_ARG_2(all)                       LW_GET_ARG_2_ all
#define LW_GET_ARG_3(all)                       LW_GET_ARG_3_ all

#define LW_STRINGIFY(x) #x
#define LW_TO_STRING(x) LW_STRINGIFY(x)

#define C(VAL) "(" << #VAL << ":" << (VAL) << ")"


#endif

