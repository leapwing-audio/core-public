#ifndef HEADER_lw_toolset_config_h_ALREADY_INCLUDED
#define HEADER_lw_toolset_config_h_ALREADY_INCLUDED

#ifdef __cplusplus
extern "C" {
#endif

#if !defined(NDEBUG)
#define LW_TOOLSET_DEBUG 1
#else
#define LW_TOOLSET_RELEASE 1
#endif

#if defined(linux) || defined(__linux) || defined(__linux__)
#define LW_TOOLSET_OS "linux"
#define LW_TOOLSET_OS_LINUX 1
#elif defined(macintosh) || defined(Macintosh) || (defined( __APPLE__) && defined(__MACH__))
#define LW_TOOLSET_OS "mac"
#define LW_TOOLSET_OS_MAC 1
#elif defined(_WIN32) || defined(_WIN64) || defined(__WIN32__) || defined(__WINDOWS__)
#define LW_TOOLSET_OS "windows"
#define LW_TOOLSET_OS_WINDOWS 1
#else
#error "Undefined operating system"
#endif

#if defined(__x86__) || defined(__X86__) || defined(_M_IX86)
#define LW_TOOLSET_ARCH "x86"
#define LW_TOOLSET_ARCH_X86 1
#elif defined(__x86_64__) || defined(__X86_64__) || defined (_M_X64)
#define LW_TOOLSET_ARCH "x86_64"
#define LW_TOOLSET_ARCH_X86_64 1
#elif defined(__arm64__) || defined(__ARM64__) || defined(_M_ARM64)
#define LW_TOOLSET_ARCH "arm64"
#define LW_TOOLSET_ARCH_ARM64 1
#else
#error "Undefined architecture"
#endif

#ifdef __cplusplus
}
#endif

#endif
