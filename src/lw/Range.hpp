#ifndef HEADER_lw_Range_hpp_ALREADY_INCLUDED
#define HEADER_lw_Range_hpp_ALREADY_INCLUDED

#include <type_traits>
#include <iterator>
#include <vector>
#include <array>

namespace lw {

#define RANGE(CTR) (CTR).begin(), (CTR).end()

    template <typename T> struct Range
    {
        using value_type            = T;
        using pointer               = T *;
        using reference             = T &;
        using difference_type       = std::size_t;
        using iterator_category     = std::random_access_iterator_tag;

        Range() : first_(nullptr), last_(nullptr) {}
        Range(T * first, T * last) : first_(first), last_(last) {}
        Range(T * first, difference_type n) : first_(first), last_(first + n) {} 

        template <typename T_> 
        Range(std::vector<T_> & vct) : first_(vct.data()), last_(vct.data() + vct.size()) {}
        template <typename T_> 
        Range(const std::vector<T_> & vct) : first_(vct.data()), last_(vct.data() + vct.size()) {}
        
        template <typename T_, std::size_t N> 
        Range(std::array<T_, N> & vct) : first_(vct.data()), last_(vct.data() + N) {}
        template <typename T_, std::size_t N> 
        Range(const std::array<T_, N> & vct) : first_(vct.data()), last_(vct.data() + N) {}

        template <typename T2> 
        Range(const Range<T2> & rhs) : 
            first_(rhs.first_), last_(rhs.last_)
        {
        }

        difference_type size() const 
        { 
            return std::distance(first_, last_); 
        }

        bool empty() const 
        { 
            return first_ == last_; 
        }

        T * begin() const
        { 
            return first_; 
        }

        T * end() const
        { 
            return last_; 
        }

        const T * cbegin() const
        { 
            return first_; 
        }

        const T * cend() const
        { 
            return last_; 
        }

        reference back() const
        {
            pointer lst = last_;
            return *(--lst);
        }

        reference front() const
        {
            return *first_;
        }

        template <typename VT> bool pop_front(VT && vt)
        {
            if(empty())
                return false;
            vt = front();

            ++first_;
            return true;
        }
        
        Range<T> drop_front(std::size_t num_samples)
        {
            std::size_t to_drop = std::min(num_samples, size());

            // create the return value
            Range result = *this;
            result.last_ = result.first_ + to_drop;

            // update our pointer
            first_ += to_drop;

            return result;
        }

        bool pop_front()
        {
            if(empty())
                return false;
            ++first_;
            return true;
        }

        template <typename VT> bool pop_back(VT && vt)
        {
            if(empty())
                return false;
            vt = back();
            --last_;
            return true;
        }

        bool pop_back()
        {
            if(empty())
                return false;
            --last_;
            return true;
        }

        pointer data() const
        {
            return first_;
        }

        reference operator[](difference_type idx) const
        {
            return first_[idx];
        }

        Range<const T> as_const() const
        {
            return { first_, last_ };
        }

        private:
        template <typename It_>
        friend class Range;

        T * first_ = nullptr;
        T * last_ = nullptr;
        
    };

    template <typename T> using ConstRange = Range<const T>;

    template <typename T>
    Range<T> range(T * first, T * last) 
    { 
        return Range<T>(first, last); 
    }
    template <typename T>
    ConstRange<T> crange(const T * first, const T * last) 
    { 
        return ConstRange<T>(first, last); 
    }

    template <typename T>
    Range<T> range(T * first, std::size_t n)
    { 
        return Range<T>(first, n); 
    }
    
    template <typename T>
    ConstRange<T> crange(const T * first, std::size_t n)
    { 
        return ConstRange<T>(first, n); 
    }
    
    template <typename CTR>
    ConstRange<typename CTR::value_type> crange(const CTR & c)
    { 
        return ConstRange<typename CTR::value_type>(c.data(), c.size()); 
    }
    
    template <typename CTR>
    Range<typename CTR::value_type> range(CTR & c)
    { 
        return Range<typename CTR::value_type>(c.data(), c.size()); 
    }
}

#endif
