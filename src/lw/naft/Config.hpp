/*!
 * Paqart: Plugin Audio Quality Assurance and Regression Testing tool
 * Copyright (C) 2018  Leapwing Audio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef HEADER_lw_naft_Config_hpp_ALREADY_INCLUDED
#define HEADER_lw_naft_Config_hpp_ALREADY_INCLUDED

namespace lw { namespace naft {

    struct Config
    {
        static constexpr char tag_open()    { return '['; }
        static constexpr char tag_close()   { return ']'; }
        static constexpr char attr_open()   { return '('; }
        static constexpr char attr_close()  { return ')'; }
        static constexpr char scope_open()  { return '{'; }
        static constexpr char scope_close() { return '}'; }
    };

    struct Tag_t {};
    struct Attribute_t {};
    struct Scope_t {};

    template <typename T> struct Matching;
    template <> struct Matching<Tag_t>
    {
        static constexpr char open() { return Config::tag_open(); }
        static constexpr char close() { return Config::tag_close(); }
    };
    template <> struct Matching<Attribute_t>
    {
        static constexpr char open() { return Config::attr_open(); }
        static constexpr char close() { return Config::attr_close(); }
    };
    template <> struct Matching<Scope_t>
    {
        static constexpr char open() { return Config::scope_open(); }
        static constexpr char close() { return Config::scope_close(); }
    };

} }

#endif
