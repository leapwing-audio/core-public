/*!
 * Paqart: Plugin Audio Quality Assurance and Regression Testing tool
 * Copyright (C) 2018  Leapwing Audio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef HEADER_lw_naft_write_Node_hpp_ALREADY_INCLUDED
#define HEADER_lw_naft_write_Node_hpp_ALREADY_INCLUDED

#include <string>
#include <ostream>

namespace lw { namespace naft { namespace write { 

    struct Node
    {
        private:
        template <typename T>
        Node(T && tag, std::ostream * oss, unsigned int depth) : 
            oss_(oss),
            is_leaf_(true),
            depth_(depth)
        {
            indent_() << "[" << std::forward<T>(tag) << "]";
        }
        public:
        ~Node()
        {
            if (oss_)
            {
                if (!is_leaf_)
                {
                    indent_() << "}";
                }
                
                *oss_ << std::endl;
                oss_ = nullptr;
            }
        }
        Node(Node && rhs) : 
            oss_(rhs.oss_),
            is_leaf_(rhs.is_leaf_),
            depth_(rhs.depth_)
        {
            rhs.oss_ = nullptr;
        }

        Node & operator=(Node && rhs)
        {
            std::swap(oss_, rhs.oss_);
            std::swap(is_leaf_, rhs.is_leaf_);
            std::swap(depth_, rhs.depth_);

            return *this;
        }

        template <typename T>
        Node node(T && tag)
        {
            if (is_leaf_)
            {
                *oss_ << " {" << std::endl;
                is_leaf_ = false;
            }

            return Node(std::forward<T>(tag), oss_, depth_+1);
        }

        template <typename K, typename V>
        Node & attr(K && key, V && value)
        {
            *oss_ << "(" << key << ":" << value << ")";
            return *this;
        }
        template <typename K>
        Node & attr(K && key)
        {
            *oss_ << "(" << key << ")";
            return *this;
        }

        std::ostream &oss() { return *oss_; }

    private:
        Node(const Node &) = delete;
        Node & operator=(const Node &) = delete;
        
        friend class Document;
        std::ostream & indent_()
        {
            for(unsigned int i = 0; i < depth_; ++i)
                *oss_ << "  ";
            return *oss_;
        }
        std::ostream * oss_;
        bool is_leaf_;
        unsigned int depth_;
        public:

    };

} } }

#endif
