/*!
 * Paqart: Plugin Audio Quality Assurance and Regression Testing tool
 * Copyright (C) 2018  Leapwing Audio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef HEADER_lw_naft_write_Document_hpp_ALREADY_INCLUDED
#define HEADER_lw_naft_write_Document_hpp_ALREADY_INCLUDED

#include "lw/naft/write/Node.hpp"

namespace lw { namespace naft { namespace write { 

    struct Document
    {
        Document(std::ostream & oss) : 
            oss_(&oss)
        {
        }

        template <typename T>
        Node node(T && tag)
        {
            return Node(std::forward<T>(tag), oss_, 0);
        }

        private:
        std::ostream * oss_;
    };


} } }

#endif
