/*!
 * Paqart: Plugin Audio Quality Assurance and Regression Testing tool
 * Copyright (C) 2018  Leapwing Audio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef HEADER_lw_naft_read_Range_hpp_ALREADY_INCLUDED
#define HEADER_lw_naft_read_Range_hpp_ALREADY_INCLUDED

#include "lw/naft/Config.hpp"

namespace lw { namespace naft { namespace read {
    
    template <typename Matching>
    const char * find_closing(const char * begin, const char * end, Matching);

    struct Range
    {
        enum State
        {
            Invalid, End, Tag, Attribute, ScopeOpen, ScopeClose
        };
    
        Range(const char * begin, const char * end, State cur = ScopeOpen) : 
            begin_(begin),
            end_(end),
            cur_(cur)
        {
            advance();
        }

        Range() : 
            begin_(nullptr),
            end_(nullptr),
            cur_(Invalid)
        {
        }

        unsigned int size() const
        {
            if (begin_ == nullptr || end_ == nullptr)
                return 0;
            return end_ - begin_;
        }

        const char * find_end() const
        {
            switch(cur_)
            {
                case Invalid:
                case End:
                default:
                    return nullptr;

                case ScopeOpen:
                case ScopeClose:
                    return begin_+1;

                case Tag:
                    return find_closing(begin_, end_, Matching<Tag_t>());

                case Attribute:
                    return find_closing(begin_, end_, Matching<Attribute_t>());
            }
        }

        State current_state() const
        {
            return cur_;
        }

        bool valid() const
        {
            switch(cur_)
            {
                case Tag:
                case Attribute:
                case ScopeOpen:
                case ScopeClose:
                    return true;
                default:
                    return false;
            }
        }

        bool advance(const char * pos)
        {
            if (pos == nullptr)
            {
                begin_ = end_ = nullptr;
                cur_ = Invalid;
                return false;
            }
            else if (pos == end_)
            {
                begin_ = end_ = nullptr;
                cur_ = End;
                return false;
            }
            else
            {
                begin_ = pos;
                goto_next_state_();
                return valid();
            }
        }


        bool advance()
        {
            return advance(begin_);
        }

        const char * current() const { return begin_; }
        const char * end() const { return end_; }

        private:
        State goto_next_state_()
        {
            switch(cur_)
            {
                case Invalid:
                case End:
                    return cur_;
                case Tag:
                case Attribute:
                    return cur_ = find_next_state_ext_();
                case ScopeOpen:
                case ScopeClose:
                    return cur_ = find_next_state_();

                default:
                    return cur_ = Invalid;
            }
        }

        State find_next_state_()
        {
            for(; begin_ != end_; ++begin_)
            {
                switch(*begin_)
                {
                    case Config::tag_open():    return Tag;
                    case Config::scope_close(): return ScopeClose;
                    default:
                        break;
                }
            }

            return End;
        }

        State find_next_state_ext_()
        {
            for(; begin_ != end_; ++begin_)
            {
                switch(*begin_)
                {
                    case Config::tag_open():    return Tag;
                    case Config::attr_open():   return Attribute;
                    case Config::scope_open():  return ScopeOpen;
                    case Config::scope_close(): return ScopeClose;
                    default:
                        break;
                }
            }

            return End;
        }

        const char * begin_;
        const char * end_;
        State cur_;
    };

    template <typename Matching>
    const char * find_closing(const char * begin, const char * end, Matching)
    {
        unsigned int open_count = 1;
        for(const char * it = begin + 1; it != end; ++it)
        {
            switch(*it)
            {
                case Matching::open():
                    ++open_count;
                    break;
                case Matching::close():
                    --open_count;
                    break;
                default:
                    break;
            }

            if (open_count == 0)
                return it;
        }

        return end;
    }

} } }

#endif
