/*!
 * Paqart: Plugin Audio Quality Assurance and Regression Testing tool
 * Copyright (C) 2018  Leapwing Audio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef HEADER_lw_naft_read_Document_hpp_ALREADY_INCLUDED
#define HEADER_lw_naft_read_Document_hpp_ALREADY_INCLUDED

#include "lw/naft/read/Range.hpp"
#include <string>
#include <algorithm>

namespace lw { namespace naft { namespace read {

    struct Document
    {
        Document() : range_() {}
        Document(const char * begin, const char * end) : range_(begin, end) {}
        explicit Document(const std::string & text) : range_(&text[0], &text[0] + text.size()) {}

        bool pop_tag_if(const std::string & value)
        {
            if (!range_.valid())
                return false;

            if (range_.current_state() != Range::Tag)
                return false;

            // find the end
            const char * tag_end = range_.find_end();
            if (tag_end == range_.end())
                return false;

            if(!std::equal(value.begin(), value.end(), range_.current()+1, tag_end))
                return false;

            range_.advance(tag_end + 1);
            return true;
        }

        bool pop_tag(std::string & value)
        {
            if (!range_.valid())
                return false;

            if (range_.current_state() != Range::Tag)
                return false;

            // find the end
            const char * tag_end = range_.find_end();
            if (tag_end == range_.end())
                return false;

            value.assign(range_.current()+1, tag_end);

            range_.advance(tag_end + 1);
            return true;
        }

        bool pop_attribute(std::string & key, std::string & value)
        {
            if (!range_.valid())
                return false;

            if (range_.current_state () != Range::Attribute)
                return false;

            const char * attribute_end = range_.find_end();
            if (attribute_end == range_.end())
                return false;

            auto it = std::find(range_.current(), attribute_end, ':');
            key.assign(range_.current() +1, it);

            if (it == attribute_end)
                value.clear();
            else
                value.assign(it+1, attribute_end);

            range_.advance(attribute_end + 1);
            return true;
        }
        bool pop_attribute_if(const std::string & key, std::string & value)
        {
            if (!range_.valid())
                return false;

            if (range_.current_state () != Range::Attribute)
                return false;

            const char * attribute_end = range_.find_end();
            if (attribute_end == range_.end())
                return false;

            auto it = std::find(range_.current(), attribute_end, ':');
            if (!std::equal(key.begin(), key.end(), range_.current()+1, it))
                return false;

            if (it == attribute_end)
                value.clear();
            else
                value.assign(it+1, attribute_end);

            range_.advance(attribute_end + 1);
            return true;
        }

        bool child_node_open()
        {
            if (!range_.valid())
                return false;

            if (range_.current_state() != Range::ScopeOpen)
                return false;

            range_.advance(range_.current() + 1);
            return true;
        }
        
        bool child_node_close()
        {
            if (!range_.valid())
                return false;

            if (range_.current_state() != Range::ScopeClose)
                return false;

            range_.advance(range_.current() + 1);
            return true;
        }

        bool attributes_finished()
        {
            switch(range_.current_state())
            {
                case Range::End:
                case Range::ScopeOpen:
                case Range::ScopeClose:
                case Range::Tag:
                    return true;

                default:
                    return false;
            }
        }

        bool valid() const
        {
            return range_.valid();
        }

        private:
        Range range_;

    };

} } }

#endif
