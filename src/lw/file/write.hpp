#ifndef HEADER_lw_file_write_hpp_ALREADY_INCLUDED
#define HEADER_lw_file_write_hpp_ALREADY_INCLUDED

#include "lw/mss.hpp"
#include "lw/file/Path.hpp"
#include "lw/file/utils.hpp"
#include <fstream>

namespace lw { namespace file {

    inline bool open(std::ofstream & ofs, const Path & fn, bool should_mkdir = false)
    {
        MSS_BEGIN(bool);
        
        if(should_mkdir)
            MSS(mkdir_p(fn.parent()));

        ofs.open(fn.string().c_str());
        MSS(ofs.good());
        MSS_END();
    }

    template <typename F>
    bool write(const Path & fn, F && f, bool should_mkdir = false)
    {
        MSS_BEGIN(bool);

        std::ofstream ofs;
        MSS(open(ofs, fn, should_mkdir));

        f(ofs);

        MSS_END();
    }

} }

#endif
