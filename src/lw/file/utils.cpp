#include "lw/file/utils.hpp"
#include "lw/detect/OS.hpp"
#include "lw/mss.hpp"

#if defined(LW_OS_WINDOWS)
#include <windows.h>
#elif defined(LW_OS_LINUX) || defined(LW_OS_MAC)
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#endif
#include <cstdlib>

namespace lw { namespace file {

    bool mkdir(const Path & p)
    {
        MSS_BEGIN(bool);
        std::string s = p.string();
        if (s.empty())
            MSS_RETURN_OK();

        const char *  str = s.c_str();

#if defined(LW_OS_WINDOWS)
        bool res = CreateDirectoryA(str, NULL);
        if (!res)
            MSS(GetLastError() == ERROR_ALREADY_EXISTS);

#elif defined(LW_OS_LINUX) || defined(LW_OS_MAC)
        struct stat st = {};

        if (stat(str, &st) == 0 && S_ISDIR(st.st_mode))
            MSS_RETURN_OK();
        MSS(::mkdir(str, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH) == 0);

#else
        MSS(false);
#endif
        MSS_END();
    }

    bool mkdir_p(const Path & p)
    {
        MSS_BEGIN(bool);
        Path c = p.root_path();

        for(const std::string & p : p.parts())
        {
            c /= p;
            MSS(mkdir(c));
        }

        MSS_END();
    }

    Path home()
    {
#if defined(LW_OS_WINDOWS)
        {
            const char * drive = getenv("HOMEDRIVE");
            const char * dir = getenv("HOMEPATH");

            if (drive && dir)
                return Path(std::string(drive) + std::string(dir));
        }
#else
        {
            const char * dir = getenv("HOME");
            if (dir)
                return Path(std::string(dir));
        }
#endif

        // fallback
        return Path("./");
    }



} }
