#include "lw/file/Path.hpp"
#include <iostream>
#include <algorithm>

namespace lw { namespace file {
        
    const char Path::Separator;

    Path::Path(const std::string & fn)
    {
        std::string can = make_canonical_(fn);

        std::size_t pos = 0;

        // the root_
        {
            std::size_t rsep = can.find(':', pos);
            if (rsep != std::string::npos)
            {
                root_ = can.substr(pos, rsep-pos);
                pos = rsep + 1;
            }
        }

        // the root _dir_
        if (pos < can.size())
        {
            if(can[pos] == Separator)
            {
                root_dir_ = Separator;
                ++pos;
            }
        }

        // the parts
        while(pos < can.size())
        {
            std::size_t p = can.find(Separator, pos);
            if(p > pos)
                parts_.push_back(can.substr(pos, p - pos));

            pos = p == std::string::npos ? std::string::npos : p + 1;
        }
    }
        
    Path::Path(const char * str)
    : Path(str ? std::string(str) : std::string())
    {
    }

    Path & Path::operator/=(const Path & path)
    {
        parts_.insert(parts_.end(), path.parts().begin(), path.parts().end());
        return *this;
    }

    Path Path::operator/(const Path & path) const
    {
        Path p = *this;
        p /= path;
        return p;
    }

    bool Path::operator==(const Path & path) const
    {
        return root_ == path.root_ 
            && root_dir_ == path.root_dir_
            && std::equal(RANGE(parts_), RANGE(path.parts_));
    }

    bool Path::operator!=(const Path & path) const
    {
        return !operator==(path);
    }

    std::string Path::root_path() const
    {
        if (!root_.empty())
            return root_ + ":/";
        else
            return root_dir_;
    }
        
    std::string Path::string() const
    {
        std::string result;

        std::size_t sz = root_.size() + root_dir_.size();
        for(auto it = parts_.begin(); it != parts_.end(); ++it)
            sz += (it == parts_.begin() ? 0 : 1) + it->size();

        result.reserve(sz);

        result.append(root_path());        
        for(auto it = parts_.begin(); it != parts_.end(); ++it)
        {
            if(it != parts_.begin())
                result.push_back(Separator);
            result.append(*it);
        }

        return result;
    }
        
    Path::operator std::string() const
    {
        return string();
    }
        
    std::ostream & operator<<(std::ostream & oss, const Path & p)
    {
        oss << p.root_path();
        auto rng = p.parts();
        for(auto it = rng.begin(); it != rng.end(); ++it)
        {
            if (it != rng.begin())
                oss << Path::Separator;
            oss << *it;
        }
        return oss;
    }

    std::string Path::make_canonical_(const std::string & str)
    {
        std::string res(str);

        std::size_t pos = 0;
        while(pos < res.size())
        {
            std::size_t s = res.find('\\', pos);
            if (s != std::string::npos)
                res[s] = Separator;
            pos = s;
        }

        return res;
    }

    Path Path::ancestor(iterator last) const
    {
        Path p = root_path();
        for(const auto & v : range(parts().begin(), last))
            p /= v;
        return p;
    }

    Path Path::parent() const
    {
        auto rng = parts();
        if (rng.empty())
            return *this;

        const std::string & str = rng.back();

        rng.pop_back();
        return ancestor(rng.end());
    }

} }
