#ifndef HEADER_lw_file_Path_hpp_ALREADY_INCLUDED
#define HEADER_lw_file_Path_hpp_ALREADY_INCLUDED

#include "lw/Range.hpp"
#include <string>
#include <list>

namespace lw { namespace file {

    struct Path
    {
        static constexpr char Separator = '/';
        using iterator = const std::string *;

        Path(const std::string & fn = std::string());
        Path(const char * str);
        
        Path & operator/=(const Path & path);
        Path operator/(const Path & path) const;
        bool operator==(const Path & path) const;
        bool operator!=(const Path & path) const;

        bool is_absolute() const { return root_dir_.empty(); }
        bool is_relative() const { return !is_absolute(); }

        lw::ConstRange<std::string> parts() const { return parts_; }
        std::string root_path() const;

        std::string string() const;
        operator std::string() const;

        Path ancestor(iterator last) const;
        Path parent() const;

        friend std::ostream & operator<<(std::ostream & oss, const Path & p);

        private:
        std::string make_canonical_(const std::string & fn);
        std::string root_;
        std::string root_dir_;
        std::vector<std::string> parts_;
    }; 
    
} }

#endif
