#ifndef HEADER_lw_file_utils_hpp_ALREADY_INCLUDED
#define HEADER_lw_file_utils_hpp_ALREADY_INCLUDED

#include "lw/file/Path.hpp"

namespace lw { namespace file {

    bool mkdir(const Path & p);
    bool mkdir_p(const Path & p);
    Path home();

} }

#endif
