#ifndef HEADER_lw_file_read_hpp_ALREADY_INCLUDED
#define HEADER_lw_file_read_hpp_ALREADY_INCLUDED

#include "lw/mss.hpp"
#include <fstream>

namespace lw { namespace file {

    bool open(std::ifstream & ifs, const std::string & fn)
    {
        MSS_BEGIN(bool);
        ifs.open(fn.c_str());
        MSS(ifs.good());
        MSS_END();
    }

    template <typename F>
    bool read(const std::string & fn, F && f)
    {
        MSS_BEGIN(bool);
        std::ifstream ifs;
        MSS(open(ifs, fn));

        f(ifs);

        MSS_END();
    }

} }

#endif
