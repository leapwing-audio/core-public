/*!
 * Paqart: Plugin Audio Quality Assurance and Regression Testing tool
 * Copyright (C) 2018  Leapwing Audio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef HEADER_lw_mss_hpp_ALREADY_INCLUDED
#define HEADER_lw_mss_hpp_ALREADY_INCLUDED

#include <lw/toolset/config.h>
#include <lw/macro.h>
#include <iostream>

#if LW_TOOLSET_RELEASE
#define MSS_VERBOSE 0
#else
#define MSS_VERBOSE 1
#endif

#if MSS_VERBOSE
#define MSS_LOG_ERROR_(expr) std::cout << "MSS error: Expression '" << #expr << "' failed (" << __FILE__ << ":" << __LINE__ << ")" << std::endl;
#else
#define MSS_LOG_ERROR_(expr)
#endif

namespace lw { namespace mss {

    template <typename RV> struct default_values
    {
        static RV ok()        { return RV::OK; }
        static RV error()     { return RV::Error; }
    };

    template <> struct default_values<bool>
    {
        static bool ok()        { return true; }
        static bool error()     { return false; }
    };


    template <typename T> struct traits
    {
        // by default just check if is the same as an ok value
        static bool is_ok(const T & t) { return t == default_values<T>::ok(); }
    };

    // ptr specializations
    template <typename T> struct traits<T *>        { static bool is_ok(T * t) { return t != nullptr; } };
    template <typename T> struct traits<const T *>  { static bool is_ok(const T * t) { return t != nullptr; } };
    template <> struct traits<std::nullptr_t>  { static bool is_ok(std::nullptr_t) { return false; } };


    template <typename T> bool is_ok(const T & t)
    {
        return traits<T>::is_ok(t);
    }
}

#define MSS_BEGIN(RV)       using lw_mss_return_value_type = RV; 
#define MSS_END()           return lw::mss::default_values<lw_mss_return_value_type>::ok();

#define MSS1(expression)    MSS2(expression, ;)
#define MSS2(expression, action) \
do \
{\
    if (!lw::mss::is_ok(expression)) \
    { \
        MSS_LOG_ERROR_( expression ) \
        action; \
        return lw::mss::default_values<lw_mss_return_value_type>::error(); \
    }\
}\
while(false) 

#define MSS_Q1(expression)    MSS_Q2(expression, ;)
#define MSS_Q2(expression, action) \
do \
{\
    if (!lw::mss::is_ok(expression)) \
    { \
        action; \
        return lw::mss::default_values<lw_mss_return_value_type>::error(); \
    }\
}\
while(false) 

#define MSS(...) LW_GET_ARG_3((__VA_ARGS__, MSS2, MSS1))(__VA_ARGS__)
#define MSS_Q(...) LW_GET_ARG_3((__VA_ARGS__, MSS_Q2, MSS_Q1))(__VA_ARGS__)
#define MSS_RETURN_OK()     return lw::mss::default_values<lw_mss_return_value_type>::ok();

}

#endif
