require 'fileutils'
require 'optparse'
require 'LW/os'
require 'LW/juce/cook'

module LW
    module Juce
        class Instance
            VISUAL_STUDIO_YEAR = 2022
            VISUAL_STUDIO_ARCH = 64

            attr_accessor :dir
            attr_accessor :mode
            attr_accessor :sdks
            attr_accessor :sdks_dir
            attr_accessor :primary_target
            attr_accessor :type

            def initialize(name, dir = nil)
                @name = name
                @dir = dir || Dir.pwd
                @sdks = [:juce]
                @sdk_dirs = {}
                @mode = :Release
                @primary_target = @name
                @type = nil
            end

            def configure()
                # check if the sdk needs to be pulled
                @sdks.each do |sdk|
                    path = nil
                    if $lw_sdk_dir && $lw_sdk_dir[path] 
                        path = $lw_sdk_dir
                    else
                        path = Juce.repo_dir(sdk)
                        Juce.fetch_sdk(sdk) unless File.exist?(path)
                    end

                    $lw_sdk_config[sdk].call(path, @mode) if $lw_sdk_config && $lw_sdk_config[sdk]
                end
            end

            def target_dir
                path  = [*dir, "Builds"]
                case OS.get
                when :linux
                    path += ["LinuxMakefile","build"]
                when :mac
                    path += ["MacOSX", "build" ,"#{@mode}"]
                when :windows
                    path += ["VisualStudio#{VISUAL_STUDIO_YEAR}", "x#{VISUAL_STUDIO_ARCH}","#{@mode}", type_]
                else
                    raise "#{OS.get} is not yet supported"
                end
                File.join(*path.compact)
            end

            def type_()
                if @type == :console
                    case OS.get
                    when :windows then return "ConsoleApp"
                    end
                end
                nil
            end

            def target(build_target = nil)
                return File.join(target_dir, build_target || primary_target)
            end

            def jucer_file(type: nil)
				prefix = type
				fn  = [prefix, @name, "jucer"].compact.join(".")
                File.join(@dir, fn)
            end

            def build(build_target = nil, build_name = nil)

                puts ">>> Building target #{@name}"
                case OS.get
                when :linux
                    dir = File.join(@dir, "Builds/LinuxMakefile")
                    cmd = []
                    cmd << "bear -- " if ENV['lw_make_cc']
                    cmd << "CXX=#{$force_cxx_compiler}" if $force_cxx_compiler
                    cmd << "C=#{$force_c_compiler}" if $force_c_compiler
                    cmd << "make -C #{dir} -j8 CONFIG=#{@mode}"
                    cmd << build_name.to_s if build_name 
                    Rake.sh(cmd.join(" "))
                when :mac
                    tgt_string = "-alltargets"
                    tgt_string = "-target '#{build_name}'" if build_name
                    Dir.chdir(File.join(@dir, "Builds/MacOSX")) do
                        cmd = "xcodebuild -project #{@name}.xcodeproj #{tgt_string} -configuration #{@mode} -jobs=8"
                        cmd += " | xcpretty -r json-compilation-database --output #{Build.root("compile_commands.json")}"if ENV['lw_make_cc']
                        Rake.sh(cmd) 
                    end
                when :windows
                    tgt_string = ""
                    tgt_string = "-target:\"#{build_name}\"" if build_name
                    cmd = "MSBuild #{@name}.sln #{tgt_string} /p:Configuration=#{@mode} /p:Platform=x64 /m"
                    Dir.chdir(File.join(@dir, "Builds/VisualStudio#{VISUAL_STUDIO_YEAR}")) { Rake.sh(cmd) }
                else
                    raise "No way to build #{@name} for OS #{OS.get}"
                end

                fn = target
                puts "<<< done: #{fn}"

                return fn
            end


            def clean(complete = true)
                puts ">>> Cleaning target #{@name}"
                FileUtils.rm_rf(File.join(@dir, "Builds"))
                Dir["Builds/*/build"].each { |f| FileUtils.rm_rf(f) }
                puts "<< Done"
            end

            private
        end

        @@projucer = nil
        
        def self.projucer()
            return $lw_projucer if $lw_projucer
            unless @@projucer
                dir = File.join(fetch_sdk(:juce), "extras", "Projucer")
                projucer = LW::Juce::Instance.new("Projucer", dir)
                case OS.get
                when :mac
                    projucer.primary_target = "Projucer.app/Contents/MacOS/Projucer"
                when :windows
                    projucer.primary_target = "App/Projucer.exe"
                end
                @@projucer = projucer.build
            end
            @@projucer
        end


        def self.repo_dir(key = nil)
            @repo_dir = File.join(Build.root, "extern/repo") unless @repo_dir
            dir = @repo_dir
            dir = File.join(dir, "#{key}") if key
            dir
        end

        def self.fetch_sdk(key)
    
            if $lw_sdk_dir && $lw_sdk_dir[key]
                return $lw_sdk_dir[key]
            end

            repos = {
                juce: "https://github.com/francoisbecker/JUCE.git",
                vst3sdk: "https://github.com/steinbergmedia/vst3sdk.git"
            }
            raise "unknown repo #{key}" unless repos.key?(key)

            dir = repo_dir(key)
            if !File.exist?(dir)
                FileUtils.mkdir_p(dir)
                Dir.chdir(dir) do
                    url = repos[key]
                    `git clone #{url} --recurse-submodules ./`
                end
            end
            dir
        end

        def self.clean
            FileUtils.rm_rf repo_dir 
        end
    end
end
