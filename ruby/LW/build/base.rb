
def update_load_paths()
  Dir[File.join("**", "ruby")].select{ |d| File.directory?(d) }.each do |d|
    $LOAD_PATH << d
  end
end

update_load_paths()

require('rake')
require('LW/build')
require('LW/tasks')

