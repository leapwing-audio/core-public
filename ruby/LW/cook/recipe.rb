require 'pathname'

module Cook
    class Recipe

        attr_accessor :uri, :tag, :display_name, :path, :type, :build_target, :target_name, :target_type, :target_fn, :ingredients, :dependencies

        def initialize()
            @ingredients = []
            @dependencies = []
        end

        def from_naft(parser)
            while !parser.all_attributes_finished()
                k,v = parser.pop_attribute()
                case k.to_sym
                when :uri           then @uri = v
                when :tag           then @tag = v
                when :display_name  then @display_name = v
                when :path          then @path = v
                when :type          then @type = v.to_sym
                when :build_target  then @build_target = v
                else                raise "Unexpected attribute #{k}"
                end
            end

            return unless parser.pop_child_open
            while !parser.pop_child_close
                tag = parser.pop_tag

                case tag.to_sym
                when :target        then from_naft_target(parser)
                when :file          then from_naft_file(parser)
                when :key_value     then from_naft_key_value(parser)
                when :dependency    then from_naft_dependency(parser)
                else                raise "Unexpected tag #{tag}"
                end
            end
        end

        def merge(recipe)
            src = Pathname.new(recipe.path)
            dst = Pathname.new(self.path)

            ingr = recipe.ingredients.select do |i| 
                i[:owner] == recipe.uri && i[:user_generated]
            end

            @ingredients += 
                ingr.map do |i|
                    i = i.clone
                    if i[:type] == :file
                        dir = Pathname.new(i[:dir])
                        if dir.relative?
                            i[:dir] = (src+dir).relative_path_from(dst).to_s
                        end
                    end
                    i
                end
        end

        private
        def from_naft_target(parser)

            while !parser.all_attributes_finished()
                k,v = parser.pop_attribute()
                case k.to_sym
                when :type      then @target_type = v.to_sym
                when :filename  then @target_fn = v
                when :name      then @target_name = v
                else            raise "Unexpected attribute #{k}"
                end
            end


            if parser.pop_child_open
                raise "This tag should have not children" unless parser.pop_child_close
            end
        end

        def from_naft_file(parser)
            file = {type: :file}
            while !parser.all_attributes_finished()
                k,v = parser.pop_attribute()
                case k.to_sym
                when :Type              then file[:Type] = v.to_sym
                when :Language          then file[:Language] = v.to_sym
                when :Propagation       then file[:Propagation] = v.to_sym
                when :Overwrite         then file[:Overwrite] = v.to_sym
                when :content           then file[:Content] = v.to_sym
                when :user_generated    then file[:user_generated] = (v == "0" ? false : true)
                when :owner             then file[:owner] = v
                when :dir               then file[:dir] = v
                when :rel               then file[:rel] = v
                else raise "Unexpected attribute #{k}"
                end
            end
            if parser.pop_child_open
                raise "This tag should have not children" unless parser.pop_child_close
            end

            ingredients << file
        end

        def from_naft_key_value(parser)
            key_value = {type: :key_value}
            while !parser.all_attributes_finished()
                k,v = parser.pop_attribute()
                case k.to_sym
                when :Type              then key_value[:Type] = v.to_sym
                when :Language          then key_value[:Language] = v.to_sym
                when :Propagation       then key_value[:Propagation] = v.to_sym
                when :Overwrite         then key_value[:Overwrite] = v.to_sym
                when :content           then key_value[:Content] = v.to_sym
                when :user_generated    then key_value[:user_generated] = (v == "0" ? false : true)
                when :owner             then key_value[:owner] = v
                when :key               then key_value[:key] = v
                when :value             then key_value[:value] = v
                else raise "Unexpected attribute #{k}"
                end
            end
            if parser.pop_child_open
                raise "This tag should have not children" unless parser.pop_child_close
            end

            ingredients << key_value
        end

        def from_naft_dependency(parser)
            dependency = {}
            while !parser.all_attributes_finished()
                k,v = parser.pop_attribute()
                case k.to_sym
                when :key               then dependency[:key] = v
                when :resolved          then dependency[:resolved] = (v == "0" ? false : true)
                when :uri               then dependency[:uri] = v
                else raise "Unexpected attribute #{k}"
                end
            end
            if parser.pop_child_open
                raise "This tag should have not children" unless parser.pop_child_close
            end

            dependencies << dependency
        end
    end
end
