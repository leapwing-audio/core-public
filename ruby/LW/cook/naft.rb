require('LW/cook/parse')
require('LW/cook')
require('LW/naft')

module Cook
    class Naft
        def initialize(naft_file = nil, va = { build_dir: nil })
            @recipes = {}
                
            if naft_file
                build_dir = va[:build_dir] || File.dirname(naft_file)
                str = File.read(naft_file).to_s
                parser = ::Naft::Parse.new(str)
                @recipes = Cook.parse(parser, build_dir)
            end
            
        end

        def all_recipes()
            @recipes.keys()
        end

        def match_uri_or_glob(uri_or_glob)
            uri_or_glob = "/#{uri_or_glob}" unless uri_or_glob.start_with? '/'
            @recipes.keys.select { |k| File.fnmatch?(uri_or_glob, k) }
        end

        def find_recipe(uri)
            uri = "/#{uri}" unless uri.start_with? '/'
            rcp = @recipes[uri]
            raise "Unknown uri #{uri}" unless rcp
            rcp
        end

        def find_all_dependencies(uri)
            find_recipe(uri)

            result = {}
            todo = [uri]

            while !todo.empty?
                cur = todo.pop()
                rcp = find_recipe(cur)
                raise "Unknown uri #{cur}" unless rcp

                if !result[cur]
                    result[cur] = rcp
                    todo += rcp.dependencies.map {|d| d[:key]}.to_a
                end
            end

            result
        end
    end
end
