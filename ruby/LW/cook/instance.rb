require('LW/build')
require('LW/cook/naft')
require('LW/cook')
require('LW/executable')

module Cook
    class Instance
        attr_reader :mode
        attr_reader :additional_recipes

        def initialize(mode, additional_recipes: nil, extra_toolchains: nil, options: nil)
            @additional_recipes = [additional_recipes].flatten.compact
            @extra_toolchains = [extra_toolchains].flatten.compact
            @extra_toolchains = ["default"] if @extra_toolchains.empty?
            @options = [options].flatten.compact
            @mode = [mode].flatten
            @build_dir = File.join(Build.root, "build", @mode.join("-"))
            FileUtils.mkdir_p(@build_dir) unless File.exist?(@build_dir)
        end

        def build_dir(fn = nil)
            dir = @build_dir
            dir = File.join(dir, fn) if fn
            dir
        end

        def cmd(args)
            ar = [Cook.cook_exec]
            ar << "-o" << "#{@build_dir}"
            
            toolchain_file = File.join(Build.root, "core-public/cook/toolchain/leapwing.chai")

            ar += (@extra_toolchains + [toolchain_file]).map { |t| ["-t", "#{t}"] }.flatten
            ar += @mode.map { |m| ["-T", "#{m}"] }.flatten
            ar += @options.map { |t| ["-T", "#{t}"] }.flatten

            args = [args].flatten
            ar += ([File.join(Build.root, "recipes.chai")] + @additional_recipes).map { |f| ["-f", "#{f}"] }.flatten
            ar += args

            return ar.join(" ")
        end

        def naft()
            unless @naft
                success = true
                Rake.sh(cmd("-g naft")) { |ok, res| success = ok }
                if success
                    @naft = Naft.new(build_dir("recipes.naft"))
                else
                    @naft = Naft.new
                end
            end
            @naft
        end

        def build(uris_or_globs = nil)

            all_uris = []
            [uris_or_globs].flatten.each do |r|
                uris = naft.match_uri_or_glob(r)
                raise("No match for expression #{r}") if uris.empty?
                all_uris += uris
            end

            all_uris.uniq!

            Rake.sh(cmd("-g ninja " + all_uris.join(" ")))

            ninja_fn = File.join(build_dir)
            Rake.sh "#{Cook.ninja_exec} -f #{build_dir('build.ninja')}"


            map = {}
            all_uris.each do |uri|
                rcp = naft.find_recipe(uri)
                
                map[uri] = if rcp.target_fn && !rcp.target_fn.empty?
                             if rcp.target_type == :Executable
                               LW::Executable.new(rcp.target_fn)
                             else
                               rcp.target_fn
                             end
                           else
                             nil
                           end
            end

            return map
        end

        def build_single(uri_or_glob)
          uris = naft.match_uri_or_glob(uri_or_glob)
          raise("Expression #{uri_or_glob} matches #{uris.size} recipe(s), expected 1") if uris.size !=1 
          build(uris).values[0]
        end
    end
end

