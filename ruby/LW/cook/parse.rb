require('LW/cook/recipe')

module Cook
    def self.parse(parser, output_dir = nil)
        map = {}
        while (tag = parser.pop_tag)
            case tag.to_sym
            when :configuration then parse_configuration(parser)
            when :structure then parse_structure(parser, map)
            else raise "Unexpected tag #{tag}"
            end
        end

        if output_dir
            map.each do |k,v| 
                v.target_fn = File.expand_path(v.target_fn, output_dir)  if v.target_fn && !v.target_fn.empty?
            end
        end

        return map
    end

    private
    def self.parse_configuration(parser)
        while !parser.all_attributes_finished()
            k,v = parser.pop_attribute()
            raise "Unexpected attribute #{k}"
        end

        return unless parser.pop_child_open
        while !parser.pop_child_close
            tag = parser.pop_tag

            case tag.to_sym
            when :config then parse_config(parser)
            else raise "Unexpected tag #{tag}"
            end
        end
    end

    def self.parse_config(parser)
        while !parser.all_attributes_finished()
            k,v = parser.pop_attribute()
            case k.to_sym
            when :key           
            when :value         
            when :resolved      
            else raise "Unexpected attribute #{k}"
            end
        end



        return unless parser.pop_child_open
        while !parser.pop_child_close
            raise "Unexpected tag #{parser.pop_tag}"
        end
    end

    def self.parse_structure(parser, map)
        while !parser.all_attributes_finished()
            k,v = parser.pop_attribute()
                raise "Unexpected attribute #{k}"
        end

        return unless parser.pop_child_open
        while !parser.pop_child_close
            tag = parser.pop_tag

            case tag.to_sym
            when :book then parse_book(parser, map)
            else raise "Unexpected tag #{tag}"
            end
        end

    end

    def self.parse_book(parser, map)
        while !parser.all_attributes_finished()
            k,v = parser.pop_attribute()
            case k.to_sym
            when :uri           
            when :display_name  
            else raise "Unexpected attribute #{k}"
            end
        end

        return unless parser.pop_child_open

        while !parser.pop_child_close
            tag = parser.pop_tag

            case tag.to_sym
            when :book then parse_book(parser, map)
            when :recipe        
                r = Recipe.new()
                r.from_naft(parser)
                map[r.uri] = r
            else raise "Unexpected tag #{tag}"
            end
        end
    end
end
