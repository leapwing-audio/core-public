require 'nokogiri'
require 'digest'
require 'LW/build/protect'

module LW
    module Juce
        def self.create_with_cook(src_fn, recipe, dst_fn)
                
			puts ">>> Creating jucer file #{dst_fn}"
            
            Build.protect(src_fn) do
                insert_cook_recipe_(src_fn, recipe, dst_fn)
            end
            
            Build.protect(File.dirname(dst_fn)) do

                Rake.sh(LW::Juce.projucer, "--resave", dst_fn)


                # post-update the visual studio files
                Dir[File.join(File.dirname(dst_fn), "Builds", "VisualStudio*", "*.vcxproj")].each do |fn|
                    doc = File.open(fn) {|f| Nokogiri::XML(f) }

                    doc.xpath("//xmlns:ItemDefinitionGroup/*/xmlns:ObjectFileName").each do 
                        |f| f.children.remove
                        f.add_child("$(IntDir)\\a\\b\\c\\%(RelativeDir)\\")
                    end

                    File.write(fn, doc.to_xml)
                end
            end

            puts "<<< done: #{dst_fn}"
        end

        private

        
        def self.insert_cook_recipe_(src_fn, recipe, dst_fn)

            doc = File.open(src_fn) {|f| Nokogiri::XML(f) }
            cook_tgt = doc.xpath("//MAINGROUP/*/GROUP[@name='cook']")
            raise "Expected a single GROUP with name 'cook', got #{cook_tgt.size}" unless cook_tgt.size == 1
            node = cook_tgt[0]
            node.children.remove


            defines = {}
            include_dirs = {}
            library_dirs = {}
            libraries = {}

            files = []

            recipe.ingredients.each do |f|

                if [:Source, :Header].include? (f[:Type])

                    include_dirs[f[:dir]] = 1 if f[:Type] == :Header
                    files << platform_independent_file_(f)
                elsif f[:Type] == :IncludePath
                    include_dirs[f[:dir]] = 1
                else
                    # puts "Skipping #{f}"
                end
            end


            files.sort! { |a,b| "#{a[:dir]}#{a[:rel]}" <=> "#{b[:dir]}#{b[:rel]}" }

            files.each do |f|
                n = node.add_child("<FILE />\n")[0]
                n['id'] = make_juce_id(f)
                n['name'] = File.basename(f[:rel])
                n['compile'] = f[:Type] == :Source ? 1 : 0
                n[:resource] = 0
                n[:file] = File.join(f[:dir], f[:rel].gsub("\\", "/"))
            end

            include_dirs.keys().each do |k|
                if Pathname.new(k).relative?
                    newp = (Pathname.new("../../") + k)
                    include_dirs[newp.to_s] = include_dirs.delete(k)
                end
            end


            doc.xpath("//EXPORTFORMATS/*").each do |exp|
                exp.xpath("//CONFIGURATION").each do |cfg|
                    paths = {}
                    str = cfg[:headerPath] || ""
                    str.split("\n").each {|p| paths[p] = 1 }
                    paths.merge!(include_dirs)

                    cfg[:headerPath] = paths.keys().join("\n")
                end
            end

            File.write(dst_fn, doc.to_xml)

        end

        private
        def self.platform_independent_file_(file)
            [:dir, :rel].each { |k| file[k] = file[k].gsub("\\", "/") }
            file
        end
        def self.make_juce_id(file)
            chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"
            id = "a"*6

            md5 = Digest::MD5.new
            md5.update file[:dir]
            md5.update file[:rel]
            seed = md5.hexdigest.to_i(16)

            rnd = Random.new(seed)

            id[0] = chars[rnd.rand(52)]
            (1..5).each {|idx| id[idx] = chars[rnd.rand(62)] }

            id
        end
    end
end
