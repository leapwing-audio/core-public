extras = []
mode = :unknown

# find the position for keyword rake
rake_idx = ARGV.index("--rake")
raise "USAGE: ruby #{__FILE__} <requires>* --rake <file> <task> <args>*" unless rake_idx

requires = ARGV.shift(rake_idx)
ARGV.shift(1)
file = ARGV.shift
task = ARGV.shift
args = ARGV

requires.each do |req| 
    if File.exist?(req)
	require File.absolute_path(req) 
    else
        require req
    end
end
require 'rake'
load file
Rake::Task[task].invoke(args) 
