require 'open3'

module LW
  class Executable
    attr_accessor :cmd
    def initialize(cmd)
      @cmd = cmd
    end
  
    def all_eof(files)
      files.find { |f| !f.eof }.nil?
    end
      
    BLOCK_SIZE = 1024

    def execute(args = nil, na = { output: true, may_fail: false })

      output = na[:output] || true
      may_fail = na[:may_fail] || false
      if args.is_a?(Hash)
        args = Executable.hash_to_array(args)
      elsif args.is_a?(String)
        args = args.split(" ")
      end

      puts "Executing #{@cmd} #{args.join(" ")}" if output


      str = StringIO.new
      status = Open3.popen3(@cmd, *args) do |stdin, stdout, stderr, thread|
        stdin.close_write

        begin
          files = [stdout, stderr]

          until all_eof(files) do
            ready = IO.select(files)

            if ready
              readable = ready[0]
              # writable = ready[1]
              # exceptions = ready[2]

              readable.each do |f|
                fileno = f.fileno

                begin
                  data = f.read_nonblock(BLOCK_SIZE)
                  puts data if output
                  str.puts data
                  yield(data) if block_given?

                rescue EOFError => e
                end
              end
            end
          end
        rescue IOError => e
          puts "IOError: #{e}"
        end

        thread.value
      end

      is_ok = status.exitstatus == 0
      if !is_ok && !may_fail
        puts str.string if !output
        fail
      end

      str.string
    end

    def self.hash_to_array(h, add_dash: true)
      h.map do |k,v|

        key = "#{k}"
        if add_dash
          if key.size == 1
            key = "-#{key}"
          elsif key.size > 1
            key = "--#{key}"
          end
        end
      
        res = []
        add = lambda { |v| res << key; res << "#{v}" unless v.nil? }
        if v.is_a?(Array)
          v.each {|e| add.call(e) }
        else
          add.call(v)
        end
        res

      end.flatten
    end
  end
end
