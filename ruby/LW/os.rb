module OS
    def OS.windows?
        (/cygwin|mswin|mingw|bccwin|wince|emx/ =~ RUBY_PLATFORM) != nil
    end

    def OS.mac?
        (/darwin/ =~ RUBY_PLATFORM) != nil
    end

    def OS.linux?
        !OS.windows? and not OS.mac?
    end

    :windows
    :mac
    :linux

    def self.get
        if windows?
            :windows
        elsif mac?
            :mac
        elsif linux?
            :linux
        else
            raise "Unknown operating system"
        end
    end
end

module Arch
  def self.i686()
    not (/i686/ =~ RUBY_PLATFORM).nil?
  end
  
  def self.x86_64()
    not (/ix86_64/ =~ RUBY_PLATFORM).nil?
  end
  
  def self.arm64()
    not (/arm64/ =~ RUBY_PLATFORM).nil?
  end

  :i686
  :x86_64
  :arm64

  def self.get
    if x86_64
      :x86_64
    elsif arm64
      :arm64
    elsif i686
      :i686
    else
      # fallback
      :i686
    end
  end
end

