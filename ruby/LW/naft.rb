module Naft
    class Parse

        :child_open
        :child_close
        :tag
        :attribute
        :end

        attr_reader :state

        def initialize(data)
            @data = data
            @state = :child_open
            find_next_()
        end

        def pop_tag()
            return nil unless @state == :tag

            # find the position of the closing tag
            pos = find_end_pos_('[', ']')
            return nil unless pos

            # extract the string
            str = @data[1...pos]

            
            # and remove from our data
            @data = @data[pos+1..-1]
            find_next_()


            return str
        end

        def pop_attribute()
            return nil unless @state == :attribute
            #find the position of the closing bracket
            pos = find_end_pos_('(', ')')
            return nil unless pos

            # extract the string
            str = @data[1...pos]
            
            # and remove from our data
            @data = @data[pos+1..-1]
            find_next_()

            sep = str.index(':')
            if sep
                return str[0...sep], str[sep+1..-1]
            else
                return str, nil
            end
        end

        def all_attributes_finished()
            return @state != :attribute
        end

        def pop_child_open()
            return false unless @state == :child_open
            @data = @data[1..-1]
            find_next_()
            return true
        end
        
        def pop_child_close()
            return false unless @state == :child_close
            @data = @data[1..-1]
            find_next_()
            return true
        end

        def pop_tag_if(name)
            return false unless @state == :tag

            # find the position of the closing tag
            pos = find_end_pos_('[', ']')
            return false unless pos

            # extract the string
            str = @data[1...pos]
            return false unless str == name

            # and remove from the data
            @data = [pos+1..-1]
            find_next_()
            return true
        end

        private 
        def find_end_pos_(open, close)
            idx = 1
            count = 1
            while idx < @data.size
                count += 1 if @data[idx] == open
                count -= 1 if @data[idx] == close
                return idx if count == 0

                idx += 1
            end

            return nil
        end

        def find_next_()
            chars = {}
            chars['['] = :tag
            chars['}'] = :child_close

            case @state
            when :child_open, :child_close
            when :tag, :attribute
                chars['{'] = :child_open
                chars['('] = :attribute
            else
                return
            end

            idx = 0
            while idx < @data.size
                c = @data[idx]
                if chars.has_key?(c)
                    @state = chars[c]
                    @data = @data[idx..-1]
                    return
                end

                idx += 1
            end

            @state = :end
        end
    end
end
