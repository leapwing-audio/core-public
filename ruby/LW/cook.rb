require('LW/build')
require('LW/cook/instance')
require('LW/naft')

module Cook
    def self.cmd_path(cmd)
        if OS.windows?
            dir = File.join("windows", "x86")
            postfix = ".exe"
        elsif OS.mac?
            dir = File.join("macos", "x64")
            postfix = ""
        elsif OS.linux?
            dir  =File.join("linux")
            postfix = ""
        end

        case cmd
        when :cook
            return File.join(Build.root, "extern", "cook-binary", "latest", dir, "cook#{postfix}")
        when :ninja
            return File.join(Build.root, "extern", "cook-binary", "ninja", dir, "ninja#{postfix}")
        end
    end

    def self.cook_exec()
        return cmd_path(:cook)
    end

    def self.ninja_exec()
        return cmd_path(:ninja)
    end
    
    

end

