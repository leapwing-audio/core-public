# location of the build directory
module Build
    @@root = nil
    @@dir = nil
    def self.root(*parts, file: nil)
        unless @@root
            @@root = File.dirname(File.dirname(File.dirname(File.dirname(__FILE__))))
        end

        all = [@@root] + parts + [file]
        all.compact!

        File.join(all)
    end

    def self.dir(fn = nil)
        unless @@dir
            @@dir = root("build")
            FileUtils.mkdir_p(@@dir) unless File.exist?(@@dir)
        end
        dir = @@dir
        dir = File.join(dir, fn) if fn
        dir
    end

    def self.mkdir(*parts, file: nil, clean: false)
        dir = File.join(*parts)
        res = dir

        FileUtils.rm_rf(dir) if clean and File.exist? dir

        if file
            res = File.join(dir, file)
            dir = File.dirname(res)
        end

        FileUtils.mkdir_p(dir)
        res
    end

    def self.deploy()
        d = root("deploy")
        FileUtils.mkdir_p(d)
        d
    end

    def self.generated_dir(*dirs, file: nil, clean: false)
      Build.mkdir(Build.root, "generated", *dirs.map { |d| d.to_s }, file: file, clean: clean)
    end

    def self.story_dir(*parts, file: nil, story_file: nil)
      story_file = story_file || caller_locations.first.path
      r = File.join(File.dirname(story_file), File.basename(story_file, ".*"), *parts)
      r = File.join(r, file) if file
      r
    end
end

