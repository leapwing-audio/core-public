require 'LW/executable'

module LW
  module Wav
    class ComparisonError < StandardError
      def initialize(fnA, fnB, mix, res, str)
        @fnA = fnA
        @fnB = fnB
        @mix = mix
        @res_string = res.map{|k,v| "#{k}:#{v}"}.join(", ")

        puts "Comparison:"
        puts " + A:   #{@fnA}"
        puts " + B:   #{@fnB}"
        puts " + mix: #{@mix}"
        super("Comparison failed: #{str}'")
      end
    end

    class Comparison
      def initialize(fileA, fileB, verbose: false)
        @fileA = fileA
        @fileB = fileB
        @verbose = verbose
      end

      def process(compare_key: "RMS amplitude", compare_value: 0)
        mix = substract_()
        res = analyse_(mix)

        if @verbose
          puts "Comparison: #{compare_key} #{compare_value}"
          puts " * #{@fileA}"
          puts " * #{@fileB}"
          puts " * #{compare_key}: #{res[compare_key]}"
        end

        ok = true
        if block_given?
          ok = !yield(res)
        elsif compare_key != nil
          ok = res[compare_key] <= compare_value
        end

        raise ComparisonError.new(@fileA, @fileB, mix, res, "#{compare_key}: #{res[compare_key]} <= #{compare_value} failed") unless ok
        res
      end

      def tmp_(file)
        require 'digest'
        md5 = Digest::MD5.new
        md5.update @fileA
        md5.update @fileB
        d = md5.hexdigest
        Build.generated_dir("wav-compare", d, file: file)
      end

      def substract_()
        timestamp = Time.now.to_i
        fnA_inv = tmp_("inverted.wav")
        mix = tmp_("mix.wav")
        
        exe = LW::Executable.new("sox")
        exe.execute(["-v", "-1", @fileA, fnA_inv])
        exe.execute(["-m", @fileB, fnA_inv, mix])

        mix
      end

      def analyse_(fn)
        exe = LW::Executable.new("sox")
        output = exe.execute([fn, "-n", "stat"], output: false)

        res = {}

        elements = output.split("\n").map { |l| l.split(":") }.select { |ar| ar.size == 2 }
        elements.each do |ar|
          k = ar[0].strip
          v = ar[1].strip
          k = k.gsub(/\s+/, " ")
          res[k] = v.to_f
        end

        res
      end
    end

    def self.compare(fnA, fnB, **kv_args)
      Comparison.new(fnA, fnB).process(**kv_args)
    end
  end
end
