module LW
  class Task
    attr_reader :type
    attr_reader :name
    attr_reader :tags
    attr_accessor :file
    attr_accessor :result
    attr_accessor :error
    attr_accessor :time

    def initialize(type, name, *tags)
      @type = Task.canonic_(type)
      @name = name
      @tags = tags.map { |t| Task.canonic_(t) }
    end

    def run()
      raise "No run implemented in #{file}"
    end

    def matches(*other_tags)
      other_tags.map { |t| Task.canonic_(t) }.all? { |t| t == type || tags.include?(t) }
    end

    private
    def self.canonic_(t)
      t.to_s.downcase
    end
  end

  @@tasks = []

  def self.register_task(obj)
    obj.file = obj.file || caller_locations.first.absolute_path
    @@tasks << obj
  end

  def self.get_tasks(*tags)
    @@tasks.select { |t| t.matches(*tags) }
  end

  def self.execute(tasks)
    all_ok = true
    tasks.each do |t|
      start = Time.now
      begin
        res = t.run()
        raise "Execution error" if res != nil && res != true
      rescue Exception => e
        t.result = false
        t.error = e
        all_ok = false
      else
        t.result = true
        t.error = nil
      end

      t.time = Time.now - start
    end
    all_ok
  end
end
