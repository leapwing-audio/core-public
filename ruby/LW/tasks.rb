require 'LW/build'
require 'LW/task'

STORY_DIR = File.join(Build.root(), "story")
STORY_RUNNER = File.join(File.dirname(__FILE__), "run.rb")

task :story, [:filter] do |t,args|
  candidates = Dir[File.join(STORY_DIR, "*.rb")].map { |fn| File.basename(fn, ".rb") }
  fltr = args[:filter]

  res = []
  res = candidates.select{ |fn| fn.end_with?(fltr) }.flatten  if fltr != nil

  if fltr == nil
    puts "No filter given"
    abort
  elsif res.size == 0
    puts "No story match the given filter. Possible candidates are:"
    candidates.each { |cnd| puts " + #{cnd}" }
    abort
  elsif res.size > 1
    puts "Multiple stories match the given filter. Possible candidates are:"
    res.each { |cnd| puts " + #{cnd}" }
    abort
  end

  sh "ruby #{STORY_RUNNER} configure.rb --rake #{STORY_DIR}/#{res[0]}.rb story:run"
end

def load_(type)
  Dir.chdir(Build.root) do
    Dir.glob("**/#{type}/*.rb").each do |fn|
      require(Build.root(fn))
    end
  end
end

task_types = [:qc]
task_types.each do |type|
  desc "Run #{type.to_s} tasks"
  task type.to_s, [:args] do |t, args|
    # all tags
    tags = [type.to_s] + args.to_a
    # load the files
    load_(type.to_s)

    tasks = LW.get_tasks(*tags)
    res = LW.execute(tasks)

    puts "The following #{tasks.size} tasks were executed:"
    tasks.each do |t|
      puts " * #{t.name} (#{t.file}): #{t.result ? "OK": "Failure"} (%.4f s)" % (t.time)
      if t.error
        puts "     #{t.error}"
        t.error.backtrace.each { |l| puts "     #{l}" }
        # t.error.backtrace.to_s.split("\n").each { |l| puts "    #{l}" }
        puts ""
      end
    end

    raise "#{type} failure" unless res
  end
  
  namespace type.to_s do

    desc "List all #{type.to_s} tasks"
    task :list, [:args] do |t, args|
      # all tags
      tags = [type.to_s] + args.to_a
      # load the files
      load_(type.to_s)

      tasks = LW.get_tasks(*tags)
      puts "The following #{tasks.size} tasks match the supplied tags:"
      tasks.each do |t|
        puts " * #{t.name}: #{t.file} [#{t.tags.join(" | ")}]"
      end
    end
  end
end
