/*
MIT License

Copyright (c) 2019 D. Grine

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#ifndef HEADER_lw_argo_Argo_hpp_INCLUDE_GUARD
#define HEADER_lw_argo_Argo_hpp_INCLUDE_GUARD

#ifndef HEADER_lw_argo_Configuration_hpp_INCLUDE_GUARD
#define HEADER_lw_argo_Configuration_hpp_INCLUDE_GUARD

#ifndef HEADER_lw_argo_core_config_build_hpp_INCLUDE_GUARD
#define HEADER_lw_argo_core_config_build_hpp_INCLUDE_GUARD



#define ARGO_NS(x) lw::argo::x
#ifdef ARGO_BUILD_STATIC_LIBRARY
#define ARGO_INLINE 
#else
#define ARGO_INLINE inline
#endif

#endif
#ifndef HEADER_lw_argo_program_Info_hpp_INCLUDE_GUARD
#define HEADER_lw_argo_program_Info_hpp_INCLUDE_GUARD

#ifndef HEADER_lw_argo_program_Description_hpp_INCLUDE_GUARD
#define HEADER_lw_argo_program_Description_hpp_INCLUDE_GUARD

#include <string>
#include <cstdint>
#include <optional>

namespace lw { namespace argo {

namespace program {

//!Holds program descriptions: goal and intended usage.
struct Description
{
    std::string brief;    //!<Short description: a one-liner describing the program's goal.
    std::string extended; //!<Long description which may span multiple lines.
    std::string usage;    //!<Describes the program's usage, i.e. its argument invocation. When empty, the output formatters are responsible for generating this information based on the available handlers. However, when the string is not empty, output formatters should use this instead.
};

} // namespace program

} }

#endif

#ifndef HEADER_lw_argo_program_Name_hpp_INCLUDE_GUARD
#define HEADER_lw_argo_program_Name_hpp_INCLUDE_GUARD


namespace lw { namespace argo {

namespace program {

//!Holds the program's different name aliases.
struct Name
{
    std::string brief;    //!<Short name of the program. For example: gcc.
    std::string extended; //!<Long name of the program. For example: GNU Compiler Collection.
};

} // namespace program

} }

#endif

#ifndef HEADER_lw_argo_program_Version_hpp_INCLUDE_GUARD
#define HEADER_lw_argo_program_Version_hpp_INCLUDE_GUARD


namespace lw { namespace argo {

namespace program {

//!Holds the program's version information.
struct Version
{
    unsigned major;      //!<Major version number.
    unsigned minor;      //!<Minor version number.
    unsigned patch;      //!<Patch version number.
    std::string githash; //!<Git revision hash.

    //!Indicates if the version is set.
    bool is_set() const { return major + minor + patch; }
};

} // namespace program

} }

#endif


namespace lw { namespace argo {

namespace program {

//!Aggregate of program-related information.
struct Info
{
    Name name;               //!<Program name.
    Description description; //!<Program description.
    Version version;         //!<Program version information.
    std::string copyright;   //!<Copyright information.
};

} // namespace program

} }

#endif


namespace lw { namespace argo {

//! Configuration is a structure containing parser behavior and program
//! information.
struct Configuration {
    program::Info program{};  //!< Program information.
    struct Parser {
        bool help = true;             //!< Addition of the `--help` flag.
        bool version = true;          //!< Addition of the `--version` flag.
        bool responsefile = true;     //!< Addition of the default response file handler.
        bool dash_names = true;       //!< Named arguments on the command line have underscores turned to dashes.
        bool implicit_values = true;  //!< When false, values must explicitly be set, e.g. `--option a --option b`, instead of `--option a b`
        bool argument_values = false; //!< When true, values can be of the form `--value` or `-v`
        bool empty_values = false;    //!< When true, values can be empty
    } parser;
};

} }

#endif

#ifndef HEADER_lw_argo_ReturnCode_hpp_INCLUDE_GUARD
#define HEADER_lw_argo_ReturnCode_hpp_INCLUDE_GUARD

#include <cassert>
#include <ostream>

namespace lw { namespace argo {

//!Enumeration class that holds the three end states of the parsing process.
enum class ReturnCode
{
    Error = 0,             //!<Indicates an error occurred during parsing. The intent is to signal the application to handle the error and exit.
    SuccessAndAbort = 1,   //!<Indicates parsing was successful. The intent is to signal the application that although no error occurred, it should still abort. Usually used by flags like `--help`.
    SuccessAndContinue = 2 //!<Indicates parsing was successful. The intent is to signal the application that it may proceed with normal execution.
};

inline std::ostream &operator<<(std::ostream &os, const ReturnCode &rc)
{
    switch (rc)
    {
    case ReturnCode::Error:
        os << "Error";
        break;
    case ReturnCode::SuccessAndAbort:
        os << "SuccessAndAbort";
        break;
    case ReturnCode::SuccessAndContinue:
        os << "SuccessAndContinue";
        break;
    default:
        assert(false);
        break;
    }
    return os;
}

} }

#endif

#ifndef HEADER_lw_argo_ResponseFile_hpp_INCLUDE_GUARD
#define HEADER_lw_argo_ResponseFile_hpp_INCLUDE_GUARD

#ifndef HEADER_lw_argo_core_mss_hpp_INCLUDE_GUARD
#define HEADER_lw_argo_core_mss_hpp_INCLUDE_GUARD

#ifndef HEADER_lw_argo_core_config_toolset_hpp_INCLUDE_GUARD
#define HEADER_lw_argo_core_config_toolset_hpp_INCLUDE_GUARD

//Compiler
#if defined(__clang__)
#define ARGO_TOOLSET_COMPILER "clang"
#define ARGO_TOOLSET_COMPILER_CLANG 1
#define ARGO_TOOLSET_COMPILER_VERSION_MAJOR __clang_major__
#define ARGO_TOOLSET_COMPILER_VERSION_MINOR __clang_minor__
#define ARGO_TOOLSET_COMPILER_VERSION_PATCH __clang_patchlevel__
#elif defined(__GNUC__)
#define ARGO_TOOLSET_COMPILER "gcc"
#define ARGO_TOOLSET_COMPILER_GCC 1
#define ARGO_TOOLSET_COMPILER_VERSION_MAJOR __GNUC__
#define ARGO_TOOLSET_COMPILER_VERSION_MINOR __GNUC_MINOR__
#define ARGO_TOOLSET_COMPILER_VERSION_PATCH __GNUC_PATCHLEVEL__
#if ARGO_TOOLSET_COMPILER_VERSION_MAJOR < 5
#define ARGO_TOOLSET_COMPILER_GCC_OLD 1
#endif
#elif defined(_MSC_VER)
#define ARGO_TOOLSET_COMPILER "msvc"
#define ARGO_TOOLSET_COMPILER_MSVC 1
#if _MSC_VER >= 1910 // (MSVC++ 15.0 - Visual Studio 2017)
#define ARGO_TOOLSET_COMPILER_MSVC_VS_2017 1
#elif _MSC_VER == 1900 // (MSVC++ 14.0 - Visual Studio 2015)
#define ARGO_TOOLSET_COMPILER_MSVC_VS_2015 1
#elif _MSC_VER == 1800 // (MSVC++ 12.0 - Visual Studio 2013)
#define ARGO_TOOLSET_COMPILER_MSVC_VS_2013 1
#else
#error "Unsupported toolset: unsupported Visual Studio version"
#endif
#else
#error "Unsupported toolset: unknown compiler"
#endif

//Operating system
#if defined(__ANDROID__)
#define ARGO_TOOLSET_PLATFORM "android"
#define ARGO_TOOLSET_PLATFORM_ANDROID 1
#elif defined(__linux__)
#define ARGO_TOOLSET_PLATFORM "linux"
#define ARGO_TOOLSET_PLATFORM_LINUX 1
#define ARGO_TOOLSET_PLATFORM_POSIX 1
#elif defined(__APPLE__)
#define ARGO_TOOLSET_PLATFORM "apple"
#define ARGO_TOOLSET_PLATFORM_APPLE 1
#define ARGO_TOOLSET_PLATFORM_POSIX 1
#elif defined(_MSC_VER) || defined(_WIN32) || defined(_WIN64) || defined(__CYGWIN__)
#define ARGO_TOOLSET_PLATFORM "windows"
#define ARGO_TOOLSET_PLATFORM_WINDOWS 1
#else
#error "Unsupported toolset: unknown platform"
#endif

//Architecture
#if defined(__amd64__)
#define ARGO_TOOLSET_ARCH "amd64"
#define ARGO_TOOLSET_ARCH_X86 1
#define ARGO_TOOLSET_ARCH_AMD 1
#define ARGO_TOOLSET_ARCH_BITS 64
#define ARGO_TOOLSET_ARCH_BITS_64 1
#define ARGO_TOOLSET_ARCH_LITTLE_ENDIAN 1
#elif defined(__X86__) || defined(__x86__) || defined(__i386__) || defined(_M_IX86)
#define ARGO_TOOLSET_ARCH "x86"
#define ARGO_TOOLSET_ARCH_X86 1
#define ARGO_TOOLSET_ARCH_INTEL 1
#define ARGO_TOOLSET_ARCH_BITS 32
#define ARGO_TOOLSET_ARCH_BITS_32 1
#define ARGO_TOOLSET_ARCH_LITTLE_ENDIAN 1
#elif defined(__X86_64__) || defined(__x86_64__) || defined(_M_X64)
#define ARGO_TOOLSET_ARCH "x86-64"
#define ARGO_TOOLSET_ARCH_X86 1
#define ARGO_TOOLSET_ARCH_INTEL 1
#define ARGO_TOOLSET_ARCH_BITS 64
#define ARGO_TOOLSET_ARCH_BITS_64 1
#define ARGO_TOOLSET_ARCH_LITTLE_ENDIAN 1
#elif defined(__arm__) || defined(_M_ARM)
#define ARGO_TOOLSET_ARCH "arm32"
#define ARGO_TOOLSET_ARCH_ARM 1
#define ARGO_TOOLSET_ARCH_BITS 32
#define ARGO_TOOLSET_ARCH_BITS_32 1
#define ARGO_TOOLSET_ARCH_LITTLE_ENDIAN 1
#define ARGO_TOOLSET_ARCH_ARM_VER __ARM_ARCH
#elif defined(__aarch64__)
#define ARGO_TOOLSET_ARCH "arm64"
#define ARGO_TOOLSET_ARCH_ARM 1
#define ARGO_TOOLSET_ARCH_BITS 64
#define ARGO_TOOLSET_ARCH_BITS_64 1
#define ARGO_TOOLSET_ARCH_LITTLE_ENDIAN 1
#define ARGO_TOOLSET_ARCH_ARM_VER __ARM_ARCH
#else
#error "Unsupported toolset: unknown platform"
#endif

//C++ Standard
#if ARGO_TOOLSET_COMPILER_GCC || ARGO_TOOLSET_COMPILER_CLANG
#if __cplusplus == 199711L
#define ARGO_TOOLSET_CPP_STD 3
#define ARGO_TOOLSET_CPP_STD_03 1
#elif __cplusplus == 201402L
#define ARGO_TOOLSET_CPP_STD 14
#define ARGO_TOOLSET_CPP_STD_14 1
#elif __cplusplus == 201703L
#define ARGO_TOOLSET_CPP_STD 17
#define ARGO_TOOLSET_CPP_STD_17 1
#elif __cplusplus >= 201103L || defined(__GXX_EXPERIMENTAL_CXX0X)
#define ARGO_TOOLSET_CPP_STD 11
#define ARGO_TOOLSET_CPP_STD_11 1
#elif ARGO_TOOLSET_COMPILER_MSVC
#if ARGO_TOOLSET_COMPILER_MSVC_VS_2017
#if _MSVC_LANG == 201703
#define ARGO_TOOLSET_CPP_STD 17
#define ARGO_TOOLSET_CPP_STD_17 1
#else
#define ARGO_TOOLSET_CPP_STD 14
#define ARGO_TOOLSET_CPP_STD_14 1
#endif
#elif ARGO_TOOLSET_COMPILER_MSVC_VS_2015
#define ARGO_TOOLSET_CPP_STD 11
#define ARGO_TOOLSET_CPP_STD_11 1
#elif ARGO_TOOLSET_COMPILER_MSVC_VS_2013
#define ARGO_TOOLSET_CPP_STD 11
#define ARGO_TOOLSET_CPP_STD_11 1
#elif ARGO_TOOLSET_COMPILER_MSVC_VS_2010
#define ARGO_TOOLSET_CPP_STD 3
#define ARGO_TOOLSET_CPP_STD_03 1
#elif ARGO_TOOLSET_COMPILER_MSVC_VS_2008
#define ARGO_TOOLSET_CPP_STD 3
#define ARGO_TOOLSET_CPP_STD_03 1
#else
#define ARGO_TOOLSET_CPP_STD 3
#define ARGO_TOOLSET_CPP_STD_03 1
#endif
#else
#error "Unsupported toolset: unknown C++ standard"
#endif
#endif

//Debug/Release
#if !defined(NDEBUG)
#define ARGO_TOOLSET_DEBUG 1
#else
#define ARGO_TOOLSET_RELEASE 1
#endif

#endif
#include <iostream>

#define ARGO_MSS_GET_ARG_1_(args) ARGO_MSS_GET_ARG_1 args
#define ARGO_MSS_GET_ARG_1(N, ...) N
#define ARGO_MSS_GET_ARG_2_(args) ARGO_MSS_GET_ARG_2 args
#define ARGO_MSS_GET_ARG_2(_1, N, ...) N
#define ARGO_MSS_GET_ARG_3_(args) ARGO_MSS_GET_ARG_3 args
#define ARGO_MSS_GET_ARG_3(_1, _2, N, ...) N

#ifdef ARGO_MSS_BEGIN
#error "ARGO_MSS_BEGIN already defined"
#endif
#define ARGO_MSS_BEGIN(type) using mss_type = type;

#ifdef ARGO_MSS_RETURN_OK
#error "ARGO_MSS_RETURN_OK already defined"
#endif
#define ARGO_MSS_RETURN_OK() return true

#ifdef ARGO_MSS_RETURN_ERROR
#error "ARGO_MSS_RETURN_ERROR already defined"
#endif
#define ARGO_MSS_RETURN_ERROR() return false

#ifdef ARGO_MSS_END
#error "ARGO_MSS_END already defined"
#endif
#define ARGO_MSS_END() ARGO_MSS_RETURN_OK();

//#define ARGO_MSS_PRINT_TRACEBACK

#ifdef ARGO_MSS
#error "ARGO_MSS already defined"
#endif
#if ARGO_MSS_PRINT_TRACEBACK
#define ARGO_MSS_LOG_TRACEBACK(expr, file, line) std::cerr << "[ARGO_MSS FAILURE] " << file << ":" << line << ": " << expr << std::endl;
#else
#define ARGO_MSS_LOG_TRACEBACK(...) 
#endif
#define ARGO_MSS_1(expr) \
    { \
        mss_type mss_res = (expr); \
        if (!mss_res) \
        { \
            ARGO_MSS_LOG_TRACEBACK(#expr, __FILE__, __LINE__) \
            return false; \
        } \
    }
#define ARGO_MSS_2(expr, block) \
    { \
        mss_type mss_res = (expr); \
        if (!mss_res) \
        { \
            ARGO_MSS_LOG_TRACEBACK(#expr, __FILE__, __LINE__) \
            block; \
            return false; \
        } \
    }
#define ARGO_MSS(...) ARGO_MSS_GET_ARG_3_((__VA_ARGS__, ARGO_MSS_2, ARGO_MSS_1))(__VA_ARGS__)

#ifdef ARGO_MSS_Q
#error "ARGO_MSS_Q already defined"
#endif
#define ARGO_MSS_Q_1(expr) \
    { \
        mss_type mss_res = (expr); \
        if (!mss_res) return false; \
    }
#define ARGO_MSS_Q_2(expr, block) \
    { \
        mss_type mss_res = (expr); \
        if (!mss_res) \
        { \
            block; \
            return false; \
        } \
    }
#define ARGO_MSS_Q(...) ARGO_MSS_GET_ARG_3_((__VA_ARGS__, ARGO_MSS_Q_2, ARGO_MSS_Q_1))(__VA_ARGS__)

#endif
#ifndef HEADER_lw_argo_core_string_utility_hpp_INCLUDE_GUARD
#define HEADER_lw_argo_core_string_utility_hpp_INCLUDE_GUARD

#ifndef HEADER_lw_argo_core_NotOnFirstTime_hpp_INCLUDE_GUARD
#define HEADER_lw_argo_core_NotOnFirstTime_hpp_INCLUDE_GUARD


namespace lw { namespace argo {

namespace core {

class NotOnFirstTime
{
public:
    template <typename Functor>
    void operator()(Functor functor) const
    {
        if (!first_time_)
            functor();
        else
            first_time_ = false;
    }
    void reset() { first_time_ = true; }

private:
    mutable bool first_time_ = true;
};

} // namespace core

} }

#endif

#ifndef HEADER_lw_argo_core_log_hpp_INCLUDE_GUARD
#define HEADER_lw_argo_core_log_hpp_INCLUDE_GUARD


#define ARGO_DEBUG false
#define ARGO_S(state) static bool log_enabled_ = state
#define ARGO_L(expr) if (log_enabled_) { std::cout << "[TRACE] " << expr << std::endl; }
#define ARGO_C(expr) "(" << #expr << ":" << (expr) << ")"

#endif
#ifndef HEADER_lw_argo_core_optional_hpp_INCLUDE_GUARD
#define HEADER_lw_argo_core_optional_hpp_INCLUDE_GUARD

#include <array>
#include <cstddef>
#include <type_traits>

namespace lw { namespace argo {

namespace core {

struct nullopt_t
{
    nullopt_t(int) {}
};

const auto nullopt = nullopt_t{ 0 };

template <typename T>
class optional
{
public:
    using value_type = T;

    optional() = default;
    optional(const T &t) { set(t); }
    optional(T &&t) { set(std::forward<T>(t)); }
    optional(const optional &other)
    {
        if (other) set(*other);
    }
    optional(optional &&other)
    {
        if (other) set(std::move(*other.ptr_()));
    }
    optional(const nullopt_t &null) {}
    optional(const std::nullptr_t &null) {}
    optional &operator=(const optional &other)
    {
        if (this != &other)
        {
            if (other)
                set(*other);
            else
                reset();
        }
        return *this;
    }
    optional &operator=(const T &other)
    {
        set(other);
        return *this;
    }
    optional &operator=(T &&other)
    {
        set(std::move(other));
        return *this;
    }
    optional &operator=(optional &&other)
    {
        if (other)
            set(std::move(*other.ptr_()));
        else
            reset();
        return *this;
    }
    optional &operator=(const nullopt_t &null)
    {
        reset();
        return *this;
    }
    ~optional() { reset(); }

    bool equal(const optional &other) const
    {
        if (valid() && other.valid()) return *get() == *other.get();
        return valid() == other.valid();
    }
    bool valid() const { return is_valid_; }
    explicit operator bool() const { return valid(); }
    void reset()
    {
        if (is_valid_) ptr_()->~T();
        is_valid_ = false;
    }
    T &emplace()
    {
        reset();
        new (ptr_()) T{};
        is_valid_ = true;
        return *ptr_();
    }
    void set(const T &v)
    {
        reset();
        new (ptr_()) T{v};
        is_valid_ = true;
    }
    void set(T &&v)
    {
        reset();
        new (ptr_()) T{std::move(v)};
        is_valid_ = true;
    }
    void set_if_unset(const T &v)
    {
        if (is_valid_) return;
        set(v);
    }
    const T *get() const
    {
        if (!is_valid_) return nullptr;
        return ptr_();
    }
    const T &operator*() const
    {
        assert(is_valid_);
        return *ptr_();
    }
    T &operator*()
    {
        assert(is_valid_);
        return *ptr_();
    }
    const T *operator->() const
    {
        assert(is_valid_);
        return ptr_();
    }
    T *operator->()
    {
        assert(is_valid_);
        return ptr_();
    }
    const T &value_or(const T &v) const
    {
        if (is_valid_) return *ptr_();
        return v;
    }

private:
    using Block = std::array<std::uint8_t, sizeof(T)>;

    T *ptr_() { return reinterpret_cast<T *>(&storage_.block[0]); }
    const T *ptr_() const { return reinterpret_cast<const T *>(&storage_.block[0]); }

    union Storage
    {
        max_align_t align_{};
        Block block;
    };
    Storage storage_;
    bool is_valid_ = false;
};

template <typename T>
inline bool operator==(const optional<T> &lhs, const optional<T> &rhs)
{
    if (static_cast<bool>(lhs) != static_cast<bool>(rhs)) return false;
    if (!static_cast<bool>(lhs)) return true;
    return *lhs == *rhs;
}
template <typename T>
inline bool operator!=(const optional<T> &lhs, const optional<T> &rhs)
{
    return !(lhs == rhs);
}
template <typename T>
inline bool operator<(const optional<T> &lhs, const optional<T> &rhs)
{
    if (!static_cast<bool>(rhs)) return false;
    if (!static_cast<bool>(lhs)) return true;
    return *lhs < *rhs;
}
template <typename T>
inline bool operator>(const optional<T> &lhs, const optional<T> &rhs)
{
    return rhs < lhs;
}
template <typename T>
inline bool operator<=(const optional<T> &lhs, const optional<T> &rhs)
{
    return !(rhs < lhs);
}
template <typename T>
inline bool operator>=(const optional<T> &lhs, const optional<T> &rhs)
{
    return !(lhs < rhs);
}
template <typename T>
inline bool operator==(const optional<T> &lhs, nullopt_t) noexcept
{
    return !static_cast<bool>(lhs);
}
template <typename T>
inline bool operator==(nullopt_t, const optional<T> &rhs) noexcept
{
    return !static_cast<bool>(rhs);
}
template <typename T>
inline bool operator!=(const optional<T> &lhs, nullopt_t) noexcept
{
    return static_cast<bool>(lhs);
}
template <typename T>
inline bool operator!=(nullopt_t, const optional<T> &rhs) noexcept
{
    return static_cast<bool>(rhs);
}
template <typename T>
inline bool operator<(const optional<T> &, nullopt_t) noexcept
{
    return false;
}
template <typename T>
inline bool operator<(nullopt_t, const optional<T> &rhs) noexcept
{
    return static_cast<bool>(rhs);
}
template <typename T>
inline bool operator<=(const optional<T> &lhs, nullopt_t) noexcept
{
    return !static_cast<bool>(lhs);
}
template <typename T>
inline bool operator<=(nullopt_t, const optional<T> &) noexcept
{
    return true;
}
template <typename T>
inline bool operator>(const optional<T> &lhs, nullopt_t) noexcept
{
    return static_cast<bool>(lhs);
}
template <typename T>
inline bool operator>(nullopt_t, const optional<T> &) noexcept
{
    return false;
}
template <typename T>
inline bool operator>=(const optional<T> &, nullopt_t) noexcept
{
    return true;
}
template <typename T>
inline bool operator>=(nullopt_t, const optional<T> &rhs) noexcept
{
    return !static_cast<bool>(rhs);
}
template <typename T>
inline bool operator==(const optional<T> &lhs, const T &rhs)
{
    return static_cast<bool>(lhs) ? *lhs == rhs : false;
}
template <typename T>
inline bool operator==(const T &lhs, const optional<T> &rhs)
{
    return static_cast<bool>(rhs) ? *rhs == lhs : false;
}
template <typename T>
inline bool operator!=(const optional<T> &lhs, const T &rhs)
{
    return static_cast<bool>(lhs) ? !(*lhs == rhs) : true;
}
template <typename T>
inline bool operator!=(const T &lhs, const optional<T> &rhs)
{
    return static_cast<bool>(rhs) ? !(*rhs == lhs) : true;
}
template <typename T>
inline bool operator<(const optional<T> &lhs, const T &rhs)
{
    return static_cast<bool>(lhs) ? std::less<T>{}(*lhs, rhs) : true;
}
template <typename T>
inline bool operator<(const T &lhs, const optional<T> &rhs)
{
    return static_cast<bool>(rhs) ? std::less<T>{}(lhs, *rhs) : false;
}
template <typename T>
inline bool operator<=(const optional<T> &lhs, const T &rhs)
{
    return !(lhs > rhs);
}
template <typename T>
inline bool operator<=(const T &lhs, const optional<T> &rhs)
{
    return !(lhs > rhs);
}
template <typename T>
inline bool operator>(const optional<T> &lhs, const T &rhs)
{
    return static_cast<bool>(lhs) ? rhs < lhs : false;
}
template <typename T>
inline bool operator>(const T &lhs, const optional<T> &rhs)
{
    return static_cast<bool>(rhs) ? rhs < lhs : true;
}
template <typename T>
inline bool operator>=(const optional<T> &lhs, const T &rhs)
{
    return !(lhs < rhs);
}
template <typename T>
inline bool operator>=(const T &lhs, const optional<T> &rhs)
{
    return !(lhs < rhs);
}
template <typename T>

inline void swap(optional<T> &lhs, optional<T> &rhs) noexcept(noexcept(lhs.swap(rhs)))
{
    lhs.swap(rhs);
}
template <typename T>
inline optional<typename std::decay<T>::type> make_optional(T &&value)
{
    return optional<typename std::decay<T>::type>(std::forward<T>(value));
}

} // namespace core

} }

#endif
#ifndef HEADER_lw_argo_core_range_hpp_INCLUDE_GUARD
#define HEADER_lw_argo_core_range_hpp_INCLUDE_GUARD

#define ARGO_RANGE(container) (std::begin(container)), (std::end(container))

#endif
#include <algorithm>
#include <cctype>
#include <initializer_list>
#include <sstream>
#include <vector>

namespace lw { namespace argo {

namespace core { namespace string {

template <typename T>
struct from
{
    static std::string run(const T &t) { return std::to_string(t); }
};
template <>
struct from<std::string>
{
    static std::string run(const std::string &t) { return t; }
};
template <>
struct from<const char *>
{
    static std::string run(const char *cstr) { return cstr; }
};

template <typename It, typename value_type = typename std::iterator_traits<It>::value_type>
std::string join(const std::string &separator, It begin, It end)
{
    if (begin == end) return {};

    auto it = begin;
    std::string result = from<value_type>::run(*it++);
    for (; it != end; ++it) result += separator + from<value_type>::run(*it);
    return result;
}
template <typename T>
std::string join(const std::string &separator, const std::initializer_list<T> &list)
{
    return join(ARGO_RANGE(list), separator);
}
inline std::vector<std::string> split(std::string str, std::string delimiter = " ")
{
    std::size_t pos = 0;
    std::vector<std::string> result;
    while ((pos = str.find(delimiter)) != std::string::npos)
    {
        const auto token = str.substr(0, pos);
        result.push_back(std::move(token));
        str.erase(0, pos + delimiter.length());
    }
    if (!str.empty()) result.push_back(str);
    return result;
}
inline void replace_all(std::string &str, const std::string &from, const std::string &to)
{
    std::size_t start_pos = 0u;
    while ((start_pos = str.find(from, start_pos)) != std::string::npos)
    {
        str.replace(start_pos, from.size(), to);
        start_pos += to.size();
    }
}
inline bool starts_with(const std::string &input, const std::string &test) { return test.size() <= input.size() && input.compare(0, test.size(), test) == 0; }
inline bool ends_with(const std::string &input, const std::string &test) { return input.size() >= test.size() && input.compare(input.size() - test.size(), test.size(), test) == 0; }
inline void ltrim(std::string &input)
{
    input.erase(input.begin(), std::find_if(ARGO_RANGE(input), [](int ch) { return !std::isspace(ch); }));
}
inline void rtrim(std::string &input)
{
    input.erase(std::find_if(input.rbegin(), input.rend(), [](int ch) { return !std::isspace(ch); }).base(), input.end());
}
inline void trim(std::string &input)
{
    ltrim(input);
    rtrim(input);
}
inline std::string surround(const std::string &input, const std::string &left, core::optional<std::string> right = core::nullopt)
{
    if (!right)
    {
        if ("(" == left)
            right = ")";
        else if ("[" == left)
            right = "]";
        else if ("{" == left)
            right = "}";
        else if ("<" == left)
            right = ">";
        else
            right = left;
    }
    assert(!!right);
    return left + input + *right;
}
template <typename Out>
void split(const std::string &input, const char delimiter, Out out)
{
    std::istringstream ss{ input };
    std::string part;
    while (std::getline(ss, part, delimiter)) *(out++) = part;
}
inline std::vector<std::string> split(const std::string &input, const char delimiter)
{
    std::vector<std::string> result;
    split(input, delimiter, std::back_inserter(result));
    return result;
}
inline std::string tolower(const std::string &input)
{
    std::string result;
    result.reserve(input.size());
    for (const auto ch : input) result.push_back(std::tolower(ch));
    return result;
}
inline void tolower(std::string::iterator first, std::string::iterator last)
{
    for (auto current = first; current != last; ++current) *current = std::tolower(*current);
}
inline std::string toupper(const std::string &input)
{
    std::string result;
    result.reserve(input.size());
    for (const auto ch : input) result.push_back(std::toupper(ch));
    return result;
}
inline void toupper(std::string::iterator first, std::string::iterator last)
{
    for (auto current = first; current != last; ++current) *current = std::toupper(*current);
}
inline bool is_delimited(const std::string &input, const std::string &delimiter) { return starts_with(input, delimiter) && ends_with(input, delimiter); }
inline std::string align(const std::string &input, const unsigned page_size, const unsigned indentation, bool ascii_color_code = true)
{
    ARGO_S(false);
    assert(page_size > indentation);
    std::string result;
    const std::string prefix(indentation, ' ');
    struct
    {
        unsigned line_idx = 0;
        std::string::const_iterator previous;
        std::string::const_iterator breaker;

        void reset(std::string::const_iterator current, std::string::const_iterator end)
        {
            line_idx = 0;
            previous = current;
            breaker = end;
        }
    } state;
    ARGO_L(ARGO_C(input.size()) ARGO_C(page_size) ARGO_C(indentation));
    const std::string::const_iterator begin = std::begin(input);
    const std::string::const_iterator end = std::end(input);
    std::string::const_iterator current = begin;
    state.reset(current, end);
    auto skip_whitespace = [&current, &end]() {
        while (current != end && std::isspace(*current)) ++current;
    };
    auto skip_colorcodes = [&current, &end]() {
        ARGO_S(false);
        auto it = current;
        std::string look_ahead;
        while (it != end && look_ahead.size() < 2)
        {
            look_ahead.push_back(*it);
            ++it;
        }
        if ("\033[" == look_ahead)
        {
            ARGO_L("Skipping color code");
            while (current != end && *current != 'm')
            {
                ARGO_L(" > skip");
                ++current;
            }
            if (current != end) ++current;
        }
    };
    auto complete = [&]() {
        std::copy(state.previous, current, std::back_inserter(result));
    };
    while (current != end)
    {
        skip_colorcodes();
        if (current == end) break;
        const char ch = *current;
        ARGO_L(ARGO_C(ch) ARGO_C(state.line_idx));
        if (state.line_idx == 0)
        {
            ARGO_L("New line");
            result += prefix;
            ARGO_L(ARGO_C(result));
            state.line_idx += prefix.size() + 1;
        }
        else if (state.line_idx > page_size)
        {
            ARGO_L("Line wrap");
            if (end != state.breaker) current = state.breaker;
            complete();
            result += "\n";
            skip_whitespace();
            state.reset(current, end);
        }
        else if (*current == '\n')
        {
            ARGO_L("Line break");
            complete();
            result += "\n";
            ++current;
            state.reset(current, end);
        }
        else
        {
            if (std::isspace(ch))
            {
                ARGO_L("Breaker");
                state.breaker = current;
            }
            ++state.line_idx;
            ++current;
        }
    }
    ARGO_L("Left over");
    std::copy(state.previous, end, std::back_inserter(result));
    ARGO_L(ARGO_C(result));
    return result;
}

template<typename Type>
std::string quote(Type value)
{
    std::ostringstream os;
    os << value;
    return surround(os.str(), "'");
}

template<typename It>
std::string quote(It first, It last, const std::string &last_word)
{
    const auto nr_values = std::distance(first, last);
    if (0 == nr_values) return {};
    else if (1 == nr_values) return quote(*first);
    std::ostringstream os;
    auto current = first;
    NotOnFirstTime noft{};
    for (auto i = 0u; i < nr_values - 1; ++i, ++current)
    {
        noft([&os](){ os << ", "; });
        os << quote(*current);
    }
    os << surround(last_word, " ") << quote(*current);
    return os.str();
}

template<typename It>
std::string quote_or(It first, It last) { return quote(first, last, "or"); }

template<typename It>
std::string quote_and(It first, It last) { return quote(first, last, "and"); }

}} // namespace core::string

} }

#endif

#include <fstream>

namespace lw { namespace argo {

//!Response file parser.
class ResponseFile
{
public:
    //!Parses the given reponse file and returns a boolean indicating success.
    bool parse(const std::string &filename)
    {
        ARGO_MSS_BEGIN(bool);
        filename_ = filename;
        argv_.clear();
        std::ifstream stream(filename, std::ios_base::in);
        ARGO_MSS(!stream.fail());
        std::string line;
        while (std::getline(stream, line))
        {
            core::string::ltrim(line);
            if (core::string::starts_with(line, "#")) continue;
            const auto args = core::string::split(line);
            std::copy(ARGO_RANGE(args), std::back_inserter(argv_));
        }
        ARGO_MSS_END();
    }
    //!Returns the filename of the last parsed response file.
    std::string filename() const { return filename_; }
    //!Returns the number of parsed arguments.
    int argc() const { return argv_.size(); }
    //!Returns the parsed arguments.
    std::vector<std::string> argv() const { return argv_; }

private:
    std::string filename_;
    std::vector<std::string> argv_;
};

} }

#endif

#ifndef HEADER_lw_argo_Result_hpp_INCLUDE_GUARD
#define HEADER_lw_argo_Result_hpp_INCLUDE_GUARD


namespace lw { namespace argo {

//!Aggregate holding the end result of the parsing process.
struct Result
{
    ReturnCode status = ReturnCode::SuccessAndContinue; //!<Return code status.
    std::string message;                                //!<Empty unless ReturnCode::Error, in which case it contains a descriptive error message which is intended for the end user.
    std::vector<std::string> arguments;                 //!<The normalized and expanded arguments that were processed. This includes normalizing invocations such as `--foo 1 --foo=2,3` to `--foo 1 2 3` and expanding all response files.
};

inline std::ostream &operator<<(std::ostream &os, const Result &result)
{
    os << "{\n";
    os << "  status: \"" << result.status << "\",\n";
    os << "  message: \"" << result.message << "\",\n";
    os << "  arguments: [";
    core::NotOnFirstTime delim;
    for (const auto &arg : result.arguments)
    {
        delim([&os]() { os << ", "; });
        os << core::string::surround(arg, "\"");
    }
    os << "]\n";
    os << "}";
    return os;
}

} }

#endif

#ifndef HEADER_lw_argo_Arguments_hpp_INCLUDE_GUARD
#define HEADER_lw_argo_Arguments_hpp_INCLUDE_GUARD

#ifndef HEADER_lw_argo_handler_ResponseFile_hpp_INCLUDE_GUARD
#define HEADER_lw_argo_handler_ResponseFile_hpp_INCLUDE_GUARD

#ifndef HEADER_lw_argo_handler_Interface_hpp_INCLUDE_GUARD
#define HEADER_lw_argo_handler_Interface_hpp_INCLUDE_GUARD

#ifndef HEADER_lw_argo_core_Context_hpp_INCLUDE_GUARD
#define HEADER_lw_argo_core_Context_hpp_INCLUDE_GUARD

#ifndef HEADER_lw_argo_core_AlwaysTrue_hpp_INCLUDE_GUARD
#define HEADER_lw_argo_core_AlwaysTrue_hpp_INCLUDE_GUARD


namespace lw { namespace argo {

namespace core {

template <bool value>
struct AlwaysTrueOrFalse
{
    template <typename... Args>
    bool operator()(Args &&... args) const
    {
        return value;
    }
};

using AlwaysTrue = AlwaysTrueOrFalse<true>;
using AlwaysFalse = AlwaysTrueOrFalse<false>;

} // namespace core

} }

#endif

#ifndef HEADER_lw_argo_core_Args_hpp_INCLUDE_GUARD
#define HEADER_lw_argo_core_Args_hpp_INCLUDE_GUARD

#include <iterator>

namespace lw { namespace argo {

namespace core {

class Args
{
public:
    using iterator = std::vector<std::string>::iterator;

    explicit Args(std::vector<std::string> &arguments)
        : data_(arguments)
        , begin_(std::begin(data_))
        , end_(std::end(data_))
        , current_(begin_)
    {
    }

    iterator begin() const { return begin_; }
    iterator end() const { return end_; }
    iterator current() const { return current_; }
    bool reached_end() const { return end_ == current_; }
    bool next()
    {
        ARGO_MSS_BEGIN(bool);
        ARGO_MSS(end_ != current_);
        ++current_;
        ARGO_MSS_END();
    }
    void replace(const std::vector<std::string> &arguments)
    {
        ARGO_S(ARGO_DEBUG);
        auto idx = std::distance(begin(), current());
        std::copy(ARGO_RANGE(arguments), std::inserter(data_, current() + 1));
        auto it = std::begin(data_) + idx;
        data_.erase(it);
        begin_ = std::begin(data_);
        end_ = std::end(data_);
        current_ = begin_ + idx;
        ARGO_L("Replacing current argument with new arguments in-place:");
        for (const auto &arg : data_) ARGO_L(ARGO_C(arg));
    }

private:
    std::vector<std::string> &data_; //The underlying data must exist for the entire duration of the parsing
    iterator begin_;
    iterator end_;
    iterator current_;
};

} // namespace core

} }

#endif
#ifndef HEADER_lw_argo_core_Error_hpp_INCLUDE_GUARD
#define HEADER_lw_argo_core_Error_hpp_INCLUDE_GUARD


namespace lw { namespace argo {

namespace core {

class Context;

//!Class that sets the current result's status to `ReturnCode::Error` and assigns the given message.
class Error
{
public:
    Error(const Error &other)
        : result_(other.result_)
    {
        os_ << other.os_.str();
    }
    ~Error()
    {
        auto message = os_.str();
        result_.message = message.empty() ? "Parsing failed" : message;
        result_.status = ReturnCode::Error;
    }
    //!Appends the given message and returns the object.
    template <typename T>
    Error &operator<<(T &&value)
    {
        os_ << value;
        return *this;
    }

private:
    friend class Context;

    explicit Error(Result &result)
        : result_(result)
    {
    }

    Result &result_;
    std::ostringstream os_;
};

} // namespace core

} }

#endif
#ifndef HEADER_lw_argo_core_Phase_hpp_INCLUDE_GUARD
#define HEADER_lw_argo_core_Phase_hpp_INCLUDE_GUARD


namespace lw { namespace argo {

namespace core {

enum class Phase
{
    OptionsFirstPass,
    Positionals,
    OptionsLastPass,
    Nr_
};

inline std::ostream &operator<<(std::ostream &os, const Phase phase)
{
    switch (phase)
    {
    case Phase::OptionsFirstPass: os << "OptionsFirstPass"; break;
    case Phase::Positionals: os << "Positionals"; break;
    case Phase::OptionsLastPass: os << "OptionsLastPass"; break;
    default: assert(false); os << "?";
    }
    return os;
}

} // namespace core

} }

#endif

#ifndef HEADER_lw_argo_core_handler_IHandler_hpp_INCLUDE_GUARD
#define HEADER_lw_argo_core_handler_IHandler_hpp_INCLUDE_GUARD

#ifndef HEADER_lw_argo_core_Context_fwd_hpp_INCLUDE_GUARD
#define HEADER_lw_argo_core_Context_fwd_hpp_INCLUDE_GUARD


namespace lw { namespace argo {

namespace core {

class Context;

}

} }

#endif

#ifndef HEADER_lw_argo_core_handler_visitor_IVisitor_hpp_INCLUDE_GUARD
#define HEADER_lw_argo_core_handler_visitor_IVisitor_hpp_INCLUDE_GUARD


namespace lw { namespace argo {

namespace handler {

class Flag;
class Interface;
class Option;
class Positional;
class Toggle;

namespace group {

class Interface;
}

} // namespace handler

namespace core { namespace handler { namespace visitor {

template <bool is_const, typename Type>
using type_of = typename std::conditional<is_const, const Type &, Type &>::type;

template <bool is_const>
class IVisitor
{
    template <typename Type>
    using type_of = type_of<is_const, Type>;

public:
    virtual ~IVisitor() = default;

    using is_const_type = typename std::conditional<is_const, std::true_type, std::false_type>::type;

    virtual bool visit(type_of<ARGO_NS(handler::Flag)>) = 0;
    virtual bool visit(type_of<ARGO_NS(handler::Interface)>) = 0;
    virtual bool visit(type_of<ARGO_NS(handler::Option)>) = 0;
    virtual bool visit(type_of<ARGO_NS(handler::Positional)>) = 0;
    virtual bool visit(type_of<ARGO_NS(handler::Toggle)>) = 0;
    virtual bool visit(type_of<ARGO_NS(handler::group::Interface)>) = 0;
};

using IConstVisitor = IVisitor<true>;
using IMutatingVisitor = IVisitor<false>;

}}} // namespace core::handler::visitor

} }

#endif
#ifndef HEADER_lw_argo_core_memory_hpp_INCLUDE_GUARD
#define HEADER_lw_argo_core_memory_hpp_INCLUDE_GUARD

#include <memory>

namespace lw { namespace argo {

namespace core {

template <typename T, typename... Args>
std::unique_ptr<T> make_unique(Args &&... args)
{
    return std::unique_ptr<T>(new T(std::forward<Args>(args)...));
}

} // namespace core

} }

#endif
#include <functional>

namespace lw { namespace argo {

class Arguments;

namespace core { namespace handler {

class IHandler;
using Ptr = std::unique_ptr<IHandler>;

class IHandler
{
public:
    virtual ~IHandler() = default;

    //!Indicates if the handler is correctly configured.
    virtual bool is_valid() const = 0;
    //!Returns the handler's name.
    virtual std::string name() const = 0;
    //!Returns the handler's type description.
    virtual std::string type() const = 0;
    //!Accepts a constant visitor. Returns a boolean indicating success.
    virtual bool accept(visitor::IConstVisitor &visitor) const = 0;
    //!Accepts a mutating visitor. Returns a boolean indicating success.
    virtual bool accept(visitor::IMutatingVisitor &visitor) = 0;
    //!Returns a cloned version of the handler.
    virtual Ptr clone() const = 0;
    //!Takes over the parsing process in the current context. Returns a boolean indicating success.
    virtual bool parse(Context &context) = 0;
    //!Indicates if the handler is satisfied were the parsing to stop at this point.
    virtual bool is_satisfied(Context &context) const = 0;

    const IHandler *parent() const { return pparent_; }
    void set_parent(const IHandler &handler) const { assert(&handler != this); pparent_ = &handler; }

protected:
    mutable const IHandler *pparent_ = nullptr;

private:
    friend class ARGO_NS(Arguments);

    virtual void reset_() {}
};

inline std::ostream &operator<<(std::ostream &os, const IHandler &handler) { return os << handler.name(); }

}} // namespace core::handler

} }

#endif
#ifndef HEADER_lw_argo_core_handler_Info_hpp_INCLUDE_GUARD
#define HEADER_lw_argo_core_handler_Info_hpp_INCLUDE_GUARD


namespace lw { namespace argo {

namespace core { namespace handler {

struct Info
{
    unsigned nr_parses = 0;
};

}} // namespace core::handler

} }

#endif
#include <map>

namespace lw { namespace argo {

class Arguments;

namespace core {

class Handlers;

namespace handler {

class IHandler;
}

//!Class that allows access and modification of the parsing state.
class Context
{
public:
    using ParserCouldContinueFunction = std::function<bool(Context &context)>;

    Context(Arguments &parser,
            const Configuration &config,
            const Handlers &handlers,
            Phase &phase,
            ParserCouldContinueFunction parser_could_continue = AlwaysFalse())
        : parser_could_continue(parser_could_continue)
        , pparser_(&parser)
        , config_(config)
        , phandlers_(&handlers)
        , phase_(phase)
    {
    }

    void set_args(Args &args) { pargs_ = &args; }
    const Args &args() const
    {
        assert(!!pargs_);
        return *pargs_;
    }
    Args &args()
    {
        assert(!!pargs_);
        return *pargs_;
    }
    void set_handler(const ARGO_NS(core::handler::IHandler) & handler) { phandler_ = &handler; }
    const ARGO_NS(core::handler::IHandler) & handler() const
    {
        assert(!!phandler_);
        return *phandler_;
    }
    ParserCouldContinueFunction parser_could_continue;
    const Result &result() const { return result_; }
    Result &result() { return result_; }
    //!Returns an Error object for usage within the caller's scope. The result's status will be set to `ReturnCode::Error`.
    Error error() { return Error{ result_ }; }
    //!Signals the parsing to process to abort. The result's status will be set to `ReturnCode::SuccessAndAbort`.
    void abort() { result_.status = ReturnCode::SuccessAndAbort; }
    bool aborted() const { return ReturnCode::SuccessAndAbort == result_.status; }
    //!Switches to another parser which will continue the processing using the current context.
    void switch_parser(Arguments &parser) { pparser_ = &parser; }
    //!Indicates if the given parser is the parser processing the current context.
    bool is_current_parser(const Arguments &parser) { return pparser_ == &parser; }
    const Arguments &parser() const
    {
        assert(!!pparser_);
        return *pparser_;
    }
    Arguments &parser()
    {
        assert(!!pparser_);
        return *pparser_;
    }
    //!Returns the configuration of the active parser.
    const Configuration &config() const { return config_; }
    const Handlers &handlers() const
    {
        assert(!!phandlers_);
        return *phandlers_;
    }
    Phase phase() const { return phase_; }
    void next_phase()
    {
        phase_ = static_cast<Phase>(static_cast<unsigned>(phase_) + 1);
        assert(Phase::Nr_ != phase_); 
    }
    const handler::Info &info(const std::string &name) const { return info_[name]; }
    handler::Info &info(const std::string &name) { return info_[name]; }
    const handler::Info &info(const core::handler::IHandler &handler) const { return info(handler.name()); }
    handler::Info &info(const core::handler::IHandler &handler) { return info(handler.name()); }

private:
    Arguments *pparser_ = nullptr;
    const Configuration config_;
    Args *pargs_ = nullptr;
    const ARGO_NS(core::handler::IHandler) *phandler_ = nullptr;
    const Handlers *phandlers_ = nullptr;
    Phase &phase_;
    Result result_;
    mutable std::map<std::string, handler::Info> info_;
};

} // namespace core

} }

#endif
#ifndef HEADER_lw_argo_core_handler_property_Help_hpp_INCLUDE_GUARD
#define HEADER_lw_argo_core_handler_property_Help_hpp_INCLUDE_GUARD


namespace lw { namespace argo {

namespace core { namespace handler { namespace property {

template <typename Handler>
class Help
{
public:
    Help() = default;
    Help(const Help &other) = default;

    //!Returns the help description.
    std::string help() const { return description_; }
    //!Sets the help description and returns the handler.
    Handler &help(const std::string description)
    {
        description_ = description;
        return static_cast<Handler &>(*this);
    }

protected:
    std::string description_;
};

}}} // namespace core::handler::property

} }

#endif

namespace lw { namespace argo {

namespace core { namespace handler { namespace search {

class Custom;

} } }

namespace handler {

//!A handler that can serve as a base class for custom handlers.
class Interface : public core::handler::IHandler,
                  public core::handler::property::Help<Interface>
{
public:
    //!Returns the help description.
    virtual std::string help() const = 0;

protected:
    friend class core::handler::search::Custom;

    virtual bool recognizes_(core::Context &context) const = 0;

    virtual bool accept(core::handler::visitor::IConstVisitor &visitor) const { return visitor.visit(*this); }
    virtual bool accept(core::handler::visitor::IMutatingVisitor &visitor) { return visitor.visit(*this); }
};

} // namespace handler

} }

#endif


namespace lw { namespace argo {

namespace handler {

//!A handler that processes response files.
class ResponseFile : public Interface
{
public:
    //!Constructs the response file handler with the given identifier. By default, the commonly used `@` character is chosen. As such, invocations such as `./app @/path/to/response/file.rsp` are processed by this handler. Note that the path to the response file must immediately follow the identifier string. In the case of the `@character` there is therefore no white space between the `@` the response file.
    explicit ResponseFile(const std::string &identifier = "@")
        : identifier_(identifier)
    {
    }

    virtual bool is_valid() const override { return true; }
    virtual std::string name() const override { return identifier_; }
    virtual std::string type() const override { return "Response file handler"; }
    virtual std::string help() const override
    {
        std::ostringstream os;
        os << "As an alternative to placing all the options on the command line, response files are also supported. Response files are plain text files that list command line arguments. This may be useful as a convenience or necessary due to limitations of your environment. Arguments in a response file are interpreted as if they were present at that place in the invocation. Each argument in a response file must begin and end on the same line. You cannot use the backslash character to concatenate lines. Comments are supported by prefixing lines with the // characters.";
        return os.str();
    }

private:
    virtual core::handler::Ptr clone() const override { return core::make_unique<ResponseFile>(*this); }
    virtual bool is_satisfied(core::Context &context) const override { return true; }
    virtual bool parse(core::Context &context) override
    {
        ARGO_MSS_BEGIN(bool);
        assert(!context.args().reached_end());
        const auto arg = *context.args().current();
        ARGO_MSS(core::string::starts_with(arg, identifier_));
        const std::string fn(std::begin(arg) + identifier_.size(), std::end(arg));
        ARGO_NS(ResponseFile)
        rsp;
        ARGO_MSS(rsp.parse(fn),
            {
                context.error() << "Could not parse file " << core::string::surround(fn, "'");
            });
        context.args().replace(rsp.argv());
        ARGO_MSS_END();
    }
    virtual bool recognizes_(core::Context &context) const override
    {
        assert(!context.args().reached_end());
        const auto arg = *context.args().current();
        return core::string::starts_with(arg, identifier_);
    }

    std::string identifier_;
};

} // namespace handler

} }

#endif

#ifndef HEADER_lw_argo_action_run_hpp_INCLUDE_GUARD
#define HEADER_lw_argo_action_run_hpp_INCLUDE_GUARD

#ifndef HEADER_lw_argo_action_Interface_hpp_INCLUDE_GUARD
#define HEADER_lw_argo_action_Interface_hpp_INCLUDE_GUARD

#ifndef HEADER_lw_argo_core_action_IAction_hpp_INCLUDE_GUARD
#define HEADER_lw_argo_core_action_IAction_hpp_INCLUDE_GUARD


namespace lw { namespace argo {

namespace core {

class Context;

namespace action {

class IAction;
using Ptr = std::unique_ptr<IAction>;

class IAction
{
public:
    virtual ~IAction() = default;

    virtual Ptr clone() const = 0;
    virtual bool apply(core::Context &) = 0;
};

} // namespace action

} // namespace core

} }

#endif

#ifndef HEADER_lw_argo_core_handler_utility_hpp_INCLUDE_GUARD
#define HEADER_lw_argo_core_handler_utility_hpp_INCLUDE_GUARD


namespace lw { namespace argo {

namespace core { namespace handler {

inline std::string name_of(const IHandler &handler, const bool uppercase = false)
{
    auto type = handler.type();
    if (!uppercase)
    {
        assert(!type.empty());
        string::tolower(std::begin(type), std::begin(type) + 1);
    }
    std::ostringstream os;
    os << type << " " << string::surround(handler.name(), "'");
    return os.str();
}

inline void dash(std::string &name) { core::string::replace_all(name, "_", "-"); }

inline void prefix(std::string &name)
{
    if (name.empty() || core::string::starts_with(name, "--") || core::string::starts_with(name, "-")) return;
    if (name.size() > 2) name = std::string("--") + name;
    else name = std::string("-") + name;
}

inline void prefix(std::string &first, std::string &second)
{
    if (first.empty() || second.empty()) return;
    if (first.size() == second.size()) return;

    if (core::string::starts_with(first, "--"))
    {
        if (core::string::starts_with(second, "-")) return;
        else second = std::string("-") + second;
    }
    else if (core::string::starts_with(second, "--"))
    {
        if (core::string::starts_with(first, "-")) return;
        else first = std::string("-") + first;
    }
    else if (core::string::starts_with(first, "-"))
    {
        assert(!core::string::starts_with(first, "--"));
        assert(!core::string::starts_with(second, "--"));
        second = std::string("--") + second;
    }
    else if (core::string::starts_with(second, "-"))
    {
        assert(!core::string::starts_with(first, "--"));
        assert(!core::string::starts_with(second, "--"));
        first = std::string("--") + first;
    }
    else
    {
        assert(!core::string::starts_with(first, "-"));
        assert(!core::string::starts_with(first, "--"));
        assert(!core::string::starts_with(second, "-"));
        assert(!core::string::starts_with(second, "--"));
        if (first.size() > second.size())
        {
            first = std::string("--") + first;
            second = std::string("-") + second;
        }
        else
        {
            assert(first.size() < second.size());
            first = std::string("-") + first;
            second = std::string("--") + second;
        }
    }
}

}} // namespace core::handler

} }

#endif
#ifndef HEADER_lw_argo_core_traits_conversion_hpp_INCLUDE_GUARD
#define HEADER_lw_argo_core_traits_conversion_hpp_INCLUDE_GUARD

#ifndef HEADER_lw_argo_core_convert_hpp_ALREADY_INCLUDED
#define HEADER_lw_argo_core_convert_hpp_ALREADY_INCLUDED

#include <cstdlib>

namespace lw { namespace argo {

namespace core {

//!Return the number of characters that could be used during the conversion
inline std::size_t convert(double &dst, const std::string &src)
{
    const char *c_str = src.c_str();
    char *end = nullptr;
    dst = std::strtod(c_str, &end);
    return end-c_str;
}
//!Return the number of characters that could be used during the conversion
inline std::size_t convert(float &dst, const std::string &src)
{
    const char *c_str = src.c_str();
    char *end = nullptr;
    dst = std::strtof(c_str, &end);
    return end-c_str;
}

}

} }

#endif

#include <cstdint>
#include <limits>

namespace lw { namespace argo {

namespace core { namespace traits {

template <typename T, typename = void>
struct conversion;

#define REPLACE_METRIC_SYMBOLS                    \
    if (value.size() - 1 == value.find("D"))      \
        string::replace_all(value, "D", "e1");    \
    else if (value.size() - 1 == value.find("h"))      \
        string::replace_all(value, "h", "e2");    \
    else if (value.size() - 1 == value.find("k"))      \
        string::replace_all(value, "k", "e3");    \
    else if (value.size() - 1 == value.find("K")) \
        string::replace_all(value, "K", "e3");    \
    else if (value.size() - 1 == value.find("M")) \
        string::replace_all(value, "M", "e6");    \
    else if (value.size() - 1 == value.find("G")) \
        string::replace_all(value, "G", "e9");    \
    else if (value.size() - 1 == value.find("T")) \
        string::replace_all(value, "T", "e12");   \
    else if (value.size() - 1 == value.find("P")) \
        string::replace_all(value, "P", "e15");   \
    else if (value.size() - 1 == value.find("E")) \
        string::replace_all(value, "E", "e18");
//Upper value limit

#define DEFINE_INTEGRAL_CONVERSION(T, D)                                              \
    template <>                                                                       \
    struct conversion<T>                                                              \
    {                                                                                 \
        using result_type = T;                                                        \
        static constexpr const char *description = D;                                 \
        static optional<result_type> run(std::string value)                           \
        {                                                                             \
            if (std::string::npos != value.find(".")) return nullopt;                 \
            REPLACE_METRIC_SYMBOLS                                                    \
            double dvalue;                                                            \
            if (!convert(dvalue, value)) return nullopt;                              \
            if (dvalue > std::numeric_limits<result_type>::max()) return nullopt;     \
            return static_cast<result_type>(dvalue);                                  \
        }                                                                             \
    };
DEFINE_INTEGRAL_CONVERSION(unsigned short, "an unsigned integer");
DEFINE_INTEGRAL_CONVERSION(unsigned int, "an unsigned integer");
DEFINE_INTEGRAL_CONVERSION(unsigned long, "an unsigned integer");
DEFINE_INTEGRAL_CONVERSION(unsigned long long, "an unsigned integer");
DEFINE_INTEGRAL_CONVERSION(short, "an integer");
DEFINE_INTEGRAL_CONVERSION(int, "an integer");
DEFINE_INTEGRAL_CONVERSION(long, "an integer");
DEFINE_INTEGRAL_CONVERSION(long long, "an integer");

template <>
struct conversion<std::string>
{
    using result_type = std::string;
    static constexpr const char *description = "a string";
    static optional<std::string> run(const std::string &value) { return value; }
};
template <>
struct conversion<const char *>: conversion<std::string> {};
template <>
struct conversion<float>
{
    using result_type = float;
    static constexpr const char *description = "a float";
    static optional<result_type> run(std::string value)
    {
        REPLACE_METRIC_SYMBOLS;
        float fvalue;
        if (!convert(fvalue, value)) return nullopt;
        return fvalue;
    }
};
template <>
struct conversion<double>
{
    using result_type = double;
    static constexpr const char *description = "a double";
    static optional<result_type> run(std::string value)
    {
        REPLACE_METRIC_SYMBOLS;
        double dvalue;
        if (!convert(dvalue, value)) return nullopt;
        return dvalue;
    }
};
template <>
struct conversion<bool>
{
    using result_type = bool;
    static constexpr const char *description = "a boolean";
    static optional<result_type> run(const std::string &value)
    {
        const auto lval = core::string::tolower(value);
        if ("true" == lval || "yes" == lval || "y" == lval || "1" == lval)
            return true;
        else if ("false" == lval || "no" == lval || "n" == lval || "0" == lval)
            return false;
        else
            return nullopt;
    }
};
#if ARGO_TOOLSET_CPP_STD_17 || ARGO_ENABLE_STD_OPTIONAL
template <typename T>
struct conversion<std::optional<T>>
{
    using result_type = std::optional<T>;
    static constexpr const char *description = conversion<T>::description;
    static optional<result_type> run(const std::string &value)
    {
        const auto result = conversion<T>::run(value);
        if (!!result)
            return result_type{ *result };
        else return nullopt;
    }
};
#endif
template <typename T>
struct conversion<optional<T>>
{
    using result_type = optional<T>;
    static constexpr const char *description = conversion<T>::description;
    static optional<result_type> run(const std::string &value)
    {
        const auto result = conversion<T>::run(value);
        if (!!result)
            return result_type{ *result };
        else return nullopt;
    }
};
template <typename T>
struct conversion<T, typename std::enable_if<std::is_enum<T>::value>::type>
{
    using result_type = T;
    static constexpr const char *description = "an unsigned integer mapped to an enumeration";
    static optional<result_type> run(const std::string &value)
    {
        const auto converted = conversion<unsigned>::run(value);
        if (!converted) return nullopt;
        const auto number = *converted;
        if (number >= static_cast<unsigned>(T::Nr_)) return nullopt;
        return static_cast<T>(number);
    }
};
template <typename T>
struct conversion<T, typename std::enable_if<std::is_integral<T>::value>::type>
{
    using result_type = T;
    static constexpr const char *description = "an integer";
    static optional<result_type> run(const std::string &value)
    {
        const auto result = conversion<int>::run(value);
        if (!!result)
            return *result;
        else
            return nullopt;
    }
};

}} // namespace core::traits

} }

#endif


namespace lw { namespace argo {

namespace action {

template <typename Type>
class Interface : public core::action::IAction
{
protected:
    virtual bool run(core::Context &, const Type value) = 0;

private:
    virtual bool apply(core::Context &context) override
    {
        using converter = core::traits::conversion<Type>;

        ARGO_MSS_BEGIN(bool);
        assert(context.args().current() != context.args().end());
        const auto str = *context.args().current();
        const auto value = converter::run(str);
        ARGO_MSS(!!value,
            {
                constexpr auto description = converter::description;
                context.error() << core::handler::name_of(context.handler(), true) << " expects "
                                << description
                                << " (not " << core::string::surround(str, "'") << ")";
            });
        ARGO_MSS(run(context, *value));
        ARGO_MSS_END();
    }
};

} // namespace action

} }

#endif


namespace lw { namespace argo {

namespace action {

//!Action that performs all required conversions and then invokes the associated callback function.
template <typename Type>
class Run : public Interface<Type>
{
public:
    using function_type = std::function<bool(const Type)>;
    using function_with_context_type = std::function<bool(core::Context &, const Type)>;

    explicit Run(const function_type &fnc)
        : simple_(fnc)
    {
    }
    explicit Run(const function_with_context_type &fnc)
        : extended_(fnc)
    {
    }

private:
    virtual core::action::Ptr clone() const override
    {
        if (!!simple_) return core::make_unique<Run>(*simple_);
        assert(!!extended_);
        return core::make_unique<Run>(*extended_);
    }
    virtual bool run(core::Context &context, const Type value) override
    {
        ARGO_MSS_BEGIN(bool);
        ARGO_MSS(!!simple_ ? (*simple_)(value) : (*extended_)(context, value),
            {
                if (context.result().message.empty())
                {
                    context.error() << "Could not process " << core::string::surround(*context.args().current(), "'");
                }
            });
        ARGO_MSS_END();
    }

    core::optional<function_type> simple_;
    core::optional<function_with_context_type> extended_;
};

//Reference implementations
//!Creates an action that accepts a callback with prototype `bool(const Type)`. By default, `Type = std::string`.
template <typename Type = std::string>
Run<Type> run(const typename Run<Type>::function_type &fnc)
{
    return Run<Type>{ fnc };
}
//!Creates an action that accepts a callback with prototype `bool(core::Context &, const Type)`. By default, `Type = std::string`.
template <typename Type = std::string>
Run<Type> run(const typename Run<Type>::function_with_context_type &fnc)
{
    return Run<Type>{ fnc };
}
//Wrappers
//!Creates an action that accepts a callback with prototype `bool(core::Context &)`.
inline Run<std::string> run(const std::function<bool(core::Context &)> &fnc)
{
    auto wrapper = [fnc](core::Context &context, const std::string &value) { fnc(context); return true; };
    return run(wrapper);
}
//!Creates an action that accepts a callback with prototype `void()`. Note that this is the most trivial way of attaching a stateless, non-failing callback to a handler.
inline Run<std::string> run(const std::function<void()> &fnc)
{
    auto wrapper = [fnc](const std::string &value) { fnc(); return true; };
    return run(wrapper);
}

} // namespace action

} }

#endif

#ifndef HEADER_lw_argo_core_Handlers_hpp_INCLUDE_GUARD
#define HEADER_lw_argo_core_Handlers_hpp_INCLUDE_GUARD

#ifndef HEADER_lw_argo_core_assert_hpp_ALREADY_INCLUDED
#define HEADER_lw_argo_core_assert_hpp_ALREADY_INCLUDED


#if ARGO_TOOLSET_DEBUG
#define ARGO_ASSERT_RELEASE(expr) const auto is_ok = expr; assert(!!is_ok)
#else
#define ARGO_ASSERT_RELEASE(expr) expr
#endif

#endif

#ifndef HEADER_lw_argo_core_transform_iterator_hpp_INCLUDE_GUARD
#define HEADER_lw_argo_core_transform_iterator_hpp_INCLUDE_GUARD


namespace lw { namespace argo {

namespace core {

namespace transformation {

template <typename ReturnType>
struct dereference
{
    template <typename It>
    ReturnType operator()(It it) const
    {
        const auto &ptr = *it;
        assert(!!ptr);
        return *ptr;
    }
};

template <typename ReturnType>
struct mapped_type
{
    template <typename It>
    ReturnType operator()(It it) const
    {
        return it->second;
    }
};

template <typename ReturnType>
struct dereference_mapped_type
{
    template <typename It>
    ReturnType operator()(It it) const
    {
        return *it->second;
    }
};

} // namespace transformation

//Transforms the underlying iterator upon dereferencing
template <typename ResultType, typename It>
class transform_iterator
{
public:
    using difference_type = std::ptrdiff_t;
    using value_type = ResultType;
    using pointer = value_type;
    using reference = value_type;
    using iterator_category = typename std::iterator_traits<It>::iterator_category;

    using transformation_type = std::function<ResultType(It)>;

    explicit transform_iterator(const transformation_type &f, It iterator)
        : f_(f)
        , iterator_(iterator)
    {
    }
    explicit transform_iterator(It iterator)
        : f_(transformation_type{})
        , iterator_(iterator)
    {
    }

    value_type operator*() const { return f_(iterator_); }

    pointer operator->() const { return &f_(iterator_); }

    transform_iterator &operator++()
    {
        ++iterator_;
        return *this;
    }

    transform_iterator operator++(int)
    {
        transform_iterator tmp{ *this };
        this->operator++();
        return tmp;
    }

    transform_iterator &operator--()
    {
        --iterator_;
        return *this;
    }

    transform_iterator operator--(int)
    {
        transform_iterator tmp{ *this };
        this->operator--();
        return tmp;
    }

    transform_iterator &operator+=(const unsigned n)
    {
        iterator_ += n;
        return *this;
    }

    transform_iterator &operator-=(const unsigned n)
    {
        iterator_ -= n;
        return *this;
    }

    transform_iterator operator+(const unsigned n) const
    {
        transform_iterator tmp{ *this };
        tmp += n;
        return tmp;
    }

    transform_iterator operator-(const unsigned n) const
    {
        transform_iterator tmp{ *this };
        tmp -= n;
        return tmp;
    }

    difference_type operator-(const transform_iterator &other) const { return iterator_ - other.iterator_; }

    bool operator==(const transform_iterator &other) const { return iterator_ == other.iterator_; }

    bool operator!=(const transform_iterator &other) const { return !this->operator==(other); }

    bool operator<(const transform_iterator &other) const { return iterator_ < other.iterator_; }

    It iterator() const { return iterator_; }

private:
    transformation_type f_;
    It iterator_;
};

template <typename ResultType, typename It>
transform_iterator<ResultType, It> make_transform_iterator(typename transform_iterator<ResultType, It>::transformation_type f, It iterator)
{
    return transform_iterator<ResultType, It>{ f, iterator };
}

} // namespace core

} }

#endif

namespace lw { namespace argo {

namespace handler {

class Positional;
}

namespace core {

class Handlers
{
    using data_type = std::vector<handler::Ptr>;

public:
    using value_type = handler::IHandler &;
    using reference = value_type;
    using const_iterator = transform_iterator<reference, typename data_type::const_iterator>;
    using iterator = transform_iterator<reference, typename data_type::iterator>;

    Handlers() = default;
    Handlers(const handler::IHandler &parent)
        : pparent_(&parent)
    {
    }
    Handlers(const Handlers &other)
        : pparent_(other.pparent_)
    {
        for (const auto &handler : other)
        {
            ARGO_ASSERT_RELEASE(add(handler));
        }
    }
    Handlers &operator=(const Handlers &other)
    {
        if (this != &other)
        {
            handlers_.clear();
            hashes_.clear();
            pparent_ = nullptr;
            for (const auto &handler : other)
            {
                ARGO_ASSERT_RELEASE(add(handler));
            }
        }
        return *this;
    }

    const_iterator begin() const { return make_transform_iterator<reference>(transformation::dereference<reference>{}, std::begin(handlers_)); }
    iterator begin() { return make_transform_iterator<reference>(transformation::dereference<reference>{}, std::begin(handlers_)); }
    const_iterator end() const { return make_transform_iterator<reference>(transformation::dereference<reference>{}, std::end(handlers_)); }
    iterator end() { return make_transform_iterator<reference>(transformation::dereference<reference>{}, std::end(handlers_)); }
    bool has(const handler::IHandler &handler) { return hashes_.count(handler.name()); }
    bool add(const handler::IHandler &handler)
    {
        ARGO_S(ARGO_DEBUG);
        ARGO_MSS_BEGIN(bool);
        ARGO_MSS(handler.is_valid());
        const auto hash = handler.name();
        ARGO_L("Adding handler with hash " << ARGO_C(hash));
        ARGO_MSS(!hashes_.count(hash));
        hashes_[hash] = handlers_.size();
        handlers_.push_back(handler.clone());
        if (!!pparent_) back().set_parent(*pparent_);
        ARGO_MSS_END();
    }
    bool remove(const handler::IHandler &handler)
    {
        ARGO_MSS_BEGIN(bool);
        ARGO_MSS(has(handler));
        const auto hash = handler.name();
        const auto idx = hashes_[hash];
        assert(idx < handlers_.size());
        assert(handlers_[idx]->name() == hash);
        hashes_.erase(hash);
        const auto it = std::begin(handlers_) + idx;
        handlers_.erase(it);
        reindex_(idx);
        ARGO_MSS_END();
    }
    bool empty() const { return handlers_.empty(); }
    unsigned size() const { return handlers_.size(); }
    const handler::IHandler &back() const
    {
        assert(!handlers_.empty());
        const auto &ptr = handlers_.back();
        assert(!!ptr);
        return *ptr;
    }

    bool accept_on_elements(handler::visitor::IConstVisitor &visitor) const
    {
        ARGO_MSS_BEGIN(bool);
        for (const auto &phandler : handlers_)
        {
            assert(!!phandler);
            ARGO_MSS(phandler->accept(visitor));
        }
        ARGO_MSS_END();
    }
    bool accept_on_elements(handler::visitor::IMutatingVisitor &visitor) const
    {
        ARGO_MSS_BEGIN(bool);
        for (const auto &phandler : handlers_)
        {
            assert(!!phandler);
            ARGO_MSS(phandler->accept(visitor));
        }
        ARGO_MSS_END();
    }
    void set_parent(const handler::IHandler &handler)
    {
        pparent_ = &handler;
        for (const auto &hdlr: *this) hdlr.set_parent(handler);
    }

private:
    void reindex_(unsigned idx)
    {
        for (auto &item : hashes_)
            if (item.second > idx) --item.second;
    }

    const handler::IHandler *pparent_ = nullptr;
    data_type handlers_;
    std::map<std::string, unsigned> hashes_;
};

} // namespace core

} }

#endif
#ifndef HEADER_lw_argo_core_handler_search_Custom_hpp_INCLUDE_GUARD
#define HEADER_lw_argo_core_handler_search_Custom_hpp_INCLUDE_GUARD

#ifndef HEADER_lw_argo_core_handler_search_ISearch_hpp_INCLUDE_GUARD
#define HEADER_lw_argo_core_handler_search_ISearch_hpp_INCLUDE_GUARD

#ifndef HEADER_lw_argo_handler_group_Interface_hpp_INCLUDE_GUARD
#define HEADER_lw_argo_handler_group_Interface_hpp_INCLUDE_GUARD

#ifndef HEADER_lw_argo_core_handler_property_Required_hpp_INCLUDE_GUARD
#define HEADER_lw_argo_core_handler_property_Required_hpp_INCLUDE_GUARD


namespace lw { namespace argo {

namespace core { namespace handler { namespace property {

template <typename Handler>
class Required
{
public:
    Required() = default;
    Required(const Required &other) = default;

    //!Indicates if the handler is required.
    bool is_required() const { return is_required_; }
    //!Marks the handler as required and returns it.
    Handler &required()
    {
        is_required_ = true;
        return static_cast<Handler &>(*this);
    }
    //!Indicates if the handler is optional. This is the default state.
    bool is_optional() const { return !is_required(); }
    //!Marks the handler as optional and returns it.
    Handler &optional()
    {
        is_required_ = false;
        return static_cast<Handler &>(*this);
    }

protected:
    bool is_satisfied_(core::Context &context) const { return is_required() ? context.info(static_cast<const Handler &>(*this)).nr_parses : true; }

private:
    bool is_required_ = false;
};

}}} // namespace core::handler::property

} }

#endif
#ifndef HEADER_lw_argo_core_handler_visitor_IsType_hpp_INCLUDE_GUARD
#define HEADER_lw_argo_core_handler_visitor_IsType_hpp_INCLUDE_GUARD

#ifndef HEADER_lw_argo_core_handler_all_hpp_INCLUDE_GUARD
#define HEADER_lw_argo_core_handler_all_hpp_INCLUDE_GUARD

#ifndef HEADER_lw_argo_handler_Flag_hpp_INCLUDE_GUARD
#define HEADER_lw_argo_handler_Flag_hpp_INCLUDE_GUARD

#ifndef HEADER_lw_argo_core_handler_property_Actionable_hpp_INCLUDE_GUARD
#define HEADER_lw_argo_core_handler_property_Actionable_hpp_INCLUDE_GUARD

#ifndef HEADER_lw_argo_action_store_hpp_INCLUDE_GUARD
#define HEADER_lw_argo_action_store_hpp_INCLUDE_GUARD

#include <list>
#include <set>

namespace lw { namespace argo {

namespace action {

//!Stores the parsed value in the given variable.
template <typename Type>
Run<Type> store(Type &variable)
{
    auto set = [&variable](Type value) { variable = value; return true; };
    return Run<Type>{ set };
}
//!Stores the `const_value` in the given variable when the associated handler triggers. `ConstType` must be copy-assignable to `Type`.
template <typename Type, typename ConstType>
Run<Type> store(Type &variable, ConstType const_value)
{
    auto set = [&variable, &const_value](Type value) { variable = const_value; return true; };
    return Run<Type>{ set };
}
//!Appends the parsed values in-order to the given vector.
template <typename Type>
Run<Type> store(std::vector<Type> &variable)
{
    auto set = [&variable](Type value) { variable.push_back(value); return true; };
    return Run<Type>{ set };
}
//!Appends the parsed values in-order to the given list.
template <typename Type>
Run<Type> store(std::list<Type> &variable)
{
    auto set = [&variable](Type value) { variable.push_back(value); return true; };
    return Run<Type>{ set };
}
//!Inserts the parsed values in the given set.
template <typename Type>
Run<Type> store(std::set<Type> &variable)
{
    auto set = [&variable](Type value) { variable.insert(value); return true; };
    return Run<Type>{ set };
}

} // namespace action

} }

#endif

#ifndef HEADER_lw_argo_core_type_traits_hpp_INCLUDE_GUARD
#define HEADER_lw_argo_core_type_traits_hpp_INCLUDE_GUARD


namespace lw { namespace argo {

namespace core {

template <typename Variable>
bool is_collection(const Variable &variable)
{
    return false;
}
template <typename Type>
bool is_collection(const std::vector<Type> &variable)
{
    return true;
}
template <typename Type>
bool is_collection(const std::list<Type> &variable)
{
    return true;
}
template <typename Type>
bool is_collection(const std::set<Type> &variable)
{
    return true;
}

} // namespace core

} }

#endif


namespace lw { namespace argo {

namespace core { namespace handler { namespace property {

template <typename Handler>
class Actionable
{
public:
    Actionable() = default;
    Actionable(const Actionable &other)
    {
        assert(actions_().empty());
        for (const auto &paction : other.actions_())
        {
            assert(!!paction);
            actions_().push_back(paction->clone()); //Not calling action() as this results in a false positive with ASAN (invalid downcast)
        }
    }

    //!Adds an action to the handler and returns it.
    Handler &action(const action::IAction &action)
    {
        actions_().push_back(action.clone());
        return static_cast<Handler &>(*this);
    }
    //!Adds a requirement to the handler and returns it. Note that this is syntactic sugar since it has the same effect as calling ``action``.
    Handler &require(const action::IAction &requirement) { return action(requirement); }

protected:
    const std::vector<action::Ptr> &actions_() const { return actions__; }
    std::vector<action::Ptr> &actions_() { return actions__; }
    bool run_(core::Context &context)
    {
        ARGO_S(ARGO_DEBUG);
        ARGO_MSS_BEGIN(bool);
        for (const auto &paction : actions_())
        {
            assert(!!paction);
            ARGO_L("Applying action on value");
            ARGO_MSS(paction->apply(context));
            ARGO_L("Finished applying action on value");
        }
        ARGO_MSS_END();
    }
    template<typename Variable>
    void add_store_action_(Variable &variable)
    {
        action(ARGO_NS(action::store(variable)));
        if (core::is_collection(variable)) static_cast<Handler &>(*this).nargs("*");
        else static_cast<Handler &>(*this).nargs(1);
    }

private:
    std::vector<action::Ptr> actions__;
};

}}} // namespace core::handler::property

} }

#endif
#ifndef HEADER_lw_argo_core_handler_property_Named_hpp_INCLUDE_GUARD
#define HEADER_lw_argo_core_handler_property_Named_hpp_INCLUDE_GUARD

#ifndef HEADER_lw_argo_core_utility_hpp_INCLUDE_GUARD
#define HEADER_lw_argo_core_utility_hpp_INCLUDE_GUARD

#include <exception>

namespace lw { namespace argo {

namespace core {

inline std::string basename_of(const std::string &path)
{
    return std::string(std::find_if(path.rbegin(), path.rend(), [](const char ch) {
#if ARGO_TOOLSET_PLATFORM_WINDOWS
                           return '/' == ch || '\\' == ch;
#else
                           return '/' == ch;
#endif
                       })
                           .base(),
                       path.end());
}
inline bool is_number(const std::string &value)
{
    float dummy;
    const auto chars_used = convert(dummy, value);
    return chars_used == value.size();
}

namespace name {

inline bool is_shorthand(const std::string &name)
{
    ARGO_MSS_BEGIN(bool);
    ARGO_MSS(!name.empty());
    ARGO_MSS(!is_number(name));
    ARGO_MSS_Q(string::starts_with(name, "-"));
    ARGO_MSS_END();
}
inline bool is_longhand(const std::string &name)
{
    ARGO_MSS_BEGIN(bool);
    ARGO_MSS(!name.empty());
    ARGO_MSS_Q(2 < name.size());
    ARGO_MSS_Q(string::starts_with(name, "--"));
    ARGO_MSS_END();
}
inline bool is_option(const std::string &name)
{
    ARGO_MSS_BEGIN(bool);
    ARGO_MSS_Q(is_shorthand(name) || is_longhand(name));
    ARGO_MSS_END();
}
inline bool is_positional(const std::string &name)
{
    ARGO_MSS_BEGIN(bool);
    ARGO_MSS(!name.empty());
    ARGO_MSS(!is_shorthand(name));
    ARGO_MSS(!is_longhand(name));
    ARGO_MSS(!is_number(name));
    ARGO_MSS_END();
}
inline std::string guess_shorthand(const std::string &longhand)
{
    assert(is_longhand(longhand));
    return std::string("-") + longhand[2];
}
} // namespace name

inline std::vector<std::string> normalize_arguments(const std::vector<std::string> &arguments)
{
    ARGO_S(false);
    std::vector<std::string> args;
    args.reserve(arguments.size());
    for (const auto &argument : arguments)
    {
        ARGO_L(ARGO_C(argument));
        const auto &kv = string::split(argument, "=");
        if (1 < kv.size())
        {
            const auto &name = kv[0];
            if (name::is_option(name))
            {
                ARGO_L(ARGO_C(name));
                args.push_back(name);
                const std::string value(std::begin(argument) + name.size() + 1, std::end(argument));
                if (string::is_delimited(value, "'") || string::is_delimited(value, "\""))
                {
                    ARGO_L(ARGO_C(value));
                    args.push_back(value);
                    continue;
                }
                else
                {
                    const auto &values = string::split(value, ",");
                    for (const auto &value : values)
                    {
                        ARGO_L(ARGO_C(value));
                        args.push_back(value);
                    }
                    continue;
                }
            }
        }
        args.push_back(argument);
    }
    return args;
}

} // namespace core

} }

#endif


namespace lw { namespace argo {

namespace core { namespace handler { namespace property {

class Named
{
public:
    explicit Named(std::string name)
    {
        dash(name);
        prefix(name);
        if (name::is_longhand(name))
            longhand_ = name;
        else if (name::is_shorthand(name))
            shorthand_ = name;
        validate_();
    }
    explicit Named(std::string first, std::string second)
    {
        dash(first);
        dash(second);
        prefix(first, second);
        if (name::is_longhand(first))
            longhand_ = first;
        else if (name::is_shorthand(first))
            shorthand_ = first;
        if (name::is_longhand(second))
            longhand_ = second;
        else if (name::is_shorthand(second))
            shorthand_ = second;
        validate_();
    }
    Named(const Named &other) = default;

    //!Indicates if a longhand is set.
    bool has_longhand() const { return !!longhand_; }
    //!Returns the longhand. Note that the longhand must be set.
    std::string longhand() const
    {
        assert(has_longhand());
        return *longhand_;
    }
    //!Indicates if a shorthand is set.
    bool has_shorthand() const { return !!shorthand_; }
    //!Returns the shorthand. Note that the shorthand must be set.
    std::string shorthand() const
    {
        assert(has_shorthand());
        return *shorthand_;
    }

protected:
    void validate_()
    {
        is_valid__ = has_longhand() || has_shorthand();
        if (has_longhand()) is_valid__ = name::is_longhand(*longhand_);
        if (has_shorthand()) is_valid__ = is_valid__ && name::is_shorthand(*shorthand_);
    }
    std::string name_() const
    {
        std::ostringstream os;
        if (has_longhand())
        {
            os << longhand();
            if (has_shorthand()) os << " (" << shorthand() << ")";
        }
        else if (has_shorthand())
        {
            os << shorthand();
        }
        return os.str();
    }
    bool is_valid_() const { return is_valid__; }

private:
    core::optional<std::string> longhand_;
    core::optional<std::string> shorthand_;
    bool is_valid__ = true;
};

}}} // namespace core::handler::property

} }

#endif

namespace lw { namespace argo {

namespace handler {

//!A handler that processes flags: shorthand or longhand arguments without values.
class Flag : public core::handler::IHandler,
             public core::handler::property::Named,
             public core::handler::property::Help<Flag>,
             public core::handler::property::Required<Flag>,
             public core::handler::property::Actionable<Flag>
{
public:
    //!Constructs a flag with a longhand and shorthand. They may be provided in any order.
    explicit Flag(const std::string &first, const std::string &second)
        : core::handler::property::Named(first, second)
    {
    }
    //!Constructs an option with a longhand or shorthand.
    explicit Flag(const std::string &name)
        : core::handler::property::Named(name)
    {
    }

    virtual bool is_valid() const override
    {
        ARGO_MSS_BEGIN(bool);
        ARGO_MSS(core::handler::property::Named::is_valid_());
        ARGO_MSS_END();
    }
    virtual std::string name() const override { return core::handler::property::Named::name_(); }
    virtual std::string type() const override { return "Flag"; }
    virtual bool accept(core::handler::visitor::IConstVisitor &visitor) const override { return visitor.visit(*this); }
    virtual bool accept(core::handler::visitor::IMutatingVisitor &visitor) override { return visitor.visit(*this); }
    virtual core::handler::Ptr clone() const override { return core::make_unique<Flag>(*this); }
    virtual bool parse(core::Context &context) override
    {
        ARGO_S(ARGO_DEBUG);
        ARGO_MSS_BEGIN(bool);
        ARGO_L("Flag parsing...");

        //First process the name
        ARGO_MSS(context.args().current() != context.args().end());
        const auto name = *context.args().current();

        //Running actions
        ARGO_L("Running actions: " << ARGO_C(actions_().size()));
        ARGO_MSS(core::handler::property::Actionable<Flag>::run_(context));
        ARGO_L("All actions were run");

        //Consume the name _after_ applying the actions, so that they get the flag name as value
        ARGO_L("Consuming " << ARGO_C(name));
        ARGO_MSS(context.args().next());

        ARGO_L("Finished flag parsing");
        ARGO_MSS_END();
    }
    virtual bool is_satisfied(core::Context &context) const override
    {
        ARGO_MSS_BEGIN(bool);
        ARGO_MSS(core::handler::property::Required<Flag>::is_satisfied_(context), context.error() << core::handler::name_of(*this, true) << " is required");
        ARGO_MSS_END();
    }
};

} // namespace handler

} }

#endif

#ifndef HEADER_lw_argo_handler_Option_hpp_INCLUDE_GUARD
#define HEADER_lw_argo_handler_Option_hpp_INCLUDE_GUARD

#ifndef HEADER_lw_argo_core_handler_property_Cardinality_hpp_INCLUDE_GUARD
#define HEADER_lw_argo_core_handler_property_Cardinality_hpp_INCLUDE_GUARD

#ifndef HEADER_lw_argo_nargs_FixedNumber_hpp_INCLUDE_GUARD
#define HEADER_lw_argo_nargs_FixedNumber_hpp_INCLUDE_GUARD

#ifndef HEADER_lw_argo_core_nargs_INArgs_hpp_INCLUDE_GUARD
#define HEADER_lw_argo_core_nargs_INArgs_hpp_INCLUDE_GUARD


namespace lw { namespace argo {

namespace core { namespace nargs {

class INArgs;
using Ptr = std::unique_ptr<INArgs>;

class INArgs
{
public:
    virtual ~INArgs() = default;

    virtual std::string description() const = 0;
    virtual std::string symbol() const = 0;

    virtual Ptr clone() const = 0;
    virtual bool must_have_next() const = 0;
    virtual bool may_have_next() const = 0;
    virtual void next() = 0;
    virtual void reset() = 0;
};

}} // namespace core::nargs

} }

#endif


namespace lw { namespace argo {

namespace nargs {

class FixedNumber : public core::nargs::INArgs
{
public:
    explicit FixedNumber(const unsigned value)
        : value_(value)
    {
    }

    virtual std::string description() const override
    {
        std::ostringstream os;
        if (0 == value_)
            os << "no arguments";
        else
            os << value_ << " argument" << (value_ > 1 ? "s" : "");
        return os.str();
    }
    virtual std::string symbol() const override { return std::to_string(value_); }

    virtual core::nargs::Ptr clone() const override { return std::unique_ptr<FixedNumber>(new FixedNumber{ *this }); }
    virtual bool must_have_next() const override { return count_ < value_; };
    virtual bool may_have_next() const override { return must_have_next(); }
    virtual void next() override { ++count_; }
    virtual void reset() override { count_ = 0; }

    unsigned value() const { return value_; }

private:
    FixedNumber(const FixedNumber &other) = default;

    unsigned value_;
    unsigned count_ = 0;
};

} // namespace nargs

} }

#endif

#ifndef HEADER_lw_argo_nargs_OneOrMore_hpp_INCLUDE_GUARD
#define HEADER_lw_argo_nargs_OneOrMore_hpp_INCLUDE_GUARD


namespace lw { namespace argo {

namespace nargs {

class OneOrMore : public core::nargs::INArgs
{
public:
    OneOrMore() = default;
    virtual std::string description() const override { return "one or more arguments"; }
    virtual std::string symbol() const override { return "+"; }

    virtual core::nargs::Ptr clone() const override { return std::unique_ptr<OneOrMore>(new OneOrMore{ *this }); }
    virtual bool must_have_next() const override { return count_ == 0; };
    virtual bool may_have_next() const override { return true; }
    virtual void next() override { ++count_; }
    virtual void reset() override { count_ = 0; }

private:
    OneOrMore(const OneOrMore &other) = default;

    unsigned count_ = 0;
};

} // namespace nargs

} }

#endif

#ifndef HEADER_lw_argo_nargs_Optional_hpp_INCLUDE_GUARD
#define HEADER_lw_argo_nargs_Optional_hpp_INCLUDE_GUARD


namespace lw { namespace argo {

namespace nargs {

class Optional : public core::nargs::INArgs
{
public:
    Optional() = default;
    virtual std::string description() const override { return "an optional argument"; }
    virtual std::string symbol() const override { return "?"; }

    virtual core::nargs::Ptr clone() const override { return std::unique_ptr<Optional>(new Optional{ *this }); }
    virtual bool must_have_next() const override { return false; };
    virtual bool may_have_next() const override { return count_ == 0; }
    virtual void next() override { ++count_; }
    virtual void reset() override { count_ = 0; }

private:
    Optional(const Optional &other) = default;

    unsigned count_ = 0;
};

} // namespace nargs

} }

#endif

#ifndef HEADER_lw_argo_nargs_ZeroOrMore_hpp_INCLUDE_GUARD
#define HEADER_lw_argo_nargs_ZeroOrMore_hpp_INCLUDE_GUARD


namespace lw { namespace argo {

namespace nargs {

class ZeroOrMore : public core::nargs::INArgs
{
public:
    ZeroOrMore() = default;
    virtual std::string description() const override { return "zero or more arguments"; }
    virtual std::string symbol() const override { return "*"; }

    virtual core::nargs::Ptr clone() const override { return std::unique_ptr<ZeroOrMore>(new ZeroOrMore{ *this }); }
    virtual bool must_have_next() const override { return false; };
    virtual bool may_have_next() const override { return true; }
    virtual void next() override {}
    virtual void reset() override {}

private:
    ZeroOrMore(const ZeroOrMore &other) = default;
};

} // namespace nargs

} }

#endif


namespace lw { namespace argo {

namespace core { namespace handler { namespace property {

template <typename Handler>
class Cardinality
{
public:
    Cardinality()
        : pnargs_(make_unique<ARGO_NS(nargs::FixedNumber)>(1))
    {
    }
    Cardinality(const Cardinality &other)
        : pnargs_(!!other.pnargs_ ? other.pnargs_->clone() : nullptr)
    {
    }

    //!Sets the expected number of arguments and returns the handler.
    Handler &nargs(const std::string &type)
    {
        if ("+" == type)
            pnargs_ = make_unique<ARGO_NS(nargs::OneOrMore)>();
        else if ("*" == type)
            pnargs_ = make_unique<ARGO_NS(nargs::ZeroOrMore)>();
        else if ("?" == type)
            pnargs_ = make_unique<ARGO_NS(nargs::Optional)>();
        else
            is_valid__ = false;
        return static_cast<Handler &>(*this);
    }
    //!Sets the expected fixed number of arguments and returns the handler.
    Handler &nargs(const unsigned nr)
    {
        if (0 == nr)
            is_valid__ = false;
        else
            pnargs_ = make_unique<ARGO_NS(nargs::FixedNumber)>(nr);
        return static_cast<Handler &>(*this);
    }
    //!Returns the cardinality description.
    std::string nargs_type() const { return nargs_().description(); }
    //!Returns the cardinality symbol.
    std::string nargs_symbol() const { return nargs_().symbol(); }

protected:
    const core::nargs::INArgs &nargs_() const
    {
        assert(!!pnargs_);
        return *pnargs_;
    }
    core::nargs::INArgs &nargs_()
    {
        assert(!!pnargs_);
        return *pnargs_;
    }
    bool is_valid_() const { return is_valid__; }
    bool is_satisfied_() const
    {
        ARGO_MSS_BEGIN(bool);
        ARGO_MSS(!nargs_().must_have_next());
        ARGO_MSS_END();
    }

private:
    core::nargs::Ptr pnargs_;
    bool is_valid__ = true;
};

}}} // namespace core::handler::property

} }

#endif

namespace lw { namespace argo {

namespace handler {

//!Handler that processes arguments with values.
class Option : public core::handler::IHandler,
               public core::handler::property::Named,
               public core::handler::property::Help<Option>,
               public core::handler::property::Required<Option>,
               public core::handler::property::Cardinality<Option>,
               public core::handler::property::Actionable<Option>
{
public:
    //!Constructs an option with a longhand and shorthand. They may be provided in any order. This constructor is useful if no direct variable binding can be done. Using this constructor implies adding a custom action later in order to create a valid Option.
    explicit Option(const std::string &first, const std::string &second)
        : core::handler::property::Named(first, second)
    {
    }
    //!Constructs an option with a longhand or shorthand. This constructor is useful if no direct variable binding can be done. Using this constructor implies adding a custom action later in order to create a valid Option.
    explicit Option(const std::string &name)
        : core::handler::property::Named(name)
    {
    }
    //!Constructs an option with a longhand and shorthand. They may be provided in any order. This constructor is useful if no direct variable binding can be done. Using this constructor implies adding a custom action later in order to create a valid Option.
    explicit Option(const char *first, const char *second)
        : Option(std::string(first), std::string(second))
    {
    }
    //!Constructs an option with a longhand or shorthand.
    explicit Option(const char *name)
        : Option(std::string(name))
    {
    }
    //!Constructs an option with a longhand and shorthand. They may be provided in any order. Any values will be stored in the given variable.
    template <typename Variable, typename = typename std::enable_if<!std::is_const<Variable>::value>::type>
    explicit Option(const std::string &first, const std::string &second, Variable &variable)
        : Option(first, second)
    {
        add_store_action_(variable);
    }
    //!Constructs an option with a longhand or shorthand. Any values will be stored in the given variable.
    template <typename Variable, typename = typename std::enable_if<!std::is_const<Variable>::value>::type>
    explicit Option(const std::string &name, Variable &variable)
        : Option(name)
    {
        add_store_action_(variable);
    }

    virtual bool is_valid() const override
    {
        ARGO_MSS_BEGIN(bool);
        ARGO_MSS(!actions_().empty());
        ARGO_MSS(core::handler::property::Named::is_valid_());
        ARGO_MSS(core::handler::property::Cardinality<Option>::is_valid_());
        ARGO_MSS_END();
    }
    virtual std::string name() const override { return core::handler::property::Named::name_(); }
    virtual std::string type() const override { return "Option"; }
    virtual bool accept(core::handler::visitor::IConstVisitor &visitor) const override { return visitor.visit(*this); }
    virtual bool accept(core::handler::visitor::IMutatingVisitor &visitor) override { return visitor.visit(*this); }
    virtual core::handler::Ptr clone() const override { return core::make_unique<Option>(*this); }
    virtual bool parse(core::Context &context) override
    {
        ARGO_S(ARGO_DEBUG);
        ARGO_MSS_BEGIN(bool);
        ARGO_L("Option parsing...");

        //First process the name
        ARGO_MSS(!context.args().reached_end());
        ARGO_L("Consuming name '" << *context.args().current() << "'");
        ARGO_MSS(context.args().next());

        if (context.config().parser.implicit_values)
        {
            //Next, process any values step by step
            ARGO_L("Processing implicit value(s)");
            while (!context.args().reached_end())
            {
                bool stop_processing = false;
                ARGO_MSS(process_value_(context, stop_processing));
                if (stop_processing) ARGO_MSS_RETURN_OK();
            }
        }
        else
        {
            // One value
            ARGO_L("Processing explicit value");
            bool stop_processing = false;
            if (!context.args().reached_end()) ARGO_MSS(process_value_(context, stop_processing));
            if (stop_processing) ARGO_MSS_RETURN_OK();
        }

        ARGO_L("Finished option parsing");
        ARGO_MSS_END();
    }
    virtual bool is_satisfied(core::Context &context) const override
    {
        ARGO_MSS_BEGIN(bool);
        ARGO_MSS(core::handler::property::Required<Option>::is_satisfied_(context), context.error() << core::handler::name_of(*this, true) << " is required");
        ARGO_MSS(is_optional() || core::handler::property::Cardinality<Option>::is_satisfied_(), context.error() << core::handler::name_of(*this, true) << " expects " << nargs_().description());
        ARGO_MSS_END();
    }

private:
    virtual void reset_() override { nargs_().reset(); }
    bool process_value_(core::Context &context, bool &stop_processing)
    {
        ARGO_S(ARGO_DEBUG);
        ARGO_MSS_BEGIN(bool);
        const auto value = *context.args().current();
        ARGO_L("Considering consuming " << ARGO_C(value));
        if (!context.config().parser.empty_values && value.empty())
        {
            ARGO_L("Argument is empty, not configured to accept this");
            context.error() << core::handler::name_of(*this, true) << " expects non-empty arguments";
            stop_processing = true;
            ARGO_MSS_RETURN_ERROR(); // We explicitly reject this
        }
        if (context.parser_could_continue(context))
        {
            ARGO_L("Yielding back, the toplevel parser definitely knows what to do");
            stop_processing = true;
            ARGO_MSS_RETURN_OK(); // Let someone else handle this
        }
        else if (!context.config().parser.argument_values && core::name::is_option(value))
        {
            ARGO_L("Argument like value, not configured to accept this");
            stop_processing = true;
            ARGO_MSS_RETURN_OK(); // Let someone else handle this
        }

        //Check if we're allowed another value
        if (!nargs_().may_have_next())
        {
            const auto last_arg = std::distance(context.args().current(), context.args().end()) == 1;
            switch (context.phase())
            {
                case core::Phase::OptionsFirstPass:
                    {
                        if (core::name::is_option(value))
                        {
                            ARGO_L("The value is an option which is unknown so let the toplevel parser handle the failure");
                            stop_processing = true;
                            ARGO_MSS_RETURN_OK();
                        }
                        else if (last_arg)
                        {
                            ARGO_L("This is the last value, let's see if the positional phase will allow the toplevel parser to continue");
                            context.next_phase();
                            if (!context.parser_could_continue(context))
                            {
                                ARGO_L("We can't have another value: the positional phase did not resolve anything");
                                context.error() << "Unexpected value '" << value << "' for " << core::handler::name_of(*this) << ": expected " << nargs_().description();
                                ARGO_MSS_RETURN_ERROR();
                            }
                            ARGO_L("Yielding back, the toplevel parser knows how to handle this value as a positional");
                            stop_processing = true;
                            ARGO_MSS_RETURN_OK();
                        }
                    }
                    break;
                case core::Phase::Positionals: break;
                case core::Phase::OptionsLastPass:
                                               {
                                                   ARGO_L("We can't have another value because we have already had the positionals phase");
                                                   context.error() << "Unexpected value '" << value << "' for " << core::handler::name_of(*this) << ": expected " << nargs_().description();
                                                   ARGO_MSS_RETURN_ERROR();
                                               }
                                               break;
                case core::Phase::Nr_: break;
            }
            ARGO_L("Let the toplevel parser handle the failure");
            stop_processing = true;
            ARGO_MSS_RETURN_OK();
        }

        //Running all actions
        assert(!actions_().empty()); //If this assertion triggers, it most likely means you have used a wrong constructor for Option.
        //The most common case is when you have dynamically created the option names and passed them as non-const references.
        //For example: Option{name_one, name_two} where name_one, name_two are non-const references to std::string.
        //In this case, the compiler might choose the templated constructor: Option{const std::string &, Variable &}
        //To work around this, use the correct types. If this is a big nuisance, consider filing a bug to request a change in the API.
        ARGO_L("Running actions: " << ARGO_C(actions_().size()));
        ARGO_MSS(core::handler::property::Actionable<Option>::run_(context));
        ARGO_L("All actions were run");

        //Consume value
        ARGO_L("Consuming " << ARGO_C(value));
        ARGO_MSS(context.args().next());

        //Update properties
        nargs_().next();
        ARGO_MSS_END();
    }
};

} // namespace handler

} }

#endif

#ifndef HEADER_lw_argo_handler_Positional_hpp_INCLUDE_GUARD
#define HEADER_lw_argo_handler_Positional_hpp_INCLUDE_GUARD


namespace lw { namespace argo {

namespace handler {

//!A handler that processes positional arguments: unnamed arguments that signal values.
class Positional : public core::handler::IHandler,
                   public core::handler::property::Help<Positional>,
                   public core::handler::property::Required<Positional>,
                   public core::handler::property::Cardinality<Positional>,
                   public core::handler::property::Actionable<Positional>
{
public:
    //!Constructs the positional handler with the given name. Note that _the name may not be a shorthand or longhand_. It is merely used in the generation of help output and error messages.
    explicit Positional(const std::string &name)
        : name_(name)
    {
    }
    //!Constructs the positional handler with the given name. Note that _the name may not be a shorthand or longhand_. It is merely used in the generation of help output and error messages. Any values are stored in the given variable.
    template <typename Variable>
    explicit Positional(const std::string &name, Variable &variable)
        : Positional(name)
    {
        add_store_action_(variable);
    }

    virtual bool is_valid() const override
    {
        ARGO_MSS_BEGIN(bool);
        ARGO_MSS(core::handler::property::Cardinality<Positional>::is_valid_());
        ARGO_MSS(!core::name::is_shorthand(name_) && !core::name::is_longhand(name_));
        ARGO_MSS_END();
    }
    virtual std::string name() const override { return name_; }
    virtual std::string type() const override { return "Positional argument"; }
    virtual bool accept(core::handler::visitor::IConstVisitor &visitor) const override { return visitor.visit(*this); }
    virtual bool accept(core::handler::visitor::IMutatingVisitor &visitor) override { return visitor.visit(*this); }
    virtual core::handler::Ptr clone() const override { return core::make_unique<Positional>(*this); }
    virtual bool parse(core::Context &context) override
    {
        ARGO_S(ARGO_DEBUG);
        ARGO_MSS_BEGIN(bool);
        ARGO_L("Positional parsing...");
        ARGO_MSS(context.info(*this).nr_parses, ARGO_L("Skipping this handler, as it has already run..."));

        //First process the name
        ARGO_MSS(!context.args().reached_end());
        const auto name = *context.args().current();

        //Next, process any values
        ARGO_L("Processing value(s)");
        while (!context.args().reached_end())
        {
            const auto value = *context.args().current();
            ARGO_L("Considering consuming " << ARGO_C(value));
            if (context.parser_could_continue(context))
            {
                ARGO_L("Yielding back, the toplevel parser knows what to do");
                ARGO_MSS_RETURN_OK();
            }

            //We process all values that we are allowed to process
            if (!nargs_().may_have_next())
            {
                if (this == last_positional_(context))
                {
                    if (!core::name::is_option(value))
                    {
                        ARGO_L("We can't have another value");
                        context.error() << "Unexpected value '" << value << "' for " << core::handler::name_of(*this) << ": expected " << nargs_().description();
                        ARGO_MSS_RETURN_ERROR();
                    }
                    else
                    {
                        ARGO_L("We can't have another value. The value is an option which we know the parser can't continue with, but we yield anyway");
                        ARGO_MSS_RETURN_OK();
                    }
                } else
                {
                    ARGO_L("Yielding back");
                    ARGO_MSS_RETURN_OK();
                }
            }

            //Running all actions
            ARGO_L("Running actions: " << ARGO_C(actions_().size()));
            ARGO_MSS(core::handler::property::Actionable<Positional>::run_(context));
            ARGO_L("All actions were run");

            //Consume value
            ARGO_L("Consuming " << ARGO_C(value));
            ARGO_MSS(context.args().next());

            //Update properties
            nargs_().next();
        }
        ARGO_MSS(!nargs_().must_have_next(), context.error() << "Missing value(s) for " << core::handler::name_of(*this) << ": expected " << nargs_().description(););

        ARGO_L("Finished option parsing");
        ARGO_MSS_END();
    }
    virtual bool is_satisfied(core::Context &context) const override
    {
        ARGO_MSS_BEGIN(bool);
        ARGO_MSS(core::handler::property::Required<Positional>::is_satisfied_(context), context.error() << core::handler::name_of(*this, true) << " is required";);
        ARGO_MSS(is_optional() || core::handler::property::Cardinality<Positional>::is_satisfied_(), context.error() << core::handler::name_of(*this, true) << " expects " << nargs_().description(););
        ARGO_MSS_END();
    }

private:
    virtual void reset_() override { nargs_().reset(); }
    Positional *last_positional_(core::Context &context) const
    {
        //#TODO: implement this to get better error reporting
        return nullptr;
    }

    std::string name_;
};

} // namespace handler

} }

#endif

#ifndef HEADER_lw_argo_handler_Toggle_hpp_INCLUDE_GUARD
#define HEADER_lw_argo_handler_Toggle_hpp_INCLUDE_GUARD


namespace lw { namespace argo {

namespace handler {

//!A handler that processes toggles: arguments that toggle boolean values.
class Toggle : public core::handler::IHandler,
               public core::handler::property::Named,
               public core::handler::property::Help<Toggle>,
               public core::handler::property::Required<Toggle>,
               public core::handler::property::Actionable<Toggle>
{
public:
    //!Constructs a toggle with a longhand and shorthand. They may be provided in any order. The passed setter function will be called with the truthy value when the handler is triggered. This version of the setter function can alter the parsing process.
    explicit Toggle(const std::string &first, const std::string &second, const std::function<bool(core::Context &, const bool)> &setter)
        : core::handler::property::Named(first, second)
        , setter_(setter)
        , negation_longhand_(default_negation_longhand_())
    {
    }
    //!Constructs a toggle with a longhand and shorthand. They may be provided in any order. The passed setter function will be called with the truthy value when the handler is triggered. This version of the setter function cannot alter the parsing process.
    explicit Toggle(const std::string &first, const std::string &second, const std::function<void(const bool)> &setter)
        : core::handler::property::Named(first, second)
        , setter_([setter](core::Context &, const bool value){ setter(value); return true; })
        , negation_longhand_(default_negation_longhand_())
    {
    }
    //!Constructs a toggle with a longhand and shorthand. They may be provided in any order. The passed setter function will be called with the truthy value when the handler is triggered. This version of the setter function can alter the parsing process. The passed getter is useful for formatters to display helpful output.
    explicit Toggle(const std::string &first, const std::string &second, const std::function<bool(core::Context &, const bool)> &setter, const std::function<bool()> &getter)
        : Toggle(first, second, setter)
    {
        getter_ = getter;
    }
    //!Constructs a toggle with a longhand and shorthand. They may be provided in any order. The passed variable will be toggled when the handler is triggered.
    explicit Toggle(const std::string &first, const std::string &second, bool &variable)
        : Toggle(first, second, [&variable](core::Context &, const bool value) { variable = value; return true; }, [&variable](){ return variable; })
    {
    }
    //!Constructs a toggle with a longhand or shorthand. The passed setter function will be called with the truthy value when the handler is triggered. This version of the setter function cal alter the parsing process.
    explicit Toggle(const std::string &name, const std::function<bool(core::Context &, const bool)> &setter)
        : core::handler::property::Named(name)
        , setter_(setter)
        , negation_longhand_(default_negation_longhand_())
    {}
    //!Constructs a toggle with a longhand or shorthand. The passed setter function will be called with the truthy value when the handler is triggered. This version of the setter function cannot alter the parsing process.
    explicit Toggle(const std::string &name, const std::function<void(const bool)> &setter)
        : core::handler::property::Named(name)
        , setter_([setter](core::Context &, const bool value){ setter(value); return true; })
        , negation_longhand_(default_negation_longhand_())
    {}
    //!Constructs a toggle with a longhand or shorthand. The passed setter function will be called with the truthy value when the handler is triggered. This version of the setter function cannot alter the parsing process. The passed getter is useful for formatters to display helpful output.
    explicit Toggle(const std::string &name, const std::function<bool(core::Context &, const bool)> &setter, const std::function<bool()> &getter)
        : Toggle(name, setter)
    {
        getter_ = getter;
    }
    //!Constructs a toggle with a longhand or shorthand. The passed variable will be toggled when the handler is triggered.
    explicit Toggle(const std::string &name, bool &variable)
        : Toggle(name, [&variable](core::Context &, const bool value) { variable = value; return true; }, [&variable](){ return variable; })
    {
    }

    virtual bool is_valid() const override
    {
        ARGO_MSS_BEGIN(bool);
        ARGO_MSS(core::handler::property::Named::is_valid_());
        ARGO_MSS(is_valid_);
        ARGO_MSS_END();
    }
    virtual std::string name() const override { return core::handler::property::Named::name_(); }
    virtual std::string type() const override { return "Toggle"; }
    //!Sets the negation longhand. For example, constructing a "--debug" toggle will automatically respond to a "--no-debug" argument. If the latter name is to be different, it can be set using this method.
    Toggle &with_negation(const std::string &longhand)
    {
        const auto is_longhand = core::name::is_longhand(longhand);
        is_valid_ = is_valid_ && is_longhand;
        if (is_longhand) negation_longhand_ = longhand;
        return *this;
    }
    //!Returns the negation longhand.
    std::string negation_longhand() const { return negation_longhand_; }
    //!Indicates if the default value is available.
    bool has_default_value() const { return !!getter_; }
    //!Returns the default value. This assumes `has_default_value()` returns true.
    bool default_value() const { return (*getter_)(); }
    virtual bool accept(core::handler::visitor::IConstVisitor &visitor) const override { return visitor.visit(*this); }
    virtual bool accept(core::handler::visitor::IMutatingVisitor &visitor) override { return visitor.visit(*this); }
    virtual core::handler::Ptr clone() const override { return core::make_unique<Toggle>(*this); }
    virtual bool parse(core::Context &context) override
    {
        ARGO_S(ARGO_DEBUG);
        ARGO_MSS_BEGIN(bool);
        ARGO_L("Toggle parsing...");

        //First process the name
        ARGO_MSS(!context.args().reached_end());
        const auto name = *context.args().current();
        ARGO_L("Consuming name '" << name << "'");
        ARGO_MSS(context.args().next());

        //Generation of negation
        const bool negating = name == negation_longhand_;
        ARGO_L(ARGO_C(negating));

        //Next, process a potential truthy value
        if (!context.args().reached_end())
        {
            const auto value = *context.args().current();
            ARGO_L("Considering consuming potential truthy value " << ARGO_C(value));
            const auto truthy = core::traits::conversion<bool>::run(value);
            if (!!truthy)
            {
                const bool value = negating ? !*truthy : *truthy;
                ARGO_MSS(setter_(context, value));

                //Consume value
                ARGO_L("Consuming " << ARGO_C(value));
                ARGO_MSS(context.args().next());
            }
            else
            {
                ARGO_L("Not consuming, leaving to toplevel parser");
                const bool value = !negating;
                ARGO_MSS(setter_(context, value));
            }
        } else ARGO_MSS(setter_(context, !negating));

        //Running all actions
        ARGO_L("Running actions: " << ARGO_C(actions_().size()));
        ARGO_MSS(core::handler::property::Actionable<Toggle>::run_(context));
        ARGO_L("All actions were run");

        ARGO_L("Finished toggle parsing");
        ARGO_MSS_END();
    }
    virtual bool is_satisfied(core::Context &context) const override
    {
        ARGO_MSS_BEGIN(bool);
        ARGO_MSS(core::handler::property::Required<Toggle>::is_satisfied_(context), context.error() << core::handler::name_of(*this, true) << " is required");
        ARGO_MSS_END();
    }

private:
    std::string default_negation_longhand_() const
    {
        if (!core::handler::property::Named::has_longhand()) return "";
        const auto lhs = core::handler::property::Named::longhand();
        std::ostringstream os;
        os << "--no-" << std::string(std::begin(lhs) + 2, std::end(lhs));
        return os.str();
    }

    std::function<bool(core::Context &, const bool)> setter_;
    core::optional<std::function<bool()>> getter_;
    bool is_valid_ = true;
    std::string negation_longhand_;
};

} // namespace handler

} }

#endif


#endif

namespace lw { namespace argo {

namespace core { namespace handler {

namespace visitor {

template <typename RefType>
class IsType : public IVisitor<true>
{
public:
    virtual bool visit(const ARGO_NS(handler::Flag) & handler) override { return visit_(handler); }
    virtual bool visit(const ARGO_NS(handler::Interface) & handler) override { return visit_(handler); }
    virtual bool visit(const ARGO_NS(handler::Option) & handler) override { return visit_(handler); }
    virtual bool visit(const ARGO_NS(handler::Positional) & handler) override { return visit_(handler); }
    virtual bool visit(const ARGO_NS(handler::Toggle) & handler) override { return visit_(handler); }
    virtual bool visit(const ARGO_NS(handler::group::Interface) & handler) override { return visit_(handler); }

private:
    template <typename Type>
    bool visit_(const Type &handler) const
    {
        return std::is_same<Type, RefType>::value;
    }
};

} // namespace visitor

template <typename RefType, typename Type>
bool is_type(const Type &type)
{
    visitor::IsType<RefType> visitor;
    return type.accept(visitor);
}

}} // namespace core::handler

} }

#endif

namespace lw { namespace argo {

namespace handler { namespace group {

class Interface : public core::handler::IHandler,
                  public core::handler::property::Help<Interface>,
                  public core::handler::property::Required<Interface>
{
public:
    using value_type = const core::handler::IHandler &;
    using reference_type = value_type;
    using iterator = core::Handlers::const_iterator;
    using const_iterator = iterator;

    //!Constructs the group with the given name and type description.
    explicit Interface(const std::string &name, const std::string &type)
        : name_(name)
        , type_(type)
        , handlers__(*this)
    {
    }
    //!Copy constructs the group.
    Interface(const Interface &other)
        : core::handler::IHandler(other)
        , core::handler::property::Help<Interface>(other)
        , core::handler::property::Required<Interface>(other)
        , name_(other.name_)
        , type_(other.type_)
        , handlers__(other.handlers__)
    {
        handlers__.set_parent(*this);
    }
    //!Copy assigns the group.
    Interface &operator=(const Interface &other)
    {
        if (this != &other)
        {
            core::handler::IHandler::operator=(other);
            core::handler::property::Help<Interface>::operator=(other);
            core::handler::property::Required<Interface>::operator=(other);
            name_ = other.name_;
            type_ = other.type_;
            handlers__ = other.handlers__;
            handlers__.set_parent(*this);
        }
        return *this;
    }

    virtual bool is_valid() const override { return true; }
    virtual std::string name() const override { return name_; }
    virtual bool accept(core::handler::visitor::IConstVisitor &visitor) const override { return visitor.visit(*this); }
    virtual bool accept(core::handler::visitor::IMutatingVisitor &visitor) override { return visitor.visit(*this); }
    bool accept_on_elements(core::handler::visitor::IConstVisitor &visitor) const
    {
        ARGO_MSS_BEGIN(bool);
        for (const auto &element : *this) ARGO_MSS(element.accept(visitor));
        ARGO_MSS_END();
    }
    bool accept_on_elements(core::handler::visitor::IMutatingVisitor &visitor)
    {
        ARGO_MSS_BEGIN(bool);
        for (auto &element : *this) ARGO_MSS(element.accept(visitor));
        ARGO_MSS_END();
    }
    virtual std::string type() const override { return type_; }
    bool add(const core::handler::IHandler &handler) { return handlers__.add(handler); }
    bool empty() const { return handlers__.empty(); }
    std::size_t size() const { return handlers__.size(); }

    const_iterator begin() const { return std::begin(handlers__); }
    const_iterator end() const { return std::end(handlers__); }

protected:
    const core::Handlers &handlers_() const { return handlers__; }

private:
    virtual bool parse(core::Context &context) override
    {
        assert(false);
        return false;
    }

    std::string name_;
    std::string type_;
    core::Handlers handlers__;
};

}} // namespace handler::group

} }

#endif


namespace lw { namespace argo {

namespace core { namespace handler { namespace search {

class ISearch : public visitor::IConstVisitor
{
public:
    ISearch(Context &context)
        : context_(context)
    {
    }

    IHandler *result() const { return const_cast<IHandler *>(presult_); }

    virtual bool visit(const ARGO_NS(handler::group::Interface) & group)
    {
        ARGO_MSS_BEGIN(bool);
        for (const auto &handler : group) ARGO_MSS(handler.accept(*this));
        ARGO_MSS_END();
    }

protected:
    Context &context_;
    const IHandler *presult_ = nullptr;

};

using Ptr = std::unique_ptr<ISearch>;

}}} // namespace core::handler::search

} }

#endif

namespace lw { namespace argo {

namespace core { namespace handler { namespace search {

class Custom : public ISearch
{
public:
    using ISearch::visit;

    explicit Custom(Context &context)
        : ISearch(context)
    {
    }

    virtual bool visit(const ARGO_NS(handler::Interface) & custom)
    {
        if (custom.recognizes_(context_))
        {
            presult_ = &custom;
            return false; //stop
        }
        return true; //continue
    }
    virtual bool visit(const ARGO_NS(handler::Flag) &) { return true; }
    virtual bool visit(const ARGO_NS(handler::Option) &) { return true; }
    virtual bool visit(const ARGO_NS(handler::Positional) &) { return true; }
    virtual bool visit(const ARGO_NS(handler::Toggle) &) { return true; }
};

}}} // namespace core::handler::search

} }

#endif
#ifndef HEADER_lw_argo_core_handler_search_FirstMatch_hpp_INCLUDE_GUARD
#define HEADER_lw_argo_core_handler_search_FirstMatch_hpp_INCLUDE_GUARD


namespace lw { namespace argo {

namespace core { namespace handler { namespace search {

class FirstMatch : public ISearch
{
public:
    using ISearch::visit;

    explicit FirstMatch(Context &context)
        : ISearch(context)
    {
    }

    template <typename Searcher, typename... Args>
    void add(Args &&... args)
    {
        searchers_.push_back(make_unique<Searcher>(context_, std::forward<Args>(args)...));
    }
    void clear()
    {
        searchers_.clear();
        presult_ = nullptr;
    }

    virtual bool visit(const ARGO_NS(handler::Flag) & handler) { return visit_(handler); }
    virtual bool visit(const ARGO_NS(handler::Interface) & handler) { return visit_(handler); }
    virtual bool visit(const ARGO_NS(handler::Option) & handler) { return visit_(handler); }
    virtual bool visit(const ARGO_NS(handler::Positional) & handler) { return visit_(handler); }
    virtual bool visit(const ARGO_NS(handler::Toggle) & handler) { return visit_(handler); }

private:
    bool visit_(const core::handler::IHandler &handler)
    {
        for (const auto &psearcher : searchers_)
        {
            assert(!!psearcher);
            const bool continue_search = handler.accept(*psearcher);
            presult_ = psearcher->result();
            if (!continue_search) return false; //stop
        }
        return true; //continue
    }

    std::vector<Ptr> searchers_;
};

}}} // namespace core::handler::search

} }

#endif
#ifndef HEADER_lw_argo_core_handler_search_Option_hpp_INCLUDE_GUARD
#define HEADER_lw_argo_core_handler_search_Option_hpp_INCLUDE_GUARD


namespace lw { namespace argo {

namespace core { namespace handler { namespace search {

class Option : public ISearch
{
public:
    using ISearch::visit;

    explicit Option(Context &context)
        : ISearch(context)
    {
    }

    virtual bool visit(const ARGO_NS(handler::Flag) & flag) { return handle_(flag); }
    virtual bool visit(const ARGO_NS(handler::Interface) &) { return true; }
    virtual bool visit(const ARGO_NS(handler::Option) & option) { return handle_(option); }
    virtual bool visit(const ARGO_NS(handler::Positional) & positional) { return true; }
    virtual bool visit(const ARGO_NS(handler::Toggle) & toggle)
    {
        ARGO_S(ARGO_DEBUG);
        ARGO_MSS_BEGIN(bool);
        ARGO_MSS(handle_(toggle));
        assert(context_.args().current() != context_.args().end());
        auto name = *context_.args().current();
        if (context_.config().parser.dash_names) dash(name);
        const auto negation_longhand = toggle.negation_longhand();
        ARGO_L("Checking toggle negation: " << ARGO_C(negation_longhand));
        if (negation_longhand == name)
        {
            ARGO_L("Found perfect toggle negation handler");
            presult_ = &toggle;
            return false;
        }
        return handle_(toggle);
        ARGO_MSS_END();
    }

private:
    template <typename Handler>
    bool handle_(const Handler &handler)
    {
        ARGO_S(ARGO_DEBUG);
        assert(context_.args().current() != context_.args().end());
        auto name = *context_.args().current();
        if (context_.config().parser.dash_names) dash(name);
        const auto &named = static_cast<const property::Named &>(handler);
        if (named.has_longhand() && name == named.longhand())
        {
            ARGO_L("Found perfect longhand match, stopping here");
            presult_ = &handler;
            return false; //stop
        }
        else if (named.has_shorthand() && name == named.shorthand())
        {
            ARGO_L("Found perfect shorthand match, stopping here");
            presult_ = &handler;
            return false; //stop
        }
        else if (named.has_longhand() && !named.has_shorthand() && name == name::guess_shorthand(named.longhand()))
        {
            ARGO_L("Found shorthand match after guessing, continuing in case there's a better match");
            presult_ = &handler;
        }
        return true; //continue
    }
};

}}} // namespace core::handler::search

} }

#endif
#ifndef HEADER_lw_argo_core_handler_search_Positional_hpp_INCLUDE_GUARD
#define HEADER_lw_argo_core_handler_search_Positional_hpp_INCLUDE_GUARD


namespace lw { namespace argo {

namespace core { namespace handler { namespace search {

class Positional : public ISearch
{
public:
    using ISearch::visit;

    explicit Positional(Context &context)
        : ISearch(context)
    {
    }

    virtual bool visit(const ARGO_NS(handler::Interface) &) { return true; }
    virtual bool visit(const ARGO_NS(handler::Flag) &) { return true; }
    virtual bool visit(const ARGO_NS(handler::Option) &) { return true; }
    virtual bool visit(const ARGO_NS(handler::Positional) & positional)
    {
        assert(context_.args().current() != context_.args().end());
        const auto name = *context_.args().current();
        if (name::is_option(name)) return true;
        if (!context_.info(positional).nr_parses)
        {
            presult_ = &positional;
            return false; //stop
        }
        return true; //continue
    }
    virtual bool visit(const ARGO_NS(handler::Toggle) &) { return true; }
};

}}} // namespace core::handler::search

} }

#endif
#ifndef HEADER_lw_argo_core_handler_visitor_ApplyFunction_hpp_INCLUDE_GUARD
#define HEADER_lw_argo_core_handler_visitor_ApplyFunction_hpp_INCLUDE_GUARD


namespace lw { namespace argo {

namespace core { namespace handler { namespace visitor {

template <bool is_const>
class ApplyFunction : public IVisitor<is_const>
{
    template <typename Type>
    using type_of = visitor::type_of<is_const, Type>;

public:
    using function_type = std::function<bool(type_of<IHandler>)>;

    explicit ApplyFunction(const function_type &fnc)
        : fnc_(fnc)
    {
    }

    virtual bool visit(type_of<ARGO_NS(handler::Flag)> handler) override { return fnc_(handler); }
    virtual bool visit(type_of<ARGO_NS(handler::Interface)> handler) override { return fnc_(handler); }
    virtual bool visit(type_of<ARGO_NS(handler::Option)> handler) override { return fnc_(handler); }
    virtual bool visit(type_of<ARGO_NS(handler::Positional)> handler) override { return fnc_(handler); }
    virtual bool visit(type_of<ARGO_NS(handler::Toggle)> handler) override { return fnc_(handler); }
    virtual bool visit(type_of<ARGO_NS(handler::group::Interface)> handler) override { return fnc_(handler); }

private:
    function_type fnc_;
};

using ApplyConstFunction = ApplyFunction<true>;
using ApplyMutatingFunction = ApplyFunction<false>;

}}} // namespace core::handler::visitor

} }

#endif
#ifndef HEADER_lw_argo_core_raii_hpp_INCLUDE_GUARD
#define HEADER_lw_argo_core_raii_hpp_INCLUDE_GUARD


namespace lw { namespace argo {

namespace core {

template <typename ConstructionCB, typename DestructionCB>
class raii
{
public:
    explicit raii(const ConstructionCB on_construction, const DestructionCB on_destruction)
        : on_destruction_(on_destruction)
    {
        on_construction();
    }
    raii(const raii &) = delete;
    raii &operator=(const raii &) = delete;
    raii(raii &&other)
        : on_destruction_(other.on_destruction_)
    {
        other.run_destruction_ = false;
    }
    raii &operator=(raii &&) = delete;
    ~raii()
    {
        if (run_destruction_) on_destruction_();
    }

private:
    bool run_destruction_ = true;
    DestructionCB on_destruction_;
};

template <typename ConstructionCB, typename DestructionCB>
raii<ConstructionCB, DestructionCB> make_raii(const ConstructionCB &on_construction, const DestructionCB &on_destruction)
{
    return raii<ConstructionCB, DestructionCB>(on_construction, on_destruction);
}
template <typename DestructionCB>
raii<std::function<void()>, DestructionCB> make_raii(const DestructionCB &on_destruction)
{
    return raii<std::function<void()>, DestructionCB>([]() {}, on_destruction);
}

} // namespace core

} }

#endif
#ifndef HEADER_lw_argo_formatter_Default_hpp_INCLUDE_GUARD
#define HEADER_lw_argo_formatter_Default_hpp_INCLUDE_GUARD

#ifndef HEADER_lw_argo_core_OnlyOnFirstTime_hpp_INCLUDE_GUARD
#define HEADER_lw_argo_core_OnlyOnFirstTime_hpp_INCLUDE_GUARD


namespace lw { namespace argo {

namespace core {

class OnlyOnFirstTime
{
public:
    template <typename Functor>
    void operator()(Functor functor) const
    {
        if (first_time_)
        {
            functor();
            first_time_ = false;
        }
    }

private:
    mutable bool first_time_ = true;
};

} // namespace core

} }

#endif

#ifndef HEADER_lw_argo_core_handler_visitor_Collector_hpp_INCLUDE_GUARD
#define HEADER_lw_argo_core_handler_visitor_Collector_hpp_INCLUDE_GUARD


namespace lw { namespace argo {

namespace core { namespace handler { namespace visitor {

namespace collector {

template <typename Handler, bool is_const>
class NonRecursive : public IVisitor<is_const>
{
    template <typename Type>
    using type_of = visitor::type_of<is_const, Type>;
    using pointer_type = typename std::conditional<is_const, const handler::IHandler *, handler::IHandler *>::type;

public:
    using handler_type = type_of<Handler>;
    using value_type = pointer_type;
    using data_type = std::vector<value_type>;
    using function_type = std::function<bool(handler_type)>;

    data_type result() const { return result_; }
    bool foreach (const function_type &fnc, const function_type &predicate = AlwaysTrue{})
    {
        ARGO_MSS_BEGIN(bool);
        for (const auto phandler : result())
        {
            assert(!!phandler);
            auto &handler = static_cast<handler_type>(*phandler);
            if (!predicate(handler)) continue;
            ARGO_MSS(fnc(handler));
        }
        ARGO_MSS_END();
    }

private:
    virtual bool visit(type_of<ARGO_NS(handler::Flag)> handler) override { return visit_(handler); }
    virtual bool visit(type_of<ARGO_NS(handler::Interface)> handler) override { return visit_(handler); }
    virtual bool visit(type_of<ARGO_NS(handler::Option)> handler) override { return visit_(handler); }
    virtual bool visit(type_of<ARGO_NS(handler::Positional)> handler) override { return visit_(handler); }
    virtual bool visit(type_of<ARGO_NS(handler::Toggle)> handler) override { return visit_(handler); }
    virtual bool visit(type_of<ARGO_NS(handler::group::Interface)> group) override { return visit_(group); }

    template <typename Type>
    bool visit_(const Type &handler)
    {
        if (std::is_same<Handler, Type>::value) collect_(&handler);
        return true;
    }
    template <typename Type>
    bool visit_(Type &handler)
    {
        if (std::is_same<Handler, Type>::value) collect_(&handler);
        return true;
    }
    void collect_(pointer_type phandler) { result_.push_back(phandler); }

    data_type result_;
};

template <typename Handler>
using ConstNonRecursive = NonRecursive<Handler, true>;
template <typename Handler>
using NonConstNonRecursive = NonRecursive<Handler, false>;

template <typename Handler, bool is_const>
class Recursive : public IVisitor<is_const>
{
    template <typename Type>
    using type_of = visitor::type_of<is_const, Type>;
    using pointer_type = typename std::conditional<is_const, const handler::IHandler *, handler::IHandler *>::type;

public:
    using handler_type = type_of<Handler>;
    struct value_type
    {
        explicit value_type(pointer_type phandler, unsigned depth)
            : phandler(phandler)
            , depth(depth)
        {
        }

        pointer_type phandler = nullptr;
        unsigned depth = 0u;
    };
    using data_type = std::vector<value_type>;
    using function_type = std::function<bool(handler_type, unsigned)>;

    data_type result() const { return result_; }
    bool foreach (const function_type &fnc, const function_type &predicate = AlwaysTrue{})
    {
        ARGO_MSS_BEGIN(bool);
        for (const auto item : result())
        {
            assert(!!item.phandler);
            auto &handler = static_cast<handler_type>(*item.phandler);
            if (!predicate(handler, item.depth)) continue;
            ARGO_MSS(fnc(handler, item.depth));
        }
        ARGO_MSS_END();
    }

private:
    virtual bool visit(type_of<ARGO_NS(handler::Flag)> handler) override { return visit_(handler); }
    virtual bool visit(type_of<ARGO_NS(handler::Interface)> handler) override { return visit_(handler); }
    virtual bool visit(type_of<ARGO_NS(handler::Option)> handler) override { return visit_(handler); }
    virtual bool visit(type_of<ARGO_NS(handler::Positional)> handler) override { return visit_(handler); }
    virtual bool visit(type_of<ARGO_NS(handler::Toggle)> handler) override { return visit_(handler); }
    virtual bool visit(type_of<ARGO_NS(handler::group::Interface)> group) override
    {
        ARGO_MSS_BEGIN(bool);
        ARGO_MSS(visit_(group));
        ++depth_;
        ARGO_MSS(group.accept_on_elements(*this));
        --depth_;
        ARGO_MSS_END();
    }
    template <typename Type>
    bool visit_(const Type &handler)
    {
        if (std::is_same<Handler, Type>::value) collect_(&handler);
        return true;
    }
    template <typename Type>
    bool visit_(Type &handler)
    {
        if (std::is_same<Handler, Type>::value) collect_(&handler);
        return true;
    }
    void collect_(pointer_type phandler) { result_.emplace_back(phandler, depth_); }

    data_type result_;
    unsigned depth_ = 0u;
};

template <typename Handler>
using ConstRecursive = Recursive<Handler, true>;
template <typename Handler>
using NonConstRecursive = Recursive<Handler, false>;

} // namespace collector

template <typename Collector, typename Handler>
bool foreach (const Handler &handler, typename Collector::function_type fnc, typename Collector::function_type predicate = AlwaysTrue{})
{
    ARGO_MSS_BEGIN(bool);
    Collector collector;
    ARGO_ASSERT_RELEASE(handler.accept_on_elements(collector));
    ARGO_MSS(collector.foreach (fnc, predicate));
    ARGO_MSS_END();
}

}}} // namespace core::handler::visitor

} }

#endif
#ifndef HEADER_lw_argo_formatter_Interface_hpp_INCLUDE_GUARD
#define HEADER_lw_argo_formatter_Interface_hpp_INCLUDE_GUARD

#ifndef HEADER_lw_argo_core_string_Text_hpp_INCLUDE_GUARD
#define HEADER_lw_argo_core_string_Text_hpp_INCLUDE_GUARD


#define ARGO_DETAILS_STRING_TEXT_DEBUG 0

namespace lw { namespace argo {

namespace core { namespace string {

class Text
{
    struct Endline
    {
    };
    struct Indent
    {
    };
    struct Outdent
    {
    };
    struct Boolalpha
    {
    };

public:
    static const Boolalpha &boolalpha()
    {
        static const Boolalpha boolalpha_;
        return boolalpha_;
    }
    static const Endline &endl()
    {
        static const Endline endl_;
        return endl_;
    }
    static const Indent &indent()
    {
        static const Indent indent_;
        return indent_;
    }
    static const Outdent &outdent()
    {
        static const Outdent outdent_;
        return outdent_;
    }
    static const std::string &tab()
    {
        static const std::string tab_{ "    " };
        return tab_;
    }

    struct Color
    {
        struct Code
        {
            std::vector<unsigned> sequence;
        };

        static constexpr unsigned BlackForeground = 30;
        static constexpr unsigned BlackBackground = 40;
        static constexpr unsigned RedForeground = 31;
        static constexpr unsigned RedBackground = 41;
        static constexpr unsigned GreenForeground = 32;
        static constexpr unsigned GreenBackground = 42;
        static constexpr unsigned YellowForeground = 33;
        static constexpr unsigned YellowBackground = 43;
        static constexpr unsigned BlueForeground = 34;
        static constexpr unsigned BlueBackground = 44;
        static constexpr unsigned MagentaForeground = 35;
        static constexpr unsigned MagentaBackground = 45;
        static constexpr unsigned CyanForeground = 36;
        static constexpr unsigned CyanBackground = 46;
        static constexpr unsigned WhiteForeground = 37;
        static constexpr unsigned WhiteBackground = 47;
        static constexpr unsigned Reset = 0;
        static constexpr unsigned BoldBrightOn = 1;
        static constexpr unsigned Underline = 4;
        static constexpr unsigned Inverse = 7;
        static constexpr unsigned BoldBrightOff = 21;
        static constexpr unsigned UnderlineOff = 24;
        static constexpr unsigned InverseOff = 27;
    };
    static const Color::Code color_set(const std::vector<unsigned> &colors) { return Color::Code{ colors }; }
    static const Color::Code color_reset() { return Color::Code{ { Color::Reset } }; }

    Text &operator<<(std::string str);
    Text &operator<<(const Boolalpha &tag);
    Text &operator<<(const Color::Code &tag);
    Text &operator<<(const Endline &tag);
    Text &operator<<(const Indent &tag);
    Text &operator<<(const Outdent &tag);
    Text &operator<<(const Text &txt);
    template <typename T>
    Text &operator<<(const T &element);

    std::string str() const;
    void clear();

private:
    std::ostringstream ss_;
    std::size_t indent_level_ = 0;
    bool is_newline_ = true;

    std::string indent_(bool update_is_newline = true);
};

inline Text &Text::operator<<(std::string str)
{
    const std::string from = "\n";
    const std::string to = from + indent_(false);
    replace_all(str, from, to);
#if ARGO_DETAILS_STRING_TEXT_DEBUG
    std::ostringstream os;
    os << indent_() << str;
    std::cout << os.str();
    ss_ << os.str();
#else
    ss_ << indent_() << str;
#endif
    return *this;
}
inline Text &Text::operator<<(const Text::Endline &tag)
{
    ss_ << "\n";
#if ARGO_DETAILS_STRING_TEXT_DEBUG
    std::cout << std::endl;
#endif
    is_newline_ = true;
    return *this;
}
inline Text &Text::operator<<(const Text::Indent &tag)
{
    ++indent_level_;
    return *this;
}
inline Text &Text::operator<<(const Text::Outdent &tag)
{
    if (indent_level_ > 0) --indent_level_;
    return *this;
}
inline Text &Text::operator<<(const Text::Boolalpha &tag)
{
    ss_ << std::boolalpha;
    return *this;
}
inline Text &Text::operator<<(const Text::Color::Code &tag)
{
#if ARGO_TOOLSET_PLATFORM_POSIX
    ss_ << "\033[";
    ss_ << join(";", ARGO_RANGE(tag.sequence));
    ss_ << "m";
#endif
    return *this;
}
inline Text &Text::operator<<(const Text &txt) { return *this << txt.str(); }

inline std::string Text::str() const { return ss_.str(); }

inline void Text::clear()
{
    ss_.str("");
    ss_.clear();
    indent_level_ = 0;
}

inline std::string Text::indent_(bool update_is_newline)
{
    std::string result;
    if (is_newline_)
    {
        for (auto n = 0u; n < indent_level_; ++n) result += tab();
        if (update_is_newline) is_newline_ = false;
    }
    return result;
}

template <typename T>
Text &Text::operator<<(const T &element)
{
#if ARGO_DETAILS_STRING_TEXT_DEBUG
    std::ostringstream os;
    os << indent_() << element;
    ss_ << os.str();
    std::cout << os.str();
#else
    ss_ << indent_() << element;
#endif
    return *this;
}

}} // namespace core::string

} }

#endif
#ifndef HEADER_lw_argo_core_system_utility_hpp_INCLUDE_GUARD
#define HEADER_lw_argo_core_system_utility_hpp_INCLUDE_GUARD

#if ARGO_TOOLSET_PLATFORM_POSIX
#include <stdio.h>
#include <sys/ioctl.h>
#include <unistd.h>
#elif ARGO_TOOLSET_PLATFORM_WINDOWS
#include <windows.h>
#endif

namespace lw { namespace argo {

namespace core { namespace system { namespace terminal {

inline bool is_tty()
{
#if ARGO_TOOLSET_PLATFORM_POSIX
    return isatty(fileno(stdout));
#else
    return false;
#endif
}
inline bool is_pty() { return !is_tty(); }

inline unsigned int width()
{
    const auto DEFAULT = 80u;
    if (is_pty()) return DEFAULT;
#if ARGO_TOOLSET_PLATFORM_POSIX
    struct winsize w;
    if (0 != ioctl(STDOUT_FILENO, TIOCGWINSZ, &w)) return DEFAULT;
    return w.ws_col == 0 ? 80 : w.ws_col;
#else
    return DEFAULT;
#endif
}
inline unsigned int height()
{
    const auto DEFAULT = 32;
    if (is_pty()) return DEFAULT;
#if ARGO_TOOLSET_PLATFORM_POSIX
    struct winsize w;
    if (0 != ioctl(STDOUT_FILENO, TIOCGWINSZ, &w)) return DEFAULT;
    return w.ws_row;
#else
    return DEFAULT;
#endif
}

}}} // namespace core::system::terminal

} }

#endif

namespace lw { namespace argo {

class Arguments;

namespace formatter {

class Interface;

using Ptr = std::unique_ptr<Interface>;

//!Interface serving as a base class for concrete formatters.
class Interface
{
public:
    virtual ~Interface() = default;

    //!Sets the output width. Note that a width of 0 corresponds to full width, i.e. the entire width of the terminal console.
    virtual void set_width(unsigned width) { width_ = width; }
    //!Returns the output width.
    virtual unsigned width() const { return 0 == width_ ? core::system::terminal::width() : width_; }
    //!Returns the formatted error message.
    virtual std::string format_error(const core::Context &context, const std::string &message) const = 0;
    //!Returns the formatted usage section.
    virtual std::string format_usage(const core::Context &context) const = 0;
    //!Returns the formatted help section.
    virtual std::string format_help(const core::Context &context) const = 0;
    //!Returns the formatted version section.
    virtual std::string format_version(const core::Context &context) const = 0;

protected:
    static constexpr auto DEFAULT_WIDTH = 80u;

    using Txt = core::string::Text;

    unsigned width_ = 0; //! 0 means dynamic width

private:
    friend class ARGO_NS(Arguments);

    virtual Ptr clone() const = 0;
};

} // namespace formatter

} }

#endif

#include <iomanip>
#include <stack>

namespace lw { namespace argo {

namespace formatter {

//!Default formatter.
class Default : public Interface
{
public:
    Default() = default;
    Default(const Default &other)
        : indentations_(other.indentations_)
    {
        out_ << other.out_.str();
    }

    virtual unsigned width() const override
    {
        const auto w = Interface::width() * 0.66;
        return w <= DEFAULT_WIDTH ? DEFAULT_WIDTH : static_cast<unsigned>(w);
    }
    virtual std::string format_error(const core::Context &context, const std::string &message) const override { return message; }
    virtual std::string format_usage(const core::Context &context) const override
    {
        out_.clear();
        section_usage_(context);
        return out_.str();
    }
    virtual std::string format_help(const core::Context &context) const override
    {
        out_.clear();
        section_heading_(context);
        section_usage_(context);
        section_description_(context);
        section_extra_(context);
        section_copyright_(context);
        out_ << Txt::endl();
        return out_.str();
    }
    virtual std::string format_version(const core::Context &context) const override
    {
        out_.clear();
        const auto &program = context.config().program;
        if (!program.name.extended.empty()) out_ << program.name.extended << Txt::endl();
        out_ << version_info_(program.version);
        return out_.str();
    }

protected:
    virtual Ptr clone() const override { return core::make_unique<Default>(); }

    virtual void section_heading_(const core::Context &context) const
    {
        const auto &program = context.config().program;
        {
            std::string title = program.name.extended.empty() ? program.name.brief : program.name.extended;
            if (title.empty()) return;
            const auto nr = title.size() < width() ? ((width() - title.size()) / 2) : 0;
            if (nr)
            {
                std::string shift(nr, ' ');
                out_ << shift << title << Txt::endl();
            }
        }
        if (!program.name.brief.empty())
        {
            out_ << format_title_("NAME");
            push_indent_(2);
            out_ << format_content_(program.name.brief);
            if (!program.description.brief.empty())
            {
                out_ << format_content_(program.description.brief);
            }
            pop_indent_();
        }
        if (program.version.is_set())
        {
            out_ << format_title_("VERSION");
            push_indent_(2);
            out_ << format_content_(version_info_(program.version));
            pop_indent_();
        }
    }
    virtual void section_usage_(const core::Context &context) const
    {
        const auto &program = context.config().program;
        out_ << format_title_("USAGE");
        push_indent_(2);
        if (!program.description.usage.empty())
            out_ << format_content_(program.description.usage);
        else
        {
            const auto &text = generate_usage_(context);
            out_ << format_content_(text);
        }
        out_ << Txt::endl();
        //Legend
        {
            std::ostringstream legend;
            Txt note;
            {
                note << Txt::color_set({ Txt::Color::Underline }) << "Note:" << Txt::color_reset();
            }
            legend << note.str() << std::endl;
            legend << "Groups are indicated with {}" << std::endl;
            legend << "Optional groups and arguments are indicated with ()" << std::endl;
            legend << "Values are indicated with <x> where x is a cardinality marker" << std::endl;
            legend << "Positional arguments are labeled in the corresponding cardinality marker";
            out_ << format_content_(legend.str());
        }
        pop_indent_();
    }
    virtual void section_description_(const core::Context &context) const
    {
        using namespace core::handler::visitor::collector;

        out_ << format_title_("DESCRIPTION");
        const auto &program = context.config().program;
        push_indent_(2);
        if (!program.description.extended.empty())
            out_ << format_content_(program.description.extended) << Txt::endl();
        else if (!program.description.brief.empty())
            out_ << format_content_(program.description.brief) << Txt::endl();

        //Groups
        core::OnlyOnFirstTime line_groups;
        auto cb_group = [&](const handler::group::Interface &group, const unsigned depth) {
            line_groups([&]() { out_ << format_content_("The groups are as follows:") << Txt::endl(); });
            std::vector<std::string> properties = { group.type() };
            if (group.is_required()) properties.push_back("Required");
            out_ << format_item_(group.name(), properties);
            if (!group.help().empty()) out_ << format_content_(group.help());
            for (const auto &handler : group)
            {
                push_indent_(2);
                out_ << format_content_(handler.name());
                pop_indent_();
            }
            return true;
        };
        //Flags
        core::OnlyOnFirstTime line_options;
        auto cb_line_named = [&]() { line_options([&]() { out_ << Txt::endl() << format_content_("The named arguments are as follows:") << Txt::endl(); }); };
        auto cb_flag = [&](const handler::Flag &flag, const unsigned depth) {
            cb_line_named();
            std::vector<std::string> properties;
            properties.push_back(flag.type());
            if (flag.is_required()) properties.push_back("Required");
            out_ << format_item_(flag.name(), properties);
            push_indent_(2);
            if (!flag.help().empty()) out_ << format_content_(flag.help());
            pop_indent_();
            return true;
        };
        //Options
        auto cb_option = [&](const handler::Option &option, const unsigned depth) {
            cb_line_named();
            std::vector<std::string> properties;
            properties.push_back(option.type());
            if (option.is_required()) properties.push_back("Required");
            properties.push_back(option.nargs_type());
            out_ << format_item_(option.name(), properties);
            push_indent_(2);
            if (!option.help().empty()) out_ << format_content_(option.help());
            pop_indent_();
            return true;
        };
        //Toggles
        auto cb_toggle_negation = [&](const handler::Toggle &toggle, const unsigned depth) {
            if (toggle.negation_longhand().empty()) return true;
            cb_line_named();
            std::vector<std::string> properties;
            properties.push_back(toggle.type());
            out_ << format_item_(toggle.negation_longhand(), properties);
            push_indent_(2);
            {
                Txt help;
                help << "Negation of " << core::handler::name_of(toggle) << ".";
                out_ << format_content_(help.str());
            }
            pop_indent_();
            return true;
        };
        auto cb_toggle = [&](const handler::Toggle &toggle, const unsigned depth) {
            cb_line_named();
            std::vector<std::string> properties;
            properties.push_back(toggle.type());
            if (toggle.is_required()) properties.push_back("Required");
            if (toggle.has_default_value())
            {
                std::ostringstream os;
                os << "Default: " << std::boolalpha << toggle.default_value();
                properties.push_back(os.str());
            }
            out_ << format_item_(toggle.name(), properties);
            push_indent_(2);
            if (!toggle.help().empty()) out_ << format_content_(toggle.help());
            pop_indent_();
            cb_toggle_negation(toggle, depth);
            return true;
        };
        //Custom handlers
        auto cb_custom = [&](const handler::Interface &custom, const unsigned depth) {
            cb_line_named();
            std::vector<std::string> properties;
            properties.push_back(custom.type());
            out_ << format_item_(custom.name(), properties);
            push_indent_(2);
            if (!custom.help().empty()) out_ << format_content_(custom.help());
            pop_indent_();
            return true;
        };
        //Positional
        core::OnlyOnFirstTime line_positionals;
        auto cb_positional = [&](const handler::Positional &positional, const unsigned depth) {
            line_positionals([&]() { out_ << Txt::endl() << format_content_("The positional arguments are as follows:") << Txt::endl(); });
            std::vector<std::string> properties;
            properties.push_back(positional.type());
            if (positional.is_required()) properties.push_back("Required");
            properties.push_back(positional.nargs_type());
            out_ << format_item_(positional.name(), properties);
            push_indent_(2);
            if (!positional.help().empty()) out_ << format_content_(positional.help());
            pop_indent_();
            return true;
        };
        core::handler::visitor::foreach<ConstRecursive<handler::group::Interface>>(context.handlers(), cb_group);
        core::handler::visitor::foreach<ConstRecursive<handler::Flag>>(context.handlers(), cb_flag);
        core::handler::visitor::foreach<ConstRecursive<handler::Toggle>>(context.handlers(), cb_toggle);
        core::handler::visitor::foreach<ConstRecursive<handler::Option>>(context.handlers(), cb_option);
        core::handler::visitor::foreach<ConstRecursive<handler::Interface>>(context.handlers(), cb_custom);
        core::handler::visitor::foreach<ConstRecursive<handler::Positional>>(context.handlers(), cb_positional);
        pop_indent_();
    }
    virtual void section_extra_(const core::Context &context) const {}
    virtual void section_copyright_(const core::Context &context) const
    {
        const auto &program = context.config().program;
        if (program.copyright.empty()) return;

        out_ << format_title_("COPYRIGHT");
        push_indent_(2);
        auto copyright = program.copyright;
        core::string::trim(copyright);
        out_ << format_content_(copyright);
        pop_indent_();
    }

    std::string format_title_(const std::string &title) const
    {
        Txt txt;
        txt << Txt::endl() << Txt::color_set({ Txt::Color::BoldBrightOn }) << title << Txt::color_reset() << Txt::endl() << Txt::endl();
        return txt.str();
    }
    std::string format_content_(const std::string &text) const
    {
        Txt txt;
        txt << core::string::align(text, width(), indent_());
        if (!core::string::ends_with(text, "\n")) txt << Txt::endl();
        return txt.str();
    }
    std::string format_item_(const std::string &name, const std::vector<std::string> &properties) const
    {
        std::ostringstream os;
        std::string item_left;
        {
            Txt txt;
            txt << std::string(indent_(), ' ');
            txt << Txt::color_set({ Txt::Color::BoldBrightOn }) << name << Txt::color_reset();
            item_left = txt.str();
        }
        std::string item_right;
        {
            std::ostringstream os;
            for (const auto &property : properties) os << core::string::surround(property, "[");
            item_right = os.str();
        }
        os << std::left << item_left << std::right << std::setfill(' ') << std::setw(width() - name.size() - indent_()) << item_right << std::endl;
        return os.str();
    }

    template <typename Version>
    std::string version_info_(const Version &version) const
    {
        std::ostringstream info;
        std::vector<unsigned> numbers = { version.major, version.minor, version.patch };
        info << "Release:  " + core::string::join(".", ARGO_RANGE(numbers));
        const auto githash = std::string(version.githash);
        if (!githash.empty())
        {
            info << std::endl;
            info << "Revision: " + githash;
        }
        return info.str();
    }
    std::string generate_usage_(const core::Context &context) const
    {
        using namespace core::handler::visitor::collector;

        const auto &program = context.config().program;

        std::vector<std::string> items;
        items.push_back(program.name.brief.empty() ? "<app>" : program.name.brief);

        //Groups
        auto cb_group = [&](const handler::group::Interface &group) {
            std::string handler = core::string::surround(group.name(), "{");
            handler = group.is_required() ? handler : core::string::surround(handler, "(");
            items.push_back(handler);
            return true;
        };
        //Flags
        auto cb_flag = [&](const handler::Flag &flag) {
            std::string handler = flag.is_required() ? flag.name() : core::string::surround(flag.name(), "(");
            items.push_back(handler);
            return true;
        };
        //Options
        auto cb_option = [&](const handler::Option &option) {
            std::string handler = option.is_required() ? option.name() : core::string::surround(option.name(), "(");
            items.push_back(handler);
            std::string value = core::string::surround(option.nargs_symbol(), "<");
            items.push_back(value);
            return true;
        };
        //Toggles
        auto cb_toggle = [&](const handler::Toggle &toggle) {
            std::string handler = toggle.is_required() ? toggle.name() : core::string::surround(toggle.name(), "(");
            items.push_back(handler);
            return true;
        };
        //Positional
        auto cb_positional = [&](const handler::Positional &positional) {
            std::string handler = positional.name() + ":" + positional.nargs_symbol();
            handler = core::string::surround(handler, "<");
            handler = positional.is_required() ? handler : core::string::surround(handler, "(");
            items.push_back(handler);
            return true;
        };
        core::handler::visitor::foreach<ConstNonRecursive<handler::Flag>>(context.handlers(), cb_flag);
        core::handler::visitor::foreach<ConstNonRecursive<handler::Toggle>>(context.handlers(), cb_toggle);
        core::handler::visitor::foreach<ConstNonRecursive<handler::Option>>(context.handlers(), cb_option);
        core::handler::visitor::foreach<ConstNonRecursive<handler::group::Interface>>(context.handlers(), cb_group);
        core::handler::visitor::foreach<ConstNonRecursive<handler::Positional>>(context.handlers(), cb_positional);
        return core::string::join(" ", ARGO_RANGE(items));
    }

    void push_indent_(unsigned indentation) const { indentations_.push(indent_() + indentation); }
    void pop_indent_() const
    {
        if (!indentations_.empty()) indentations_.pop();
    }
    unsigned indent_() const { return indentations_.empty() ? 0 : indentations_.top(); }

    mutable std::stack<unsigned> indentations_;
    mutable Txt out_;
};

} // namespace formatter

} }

#endif


namespace lw { namespace argo {

//!Arguments is the toplevel parser of arguments. It is configured step-by-step by iteratively adding handlers to it.
class Arguments
{
public:
    //!Default constructor.
    Arguments()
    {
        process_configuration_();
    }
    //!Constructs the parser with the given configuration.
    explicit Arguments(const Configuration &config)
        : config_(config)
    {
        process_configuration_();
    }
    //!Copy constructs the parser.
    Arguments(const Arguments &other)
        : config_(other.config_)
        , handlers_(other.handlers_)
        , pformatter_(other.formatter().clone())
    {
    }
    //!Move constructs the parser.
    Arguments(Arguments &&other)
        : config_(std::move(other.config_))
        , handlers_(other.handlers_)
        , pformatter_(other.formatter().clone())
    {
    }
    //!Copy assigns the parser.
    Arguments &operator=(const Arguments &other)
    {
        if (this != &other)
        {
            config_ = other.config_;
            handlers_ = other.handlers_;
            pformatter_ = other.formatter().clone();
            pcontext_ = nullptr;
            phase_ = core::Phase::OptionsFirstPass;
        }
        return *this;
    }
    //!Move assigns the parser.
    Arguments &operator=(Arguments &&other)
    {
        if (this != &other)
        {
            config_ = std::move(other.config_);
            handlers_ = std::move(other.handlers_);
            pformatter_ = std::move(other.pformatter_);
            pcontext_ = nullptr;
            phase_ = core::Phase::OptionsFirstPass;
        }
        return *this;
    }

    //!Returns the configuration.
    const Configuration &configuration() const { return config_; }
    //!Adds the given handler to the parser. Returns a boolean indicating success.
    bool add(const core::handler::IHandler &handler)
    {
        ARGO_S(ARGO_DEBUG);
        ARGO_MSS_BEGIN(bool);
        ARGO_L("Adding handler " << handler);
        ARGO_MSS(handler.is_valid(), context_().error() << "Programmer failure: " << core::handler::name_of(handler) << " is wrongly configured";);
        ARGO_MSS(handlers_.add(handler), context_().error() << "Programmer failure: attempted to add multiple handlers for " << core::handler::name_of(handler););
        ARGO_MSS_END();
    }
    //!Merges the given parser by adding all its handlers. If a handler conflicts, the handler of the other parser is used when `replace = true` or skipped when `replace = false`.
    bool merge(const Arguments &other, bool replace = true)
    {
        ARGO_S(ARGO_DEBUG);
        ARGO_MSS_BEGIN(bool);
        ARGO_L("Merging parsers...");
        for (const auto &handler : other.handlers_)
        {
            if (!handlers_.has(handler))
            {
                ARGO_L("Adding " << handler.name());
                ARGO_MSS(add(handler));
            }
            else if (replace)
            {
                ARGO_L("Replacing " << handler.name());
                ARGO_MSS(handlers_.remove(handler));
                ARGO_MSS(add(handler));
            }
            else
            {
                ARGO_L("Skipping " << handler.name());
            }
        }
        ARGO_L("Merging done");
        ARGO_MSS_END();
    }
    //!Parses the given arguments. By default, the first argument is not skipped. Useful for unit-testing or when the arguments are not directly read from `argv`.
    Result parse(const std::vector<std::string> &arguments, const bool skip_first_argument = false)
    {
        ARGO_S(ARGO_DEBUG);
        ARGO_L("Starting parsing");
        reset_();
        auto data = core::normalize_arguments(arguments);
        auto args = core::Args{ data };
        pcontext_ = create_context_(*this, &args);
        if (skip_first_argument && context_().args().current() != context_().args().end()) context_().args().next();
        process_();
        auto result = context_().result();
        result.arguments.assign(ARGO_RANGE(context_().args()));
        ARGO_L("Finished parsing");
        return result;
    }
    //!Parses the given C-style arguments. By default, the first argument is skipped. Useful to directly pass the arguments of `main` where the first argument is the application's executable name.
    template <typename CharArrayType>
    Result parse(unsigned int argc, CharArrayType argv, const bool skip_first_argument = true)
    {
        std::vector<std::string> args;
        std::copy(argv, argv + argc, std::back_inserter(args));
        if (config_.program.name.brief.empty() && !!argc) config_.program.name.brief = core::basename_of(argv[0]);
        return parse(args, skip_first_argument);
    }
    //!Prints the usage section to the given output stream.
    bool print_usage(std::ostream &os) const
    {
        ARGO_MSS_BEGIN(bool);
        ARGO_MSS(!!os);
        os << formatter().format_usage(context_());
        ARGO_MSS_END();
    }
    //!Prints the help section to the given output stream.
    bool print_help(std::ostream &os) const
    {
        ARGO_MSS_BEGIN(bool);
        ARGO_MSS(!!os);
        os << formatter().format_help(context_());
        ARGO_MSS_END();
    }
    //!Prints the version section to the given output stream.
    bool print_version(std::ostream &os) const
    {
        ARGO_MSS_BEGIN(bool);
        ARGO_MSS(!!os);
        os << formatter().format_version(context_());
        ARGO_MSS_END();
    }
    //!Sets the output formatter.
    void set_formatter(const formatter::Interface &formatter) { pformatter_ = formatter.clone(); }
    //!Returns the output formatter.
    const formatter::Interface &formatter() const
    {
        if (!pformatter_) pformatter_ = core::make_unique<formatter::Default>();
        assert(!!pformatter_);
        return *pformatter_;
    }
    //!Returns the output formatter.
    formatter::Interface &formatter()
    {
        const auto &self = *this;
        return const_cast<formatter::Interface &>(self.formatter());
    }

private:
    static std::unique_ptr<core::Context> create_context_(Arguments &parser, core::Args *args = nullptr)
    {
        core::Context::ParserCouldContinueFunction parser_could_continue = [&parser](core::Context &context) {
            ARGO_S(ARGO_DEBUG);
            ARGO_L("Checking if parser could continue with current argument in phase " << parser.phase_);
            core::handler::search::FirstMatch search{ context };
            search.add<core::handler::search::Custom>();
            if (core::Phase::Positionals == parser.phase_) search.add<core::handler::search::Positional>();
            search.add<core::handler::search::Option>();
            parser.handlers_.accept_on_elements(search);
            const bool could_continue = search.result();
            ARGO_L(ARGO_C(could_continue));
            return could_continue;
        };
        auto ptr = core::make_unique<core::Context>(parser, parser.config_, parser.handlers_, parser.phase_, parser_could_continue);
        if (!!args) ptr->set_args(*args);
        return ptr;
    }
    std::unique_ptr<core::Context> create_switch_context_(Arguments &parser) { return create_context_(parser, &context_().args()); }
    const core::Context &context_() const
    {
        if (!pcontext_) pcontext_ = create_context_(const_cast<Arguments &>(*this));
        assert(!!pcontext_);
        return *pcontext_;
    }
    core::Context &context_()
    {
        const auto &self = *this;
        return const_cast<core::Context &>(self.context_());
    }
    void process_configuration_()
    {
        if (config_.parser.help)
        {
            ARGO_ASSERT_RELEASE(add_help_());
        }
        if (config_.parser.version)
        {
            ARGO_ASSERT_RELEASE(add_version_());
        }
        if (config_.parser.responsefile)
        {
            ARGO_ASSERT_RELEASE(add_responsefile_());
        }
    }
    bool add_help_()
    {
        ARGO_MSS_BEGIN(bool);
        handler::Flag flag{ "--help", "-h" };
        flag.action(action::run([](core::Context &context, const std::string &) {
            context.parser().print_help(std::cout);
            context.abort();
            return true;
        }));
        flag.help("Prints out this help.");
        ARGO_MSS(add(flag));
        ARGO_MSS_END();
    }
    bool add_version_()
    {
        ARGO_MSS_BEGIN(bool);
        handler::Flag flag{ "--version" };
        flag.action(action::run([](core::Context &context, const std::string &) {
            context.parser().print_version(std::cout);
            context.abort();
            return true;
        }));
        flag.help("Prints out the version information.");
        ARGO_MSS(add(flag));
        ARGO_MSS_END();
    }
    bool add_responsefile_()
    {
        ARGO_MSS_BEGIN(bool);
        handler::ResponseFile rsp;
        ARGO_MSS(add(rsp));
        ARGO_MSS_END();
    }
    core::handler::IHandler *search_()
    {
        ARGO_S(ARGO_DEBUG);
        const auto &arg = *context_().args().current();
        core::handler::search::FirstMatch search{ context_() };
        auto lookup = [&]() {
            ARGO_L("Searching handler for " << ARGO_C(arg) << ARGO_C(phase_));
            search.clear();
            search.add<core::handler::search::Custom>();
            switch (phase_)
            {
            case core::Phase::OptionsFirstPass:
                search.add<core::handler::search::Option>();
                break;
            case core::Phase::Positionals:
                search.add<core::handler::search::Positional>();
                break;
            case core::Phase::OptionsLastPass:
                search.add<core::handler::search::Option>();
                break;
            default:
                assert(false);
                break;
            }
            handlers_.accept_on_elements(search);
        };
        while (core::Phase::Nr_ != phase_ && !search.result())
        {
            lookup();
            if (!search.result())
            {
                //#TODO: optimization: if the current argument is an option
                // we don't need to go through all the phases. If we passed
                // the first options phase, anything else is superfluous
                ARGO_L("No result found, moving to next phase");
                phase_ = core::Phase(static_cast<unsigned>(phase_) + 1);
            }
        }
        return search.result();
    }
    bool process_()
    {
        ARGO_S(ARGO_DEBUG);
        ARGO_MSS_BEGIN(bool);
        if (context_().aborted()) ARGO_MSS_RETURN_OK();
        ARGO_L("Processing arguments using parser " << this << "...");
        while (context_().args().current() != context_().args().end())
        {
            if (context_().aborted())
            {
                ARGO_L("Action requested aborting processing...");
                ARGO_MSS_RETURN_OK();
            }
            const auto &arg = *context_().args().current();
            auto phandler = search_();
            ARGO_MSS(!!phandler,
                {
                    auto error = context_().error();
                    if (core::name::is_option(arg))
                        error << "Unknown option";
                    else
                    {
                        error << "Unexpected positional argument";
                    }
                    error << " " << core::string::surround(arg, "'");
                });
            auto &handler = *phandler;
            ARGO_L("Handler found: " << core::handler::name_of(handler));
            ARGO_L("Verifying delegate parser " << core::handler::name_of(handler) << " is valid");
            ARGO_MSS(handler.is_valid(),
                {
                    context_().error() << "Handler for argument " << core::string::surround(arg, "'") << " is wrongly configured";
                });
            ARGO_MSS(update_info_(handler));
            ARGO_L("Yielding to delegate parser");
            context_().set_handler(handler);
            ARGO_MSS(handler.parse(context_()));
            ARGO_L("Continuing with toplevel parsing");
            ARGO_L("Checking if a parser switch occurred");
            if (context_().is_current_parser(*this))
            {
                ARGO_L("No, so let's continue");
            }
            else
            {
                auto &parser = context_().parser();
                ARGO_L("Yes, a parser switch occurred. Delegating to parser " << &parser << "...");
                parser.pcontext_ = create_switch_context_(parser);
                parser.process_();
                ARGO_L("Parser switch finished");
                context_().result() = parser.context_().result();
                ARGO_MSS(ReturnCode::SuccessAndContinue == context_().result().status);
                ARGO_L("Continuing where we left off with parser " << this << "...");
                context_().switch_parser(*this);
            }
        }
        ARGO_L("Finished processing all arguments");
        if (!context_().aborted()) ARGO_MSS(is_satisfied_());
        ARGO_MSS_END();
    }
    bool update_info_(const core::handler::IHandler &handler)
    {
        ARGO_S(ARGO_DEBUG);
        auto pcurrent = &handler;
        while (!!pcurrent)
        {
            ARGO_L("Updating handler info of '" << *pcurrent << "'");
            auto &info = context_().info(*pcurrent);
            ++info.nr_parses;
            ARGO_L("-> " << ARGO_C(info.nr_parses));
            pcurrent = pcurrent->parent();
            if (!!pcurrent)
            {
                ARGO_L("Handler has parent: " << *pcurrent);
            }
        }
        return true;
    }
    bool is_satisfied_()
    {
        ARGO_S(ARGO_DEBUG);
        ARGO_MSS_BEGIN(bool);
        ARGO_L("Checking all handlers are satisified...");
        core::handler::visitor::ApplyConstFunction function{
            [&](const core::handler::IHandler &handler) {
                ARGO_S(ARGO_DEBUG);
                ARGO_L("Checking handler " << core::handler::name_of(handler) << " is satisfied");
                const auto res = handler.is_satisfied(context_());
                ARGO_L("-> satisfied: " << ARGO_C(res));
                return res;
            }
        };
        ARGO_MSS(handlers_.accept_on_elements(function));
        ARGO_L("All handlers are satisfied");
        ARGO_MSS_END();
    }
    void reset_()
    {
        ARGO_S(ARGO_DEBUG);
        for (auto &handler: handlers_)
        {
            ARGO_L("Resetting handler " << handler.name());
            handler.reset_();
        }
    }

    Configuration config_{};
    core::Handlers handlers_;
    mutable formatter::Ptr pformatter_ = nullptr;
    mutable std::unique_ptr<core::Context> pcontext_ = nullptr;
    core::Phase phase_ = core::Phase::OptionsFirstPass;
};

} }

#endif

#ifndef HEADER_lw_argo_handler_group_Simple_hpp_INCLUDE_GUARD
#define HEADER_lw_argo_handler_group_Simple_hpp_INCLUDE_GUARD


namespace lw { namespace argo {

namespace handler { namespace group {

//!Group that holds a simple collection of handlers.
class Simple : public Interface
{
public:
    explicit Simple(const std::string &name)
        : Interface(name, "Group")
    {
    }

private:
    virtual core::handler::Ptr clone() const override { return core::make_unique<Simple>(*this); }
    virtual bool is_satisfied(core::Context &context) const override
    {
        ARGO_MSS_BEGIN(bool);
        bool parsed = false;
        for (const auto &handler : *this)
        {
            ARGO_MSS(handler.is_satisfied(context));
            if (!parsed) parsed = !!context.info(handler).nr_parses;
        }
        ARGO_MSS(is_optional() || parsed, context.error() << "At least one argument is required for " << core::handler::name_of(*this););
        ARGO_MSS_END();
    }
};

}} // namespace handler::group

} }

#endif

#ifndef HEADER_lw_argo_handler_group_Exclusive_hpp_INCLUDE_GUARD
#define HEADER_lw_argo_handler_group_Exclusive_hpp_INCLUDE_GUARD


namespace lw { namespace argo {

namespace handler { namespace group {

class Exclusive;

namespace details { namespace exclusive {

class Validator : public core::handler::visitor::IVisitor<true>
{
public:
    explicit Validator(core::Context &context, const core::handler::IHandler &group)
        : context_(context)
        , group_(group)
    {
    }

    virtual bool visit(const handler::Flag &handler) override { return visit_(handler); }
    virtual bool visit(const handler::Interface &handler) override { return visit_(handler); }
    virtual bool visit(const handler::Option &handler) override { return visit_(handler); }
    virtual bool visit(const handler::Positional &handler) override { return visit_(handler); }
    virtual bool visit(const handler::Toggle &handler) override { return visit_(handler); }
    virtual bool visit(const handler::group::Interface &group) override
    {
        ARGO_MSS_BEGIN(bool);
        ARGO_MSS(visit_(group));
        const auto &info = context_.info(group);
        if (!!info.nr_parses) ARGO_MSS(group.is_satisfied(context_));
        ARGO_MSS_END();
    }

    unsigned nr_parsed() const { return nr_parsed_; }

private:
    template <typename Handler>
    bool visit_(const Handler &handler)
    {
        ARGO_MSS_BEGIN(bool);
        ARGO_S(ARGO_DEBUG);
        const auto &info = context_.info(handler);
        nr_parsed_ += static_cast<unsigned>(!!info.nr_parses);
        ARGO_L(ARGO_C(nr_parsed_));
        ARGO_L("Validating " << ARGO_C(handler));
        ARGO_MSS(nr_parsed_ < 2,
            {
                assert(!!pprev_handler_);
                context_.error() << core::handler::name_of(handler, true) << " belongs to " << core::handler::name_of(group_) << " and cannot be used in combination with options from the same group";
            });
        pprev_handler_ = &handler;
        ARGO_MSS_END();
    }
    core::Context &context_;
    const core::handler::IHandler &group_;
    std::string group_name_;
    unsigned nr_parsed_ = 0;
    const core::handler::IHandler *pprev_handler_ = nullptr;
};

}} // namespace details::exclusive

//!Group in which the presence of any one option/group within it, forces the exclusion of the others.
class Exclusive : public Interface
{
public:
    explicit Exclusive(const std::string &name)
        : Interface(name, "Exclusive group")
    {
    }

    virtual core::handler::Ptr clone() const override { return core::make_unique<Exclusive>(*this); }
    virtual bool is_satisfied(core::Context &context) const override
    {
        ARGO_MSS_BEGIN(bool);
        ARGO_S(ARGO_DEBUG);
        ARGO_L("Checking if " << core::handler::name_of(*this) << " is satisfied");
        details::exclusive::Validator validator{ context, *this };
        ARGO_MSS(handlers_().accept_on_elements(validator));
        ARGO_MSS(is_optional() || validator.nr_parsed() == 1, context.error() << "Missing argument for " << core::handler::name_of(*this););
        ARGO_MSS_END();
    }
};

}} // namespace handler::group

} }

#endif

#ifndef HEADER_lw_argo_handler_group_Inclusive_hpp_INCLUDE_GUARD
#define HEADER_lw_argo_handler_group_Inclusive_hpp_INCLUDE_GUARD


namespace lw { namespace argo {

namespace handler { namespace group {

namespace details { namespace inclusive {

class Validator : public core::handler::visitor::IVisitor<true>
{
public:
    explicit Validator(core::Context &context, const std::string &group_name)
        : context_(context)
        , group_name_(group_name)
    {
    }

    virtual bool visit(const handler::Flag &handler) override { return visit_(handler); }
    virtual bool visit(const handler::Interface &handler) override { return visit_(handler); }
    virtual bool visit(const handler::Option &handler) override { return visit_(handler); }
    virtual bool visit(const handler::Positional &handler) override { return visit_(handler); }
    virtual bool visit(const handler::Toggle &handler) override { return visit_(handler); }
    virtual bool visit(const handler::group::Interface &group) override
    {
        ARGO_MSS_BEGIN(bool);
        ARGO_MSS(visit_(group));
        const auto &info = context_.info(group);
        if (!!info.nr_parses) ARGO_MSS(group.is_satisfied(context_));
        ARGO_MSS_END();
    }
    bool parsed() const { return !!must_have_parsed_ && *must_have_parsed_; }

private:
    template <typename Handler>
    bool visit_(const Handler &handler)
    {
        ARGO_MSS_BEGIN(bool);
        ARGO_S(ARGO_DEBUG);
        const auto &info = context_.info(handler);
        ARGO_L("Validating " << ARGO_C(handler));
        if (!must_have_parsed_)
        {
            must_have_parsed_ = info.nr_parses;
            ARGO_L("Looking for options that must have been parsed: " << ARGO_C(*must_have_parsed_));
        }
        else
        {
            ARGO_L(ARGO_C(info.nr_parses));
            ARGO_MSS(*must_have_parsed_ == static_cast<bool>(info.nr_parses),
                {
                    auto error = context_.error();
                    if (*must_have_parsed_)
                    {
                        error << "Missing argument '" << handler << "' for inclusive group '" << group_name_ << "'";
                    }
                    else
                    {
                        error << "Argument '" << handler << "' belongs to inclusive group '" << group_name_ << "', which expects other arguments";
                    }
                });
        }
        ARGO_MSS_END();
    }
    core::Context &context_;
    std::string group_name_;
    core::optional<bool> must_have_parsed_;
};

}} // namespace details::inclusive

//!Group in which the presence of any one option/group within it, forces the presence of the others.
class Inclusive : public Interface
{
public:
    explicit Inclusive(const std::string &name)
        : Interface(name, "Inclusive group")
    {
    }

    virtual core::handler::Ptr clone() const override { return core::make_unique<Inclusive>(*this); }
    virtual bool is_satisfied(core::Context &context) const override
    {
        ARGO_MSS_BEGIN(bool);
        ARGO_S(ARGO_DEBUG);
        ARGO_L("Checking if " << core::handler::name_of(*this) << " is satisfied");
        const auto &info = context.info(*this);
        ARGO_L(ARGO_C(info.nr_parses));
        if (!!info.nr_parses)
        {
            details::inclusive::Validator validator{ context, name() };
            ARGO_MSS(handlers_().accept_on_elements(validator));
        }
        else
        {
            ARGO_MSS(is_optional(), context.error() << "Missing arguments for required " << core::handler::name_of(*this););
        }
        ARGO_MSS_END();
    }
};

}} // namespace handler::group

} }

#endif

#ifndef HEADER_lw_argo_require_any_of_hpp_INCLUDE_GUARD
#define HEADER_lw_argo_require_any_of_hpp_INCLUDE_GUARD


namespace lw { namespace argo {

namespace require {

//!Verifies that the parsed value is any of the given values.
template<typename Collection, typename Type = typename core::traits::conversion<typename std::remove_cv<typename Collection::value_type>::type>::result_type>
action::Run<Type> any_of(const Collection &values)
{
    auto check = [values](core::Context &context, Type value)
    {
        ARGO_MSS_BEGIN(bool);
        const auto it = std::find(ARGO_RANGE(values), value);
        ARGO_MSS(std::end(values) != it,
            context.error() << "Unexpected value '" << value << "' for " << core::handler::name_of(context.handler()) << ". "
                            << "Expected " << core::string::quote_or(ARGO_RANGE(values)));
        ARGO_MSS_END();
    };
    return action::Run<Type>{ check };
}

} // namespace require

} }

#endif

#ifndef HEADER_lw_argo_require_lesser_than_or_equal_hpp_INCLUDE_GUARD
#define HEADER_lw_argo_require_lesser_than_or_equal_hpp_INCLUDE_GUARD


namespace lw { namespace argo {

namespace require {

//!Verifies that the parsed value is lesser than or equal to the given value.
template<typename Type, typename = typename std::is_arithmetic<Type>::type>
action::Run<Type> lesser_than_or_equal(Type cutoff)
{
    auto check = [cutoff](core::Context &context, Type value)
    {
        ARGO_MSS_BEGIN(bool);
        ARGO_MSS(value <= cutoff,
            context.error() << "Unexpected value '" << value << "' for " << core::handler::name_of(context.handler()) << ". "
                            << "Expected value lesser than or equal to " << cutoff);
        ARGO_MSS_END();
    };
    return action::Run<Type>{ check };
}

} // namespace require

} }

#endif

#ifndef HEADER_lw_argo_require_lesser_than_hpp_INCLUDE_GUARD
#define HEADER_lw_argo_require_lesser_than_hpp_INCLUDE_GUARD


namespace lw { namespace argo {

namespace require {

//!Verifies that the parsed value is lesser than the given value.
template<typename Type, typename = typename std::is_arithmetic<Type>::type>
action::Run<Type> lesser_than(Type cutoff)
{
    auto check = [cutoff](core::Context &context, Type value)
    {
        ARGO_MSS_BEGIN(bool);
        ARGO_MSS(value < cutoff,
            context.error() << "Unexpected value '" << value << "' for " << core::handler::name_of(context.handler()) << ". "
                            << "Expected value lesser than " << cutoff);
        ARGO_MSS_END();
    };
    return action::Run<Type>{ check };
}

} // namespace require

} }

#endif

#ifndef HEADER_lw_argo_require_greater_than_hpp_INCLUDE_GUARD
#define HEADER_lw_argo_require_greater_than_hpp_INCLUDE_GUARD


namespace lw { namespace argo {

namespace require {

//!Verifies that the parsed value is greater than the given value.
template<typename Type, typename = typename std::is_arithmetic<Type>::type>
action::Run<Type> greater_than(Type cutoff)
{
    auto check = [cutoff](core::Context &context, Type value)
    {
        ARGO_MSS_BEGIN(bool);
        ARGO_MSS(value > cutoff,
            context.error() << "Unexpected value '" << value << "' for " << core::handler::name_of(context.handler()) << ". "
                            << "Expected value greater than " << cutoff);
        ARGO_MSS_END();
    };
    return action::Run<Type>{ check };
}

} // namespace require

} }

#endif

#ifndef HEADER_lw_argo_require_greater_than_or_equal_hpp_INCLUDE_GUARD
#define HEADER_lw_argo_require_greater_than_or_equal_hpp_INCLUDE_GUARD


namespace lw { namespace argo {

namespace require {

//!Verifies that the parsed value is greater than or equal to the given value.
template<typename Type, typename = typename std::is_arithmetic<Type>::type>
action::Run<Type> greater_than_or_equal(Type cutoff)
{
    auto check = [cutoff](core::Context &context, Type value)
    {
        ARGO_MSS_BEGIN(bool);
        ARGO_MSS(value >= cutoff,
            context.error() << "Unexpected value '" << value << "' for " << core::handler::name_of(context.handler()) << ". "
                            << "Expected value greater than or equal to " << cutoff);
        ARGO_MSS_END();
    };
    return action::Run<Type>{ check };
}

} // namespace require

} }

#endif

#ifndef HEADER_lw_argo_require_none_of_hpp_INCLUDE_GUARD
#define HEADER_lw_argo_require_none_of_hpp_INCLUDE_GUARD


namespace lw { namespace argo {

namespace require {

//!Verifies that the parsed value is none of the given values.
template<typename Collection, typename Type = typename core::traits::conversion<typename std::remove_cv<typename Collection::value_type>::type>::result_type>
action::Run<Type> none_of(const Collection &values)
{
    auto check = [values](core::Context &context, Type value)
    {
        ARGO_MSS_BEGIN(bool);
        const auto it = std::find(ARGO_RANGE(values), value);
        ARGO_MSS(std::end(values) == it,
            context.error() << "Disallowed value '" << value << "' for " << core::handler::name_of(context.handler()) << ". "
                            << "Expected values other than " << core::string::quote_or(ARGO_RANGE(values)));
        ARGO_MSS_END();
    };
    return action::Run<Type>{ check };
}

} // namespace require

} }

#endif

#ifndef HEADER_lw_argo_require_range_open_open_hpp_INCLUDE_GUARD
#define HEADER_lw_argo_require_range_open_open_hpp_INCLUDE_GUARD


namespace lw { namespace argo {

namespace require { namespace range {

//!Verifies that the parsed value lies within the range ``]first, last[``.
template<typename Type, typename = typename std::is_arithmetic<Type>::type>
action::Run<Type> open_open(Type first, Type last)
{
    assert(first < last);
    auto check = [first, last](core::Context &context, Type value) {
        ARGO_MSS_BEGIN(bool);
        const auto is_within_range = (value > first) && (value < last);
        ARGO_MSS(is_within_range,
            context.error() << "Unexpected value '" << value << "' for " << core::handler::name_of(context.handler()) << ". "
                            << "Expected value within range ]" << first << ", " << last << "[");
        ARGO_MSS_END();
    };
    return action::Run<Type>{ check };
}

}} // namespace require::range

} }

#endif

#ifndef HEADER_lw_argo_require_range_closed_closed_hpp_INCLUDE_GUARD
#define HEADER_lw_argo_require_range_closed_closed_hpp_INCLUDE_GUARD


namespace lw { namespace argo {

namespace require { namespace range {

//!Verifies that the parsed value lies within the range ``[first, last]``.
template<typename Type, typename = typename std::is_arithmetic<Type>::type>
action::Run<Type> closed_closed(Type first, Type last)
{
    assert(first < last);
    auto check = [first, last](core::Context &context, Type value) {
        ARGO_MSS_BEGIN(bool);
        const auto is_within_range = (value >= first) && (value <= last);
        ARGO_MSS(is_within_range,
            context.error() << "Unexpected value '" << value << "' for " << core::handler::name_of(context.handler()) << ". "
                            << "Expected value within range [" << first << ", " << last << "]");
        ARGO_MSS_END();
    };
    return action::Run<Type>{ check };
}

}} // namespace require::range

} }

#endif

#ifndef HEADER_lw_argo_require_range_open_closed_hpp_INCLUDE_GUARD
#define HEADER_lw_argo_require_range_open_closed_hpp_INCLUDE_GUARD


namespace lw { namespace argo {

namespace require { namespace range {

//!Verifies that the parsed value lies within the range ``]first, last]``.
template<typename Type, typename = typename std::is_arithmetic<Type>::type>
action::Run<Type> open_closed(Type first, Type last)
{
    assert(first < last);
    auto check = [first, last](core::Context &context, Type value) {
        ARGO_MSS_BEGIN(bool);
        const auto is_within_range = (value > first) && (value <= last);
        ARGO_MSS(is_within_range,
            context.error() << "Unexpected value '" << value << "' for " << core::handler::name_of(context.handler()) << ". "
                            << "Expected value within range ]" << first << ", " << last << "]");
        ARGO_MSS_END();
    };
    return action::Run<Type>{ check };
}

}} // namespace require::range

} }

#endif

#ifndef HEADER_lw_argo_require_range_closed_open_hpp_INCLUDE_GUARD
#define HEADER_lw_argo_require_range_closed_open_hpp_INCLUDE_GUARD


namespace lw { namespace argo {

namespace require { namespace range {

//!Verifies that the parsed value lies within the range ``[first, last[``.
template<typename Type, typename = typename std::is_arithmetic<Type>::type>
action::Run<Type> closed_open(Type first, Type last)
{
    assert(first < last);
    auto check = [first, last](core::Context &context, Type value) {
        ARGO_MSS_BEGIN(bool);
        const auto is_within_range = (value >= first) && (value < last);
        ARGO_MSS(is_within_range,
            context.error() << "Unexpected value '" << value << "' for " << core::handler::name_of(context.handler()) << ". "
                            << "Expected value within range [" << first << ", " << last << "[");
        ARGO_MSS_END();
    };
    return action::Run<Type>{ check };
}

}} // namespace require::range

} }

#endif

#ifndef HEADER_lw_argo_core_config_version_hpp_INCLUDE_GUARD
#define HEADER_lw_argo_core_config_version_hpp_INCLUDE_GUARD


//This file is generated by CMake

namespace lw { namespace argo {

namespace core { namespace config {

static constexpr char version[] = "1.4.7";

} }

} }

#endif
#endif
