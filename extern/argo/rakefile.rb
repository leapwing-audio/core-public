module Argo
  def self.repo_dir()
    File.join(File.dirname(__FILE__), "repo")
  end
end

namespace :argo do
  desc "(re)generate the argo header file"
  task :generate do
    puts Argo.repo_dir
    Dir.chdir(Argo.repo_dir) do
      sh("inv", "library.transform", "-n", "lw::argo", "-o", "../inc/lw/Argo.hpp")
    end
  end
end
