add_executable (core-public-test)

set_target_properties (core-public-test PROPERTIES 
						LABELS "Leapwing;Tests"
					   	FOLDER Core
					   	EchoString "Building core-public tests..."
					   	MACOSX_BUNDLE OFF)

target_link_libraries (core-public-test PRIVATE
						LW::core-public
						Catch2::Catch2)

target_sources (core-public-test PRIVATE

	src/lw/mss_tests.cpp
	src/lw/Range_tests.cpp
	src/lw/string/util_tests.cpp

	"${LW_TEST_MAIN}"

	)

target_include_directories (core-public-test PRIVATE src)

get_target_property (srcs core-public-test SOURCES)

list (REMOVE_ITEM srcs "${LW_TEST_MAIN}")

source_group (TREE "${CMAKE_CURRENT_LIST_DIR}" PREFIX core-public-test FILES ${srcs})

add_executable (LW::core-public-test ALIAS core-public-test)

add_test (NAME LW.core.public
		  COMMAND LW::core-public-test ${LW_CATCH_FLAGS}
		  COMMAND_EXPAND_LISTS
		  )
