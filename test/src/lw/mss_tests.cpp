#include "catch2/catch.hpp"
#include "lw/mss.hpp"

namespace {
template <typename Return, typename Value>
Return test(Value && v)
{
    MSS_BEGIN(Return);
    MSS(v);
    MSS_END();
}

}

TEST_CASE("bool mss tests", "[ut][mss]")
{
    SECTION("bool to bool") 
    {
        REQUIRE(test<bool>(true));
        REQUIRE(test<bool>(true));
        REQUIRE(!test<bool>(false));
    }

    SECTION("pointer to bool")
    {
        char v = 'a';
        char * ptr = &v;
        const char * cptr = &v;

        bool expected = true;
        SECTION("success") {  }

        SECTION("failure")
        {
            expected = false;
            ptr = nullptr;
            cptr= nullptr;
        }

        REQUIRE(expected == test<bool>(ptr));
        REQUIRE(expected == test<bool>(cptr));
    }

    SECTION("nullptr to bool")
    {
        REQUIRE(!test<bool>(nullptr));
    }
}


