#include <lw/string/util.hpp>
#include <catch2/catch.hpp>

TEST_CASE("string split test", "[ut][string]")
{
    std::string str;
    std::string sep;
    std::vector<std::string> exp;

    SECTION("empty")
    {
    }

    SECTION("empty separator")
    {
        SECTION("single character")
        {
            str = "a";
            exp = { "a" };
        }
        SECTION("multiple characters")
        {
            str = "abcd";
            exp = { "a", "b", "c", "d" };
        }
    }

    SECTION("single character separator")
    {
        sep = "|";
        SECTION("within string")
        {
            str = "a|b|c|d";
            exp = { "a", "b", "c", "d" };
        }
        SECTION("at end within string")
        {
            str = "a|b|c|d|";
            exp = { "a", "b", "c", "d" };
        }
        SECTION("multiple occurences")
        {
            str = "a||b|c|d|";
            exp = { "a", "", "b", "c", "d" };
        }
        SECTION("not in string")
        {
            str = "abcd";
            exp  = { str };
        }
    }

    std::vector<std::string> res;
    lw::string::split(str, sep, std::back_inserter(res));

    REQUIRE(res == exp);
}
