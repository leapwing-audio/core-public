#include "lw/Range.hpp"
#include "catch2/catch.hpp"

template <typename It> void const_test(const lw::ConstRange<It> & rng) {}
template <typename It> void test(const lw::Range<It> & rng) {}

TEST_CASE("Range conversion tests", "[ut][range]")
{
    using T1 = lw::Range<float>;
    using T2 = lw::Range<const float>;
    using T3 = lw::ConstRange<float>;
    using T4 = lw::ConstRange<const float>;

    {T1 in; T2 out(in); }
    {T1 in; T3 out(in); }
    {T1 in; T4 out(in); }

    {T2 in; T2 out(in); }
    {T2 in; T3 out(in); }
    {T2 in; T4 out(in); }
    
    {T3 in; T2 out(in); }
    {T3 in; T3 out(in); }
    {T3 in; T4 out(in); }
    
    {T4 in; T2 out(in); }
    {T4 in; T3 out(in); }
    {T4 in; T4 out(in); }
    
//    {T2 in; T1 out(in); }
//   {T3 in; T1 out(in); }
//    {T4 in; T1 out(in); }
}

namespace {

    bool test1(const lw::Range<int> src) { return true; }
    bool test2(const lw::ConstRange<int> src) { return true; }
    bool test3(const lw::Range<const int> src) { return true; }
}

TEST_CASE("Conversion from std::vector<>", "[ut][range]")
{
    std::vector<int> vct;

    REQUIRE(test1(vct));
    REQUIRE(test2(vct));
    REQUIRE(test3(vct));
    

    const std::vector<int> & cv = vct;
    REQUIRE(test2(cv));
    REQUIRE(test3(cv));
}
