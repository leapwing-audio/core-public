def __make_reflection_hdr(dir, rel_path)
{
    var path = "${cook.script_local_directory}/${dir}/${rel_path}"
    File.mkdir(path)
    var fn = "${path}/reflection.h"

    if(File.exists(fn)){
        return
    }

    var ns = rel_path.gsub("/", "_").gsub("-", "_")

    File.open(fn, 'w', fun[ns](os) {
        os << "#ifndef HEADER_${ns}_reflection_h_ALREADY_INCLUDED\n"
        os << "#define HEADER_${ns}_reflection_h_ALREADY_INCLUDED\n"
        os << "\n"
        os << "#ifdef __cplusplus\n"
        os << "extern \"C\" {\n"
        os << "#endif\n"
        os << "\n"
        os << "    const char * ${ns}_reflection();\n"
        os << "\n"
        os << "#ifdef __cplusplus\n"
        os << "}\n"
        os << "#endif\n"
        os << "\n"
        os << "#endif\n"
    })
}

def __make_reflection_src(dir, rel_path)
{
    var path = "${cook.script_local_directory}/${dir}/${rel_path}"
    File.mkdir(path)
    var fn = "${path}/reflection.c"
    var ns = rel_path.gsub("/", "_")

    File.open(fn, 'w', fun[rel_path,ns](os) {
        os << "#include \"${rel_path}/reflection.h\"\n"
        os << "\n"
        var ref_dir = cook.script_local_directory().gsub("\\", "\\\\")
        os << "const char * ${ns}_reflection() { return \"" << ref_dir << "\"; }\n"
    })
}

def make_ut(uri_str, dir)
{
    var uri = Uri(uri_str).as_relative

    // the unit test functionality
    var r = cook[uri].recipe("ut")
    r.depends_on("catch/func")
    r.add(dir, "**.[hc]pp")
    r.add(dir, "**.[hc]")

    // add reflection files
    __make_reflection_hdr(dir, "lw/${uri}")
    __make_reflection_src(dir, "lw/${uri}")

    // the unit test executable
    var exe = cook["ut"].recipe(uri, TargetType.Executable)
    exe.depends_on(r.uri)
    exe.depends_on("catch/exe")

    return r
}
